# Project

## Compatibility

This application targets iOS 11 and newer.

## Project Modules

This particular iOS project consists of the following modules:

1. **utility** - various utils such as extensions or util classes,
2. **persistence** - implements persistence models, converters and managers,
3. **network** - implements network models, converters and managers,
4. **domain** - merges persistence and network layer into one, incl. domain models and managers,
5. **presentation** - produces HCA Sales executable application.

## Project Build Type

Build is configured to produce two different types:

  - `Project-Dev` - type with enabled all development tools,
  - `Project` - type with disabled all development tools.

## Documentation and configuration

  - `api` - to configure api url check `Server` enum stored in `AuthApi.swift` file,
  - `third party libraies` - check `Podfile` to verify which libraries are used

## Building production

Before building production ensure to:

  - change build type to `iKawa`.

This type uses production configuration files. These files introduces crucial changes:

  - defines proper server connection details (uris, etc.),
  - enables/disables development tools

