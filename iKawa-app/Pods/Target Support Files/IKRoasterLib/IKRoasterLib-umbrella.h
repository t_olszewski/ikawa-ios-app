#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "FirmwareManager.h"
#import "FirmwareManagerDelegate.h"
#import "IKInterface.h"
#import "IKInterfaceDelegate.h"
#import "IKPeripheral.h"
#import "IKTypes.h"
#import "Profile.h"
#import "Roaster.h"
#import "RoasterManager.h"
#import "RoasterManagerDelegate.h"
#import "RoasterSetting.h"

FOUNDATION_EXPORT double IKRoasterLibVersionNumber;
FOUNDATION_EXPORT const unsigned char IKRoasterLibVersionString[];

