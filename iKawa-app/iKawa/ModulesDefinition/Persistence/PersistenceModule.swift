enum Persistence {
    typealias Base = BaseStorageProtocol
    typealias User = UserPersistenceProtocol
    typealias Session = KeychainProtocol
}
