enum Network {
    typealias Authentication = AuthenticateManagerProtocol
    typealias Exception = NetworkExceptionManagerProtocol
    typealias Observer = NetworkObserverProtocol
}
