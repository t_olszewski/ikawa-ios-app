enum Main {
    typealias View = MainViewProtocol
    typealias ViewModel = MainViewModelProtocol & BaseViewModel
}
