enum Splash {
    typealias View = SplashViewProtocol
    typealias ViewModel = SplashViewModelProtocol & BaseViewModel
}
