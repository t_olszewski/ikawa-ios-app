enum ScanQRCode {
    typealias View = ScanQRCodeViewProtocol
    typealias ViewModel = ScanQRCodeViewModelProtocol & BaseViewModel
}
