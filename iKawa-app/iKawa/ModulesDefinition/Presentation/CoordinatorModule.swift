enum Coordinator {
    typealias Base = BaseCoordinatorProtocol
    typealias Flow = FlowCoordinator
    typealias Auth = AuthCoordinatorProtocol
    typealias Main = MainCoordinatorProtocol
}
