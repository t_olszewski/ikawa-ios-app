enum CoffeePreview {
    typealias View = CoffeePreviewViewProtocol
    typealias ViewModel = CoffeePreviewViewModelProtocol & BaseViewModel
}
