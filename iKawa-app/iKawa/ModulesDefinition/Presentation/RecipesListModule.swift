enum RecipesList {
    typealias View = RecipesListViewProtocol
    typealias ViewModel = RecipesListViewModelProtocol & BaseViewModel
}
