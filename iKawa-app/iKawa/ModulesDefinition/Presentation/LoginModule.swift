enum Login {
    typealias View = LoginViewProtocol
    typealias ViewModel = LoginViewModelProtocol & BaseViewModel
}
