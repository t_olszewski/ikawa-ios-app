import DeallocationChecker
import Firebase
import IQKeyboardManagerSwift
import UIKit

// MARK: - CLASS DEFINITION

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    // MARK: - INTERNAL PROPERTIES

    var window: UIWindow?

    // MARK: - PRIVATE PROPERTIES

    // MARK: - INTERNAL APP COORDINATOR

    let dependenciesFactory = DependenciesFactory()
    var applicationCoordinator: Coordinator.Base?

    // MARK: - APPLICATION LIFE CYCLE

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // configure firebase
        FirebaseApp.configure()

        // Setup deallocation checker.
        setupDeallocationChecker()

        // Claer keychain on first app lunch.
        clearKeychainOnFirtAppLunch()

        // Setup keyboard.
        setupKeyboard()

        // Start main app flow.
        startMainFlow()

        return true
    }

    // MARK: - PRIVATE METHODS

    private func setupDeallocationChecker() {
        #if DEBUG
            DeallocationChecker.shared.setup(with: .alert)
        #endif
    }

    private func setupKeyboard() {
        IQKeyboardManager.shared.enable = true
    }

    private func startMainFlow() {
        // Create new window.
        let window = UIWindow(frame: UIScreen.main.bounds)

        // Create coordinator.
        let applicationCoordinator = ApplicationCoordinator(
            window: window,
            dependenciesFactory: dependenciesFactory
        )

        // Set new objects.
        self.window = window
        self.applicationCoordinator = applicationCoordinator

        // Start main UI flow.
        applicationCoordinator.starAppFlow()
    }

    private func clearKeychainOnFirtAppLunch() {
        if UserDefaults.standard.string(forKey: "FirstRun") == nil {
            UserDefaults.standard.set("firstRun", forKey: "FirstRun")
            UserDefaults.standard.synchronize()
            KeychainManager().clearAllSync()
        }
    }
}
