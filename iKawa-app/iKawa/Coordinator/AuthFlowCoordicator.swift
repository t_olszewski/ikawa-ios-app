import RxBus
import RxSwift
import UIKit

// MARK: - CLASS DEFINITION

protocol AuthCoordinatorProtocol: Coordinator.Flow {}

final class AuthFlowCoordicator: Coordinator.Auth {
    // MARK: - NAVIATION CALLBACKS

    var onFlowFinished: (() -> Void)?

    // MARK: - PRIVATE PROPERTIES

    private let window: UIWindow
    private var rootNavigationController: UINavigationController
    private let dependenciesFactory: Factory.ViewController
    private let keychainManager: Persistence.Session
    private let authDomainManager = AuthDomainManager(authenticationManager: AuthNetworkManager(), networkExceptionManager: NetworkExceptionManager(), userStorageManager: UserPersistenceManager(), keychainManager: KeychainManager())
    private let disposeBag = DisposeBag()

    // MARK: - INITIALIZER

    init(window: UIWindow, rootNavigationController: UINavigationController, dependenciesFactory: Factory.ViewController, keychainManager: Persistence.Session = KeychainManager()) {
        self.window = window
        self.rootNavigationController = rootNavigationController
        self.dependenciesFactory = dependenciesFactory
        self.keychainManager = keychainManager
    }

    func runFlow() {
        // Start observing logout event.
        startObservingLogoutEvent()

        // Start observing the network.
        dependenciesFactory.startNetworkObserving()

        // Create view.
        let loginView = dependenciesFactory.makeLoginViewController(onLoginButtonTap: {
            print("login button clicked!")
        })

        // Push new view controller.
        rootNavigationController.pushViewController(loginView, animated: false)
    }

    func willFinishFlow() {
        _ = rootNavigationController.popToRootViewController(animated: false)
    }

    // MARK: - PRIVATE METHODS

    private func onLoginButtonTap() {}

    private func onOnboardingTap() {
        let onboardingView = dependenciesFactory.makeRootOnboardingViewController(onSkipTap: onSkipTap)
        rootNavigationController.pushViewController(onboardingView, animated: true)
    }

    private func onSkipTap() {
        let mainView = dependenciesFactory.makeMainViewController()
        rootNavigationController.pushViewController(mainView, animated: true)
    }

    private func onBackTap() {
        rootNavigationController.popViewController(animated: true)
    }

    private func onBackTapError(message: String) {
        rootNavigationController.popViewController(animated: true)
        rootNavigationController.showBottomErrorPopUp(text: message)
    }

    private func onBackTapSuccess(message: String) {
        rootNavigationController.popViewController(animated: true)
        rootNavigationController.showBottomPopUp(text: message)
    }

    private func onShowConfirmationTap(text: String) {
        rootNavigationController.showBottomPopUp(text: text)
    }

    private func onErrorTap(text: String) {
        rootNavigationController.showBottomErrorPopUp(text: text)
    }

    private func onLogutTap() {
        onFlowFinished?()
    }

    private func startObservingLogoutEvent() {
        RxBus.shared
            .asObservable(event: Events.LoggedOut.self)
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] _ in
                self?.logout()
            }.disposed(by: disposeBag)
    }

    private func logout() {
        authDomainManager
            .logout()
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] event in
                switch event {
                case .completed:
                    // Handle succes.
                    self?.handleLogoutSuccess()

                case let .error(error):

                    // Handle error.
                    self?.handleLogoutError(error: error)
                }
            }
            .disposed(by: disposeBag)
    }

    // MARK: - PRIVATE METHODS

    private func handleLogoutSuccess() {
        // Log the fact.
        print("Logout user success.")

        // Call navigation callback.
        onLogutTap()
    }

    private func handleLogoutError(error: Error) {
        // Log the fact.
        print("Logout user error. Error = \(error.localizedDescription)")
    }
}
