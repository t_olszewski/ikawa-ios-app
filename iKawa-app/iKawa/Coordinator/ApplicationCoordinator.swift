import UIKit

// MARK: - CLASS DEFINITION

final class ApplicationCoordinator: Coordinator.Base {
    // MARK: - PUBLIC PROPERTIES

    var authFlowCoordicator: Coordinator.Auth?
    var mainFlowCoordicator: Coordinator.Main?

    // MARK: - PRIVATE PROPERTIES

    private let window: UIWindow
    private var naviagtionController: UINavigationController?
    private let dependenciesFactory: Factory.ViewController

    // MARK: - INITIALIZER

    init(window: UIWindow, dependenciesFactory: Factory.ViewController) {
        // Assign main window.
        self.window = window

        // Assign factory object.
        self.dependenciesFactory = dependenciesFactory
    }

    deinit {
        // Log the fact.
        print("Coordinators - Application cooridnator is removed")
    }

    // MARK: - PUBLIC METHODS

    func starAppFlow() {
        // Setup root view controller.
        setUpRootViewController()
    }

    // MARK: - PRIVATE METHODS

    private func setUpRootViewController() {
        // Create root screen.
        let rootScreen = dependenciesFactory.makeSplashViewController(
            onUserLogged: onUserLogged,
            onLoginRequired: onUserLogged // skip login required flow for now onLoginRequired
        )

        // Create root view controller.
        naviagtionController = UINavigationController(rootViewController: rootScreen)

        // Hide navigation bar.
        naviagtionController?.isNavigationBarHidden = true

        // Assign root view controller to main window.
        window.rootViewController = naviagtionController

        // Show the window and make it a key window.
        window.makeKeyAndVisible()
    }

    private func onUserLogged() {
        // Check if root view was created.
        guard let rootViewController = naviagtionController else { return }

        // Create authorization controller.
        authFlowCoordicator = AuthFlowCoordicator(
            window: window,
            rootNavigationController: rootViewController,
            dependenciesFactory: dependenciesFactory
        )

        // Create main controller.
        mainFlowCoordicator = MainFlowCoordicator(
            window: window,
            rootNavigationController: rootViewController,
            dependenciesFactory: dependenciesFactory
        )

        // Assign callback for flow finish.
        mainFlowCoordicator?.onFlowFinished = mainFlowFinished

        mainFlowCoordicator?.runFlow()
    }

    private func onLoginRequired() {
        // Check if root view was created.
        guard let rootViewController = naviagtionController else { return }

        // Create authorization controller.
        authFlowCoordicator = AuthFlowCoordicator(
            window: window,
            rootNavigationController: rootViewController,
            dependenciesFactory: dependenciesFactory
        )

        // Assign callback for flow finish.
        authFlowCoordicator?.onFlowFinished = authFlowFinished

        authFlowCoordicator?.runFlow()
    }

    private func authFlowFinished() {
        // Call method for clearing resources.
        authFlowCoordicator?.willFinishFlow()

        // Clear authorization controller.
        authFlowCoordicator = nil
    }

    private func mainFlowFinished() {
        // Call method for clearing resources.
        mainFlowCoordicator?.willFinishFlow()

        // Clear authorization controller.
        mainFlowCoordicator = nil
    }
}
