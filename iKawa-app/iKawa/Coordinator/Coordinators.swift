protocol BaseCoordinatorProtocol {
    var authFlowCoordicator: Coordinator.Auth? { get set }
    var mainFlowCoordicator: Coordinator.Main? { get set }

    func starAppFlow()
}

protocol FlowCoordinator {
    var onFlowFinished: (() -> Void)? { get set }
    func runFlow()
    func willFinishFlow()
}
