import RxBus
import RxSwift
import UIKit

// MARK: - CLASS DEFINITION

protocol MainCoordinatorProtocol: Coordinator.Flow {
    func showScanQRCodeViewController()
    func showCoffeePreviewViewController(_ model: Coffee)
    func showRecipesListViewController()
}

final class MainFlowCoordicator: Coordinator.Main {
    // MARK: - NAVIATION CALLBACKS

    var onFlowFinished: (() -> Void)?

    // MARK: - PRIVATE PROPERTIES

    private let window: UIWindow
    private var rootNavigationController: UINavigationController
    private let dependenciesFactory: Factory.ViewController
    private let authDomainManager = AuthDomainManager(authenticationManager: AuthNetworkManager(), networkExceptionManager: NetworkExceptionManager(), userStorageManager: UserPersistenceManager(), keychainManager: KeychainManager())
    private let disposeBag = DisposeBag()

    // MARK: - INITIALIZER

    init(window: UIWindow, rootNavigationController: UINavigationController, dependenciesFactory: Factory.ViewController) {
        self.window = window
        self.rootNavigationController = rootNavigationController
        self.dependenciesFactory = dependenciesFactory
    }

    // MARK: - PUBLIC METHODS

    func runFlow() {
        // Start observing logout event.
        startObservingLogoutEvent()

        // Start observing the network.
        dependenciesFactory.startNetworkObserving()

        let tabBarView = dependenciesFactory.makeTabBarController()
        rootNavigationController.pushViewController(tabBarView, animated: false)
    }

    func willFinishFlow() {
        _ = rootNavigationController.popToRootViewController(animated: false)
    }

    func showScanQRCodeViewController() {
        let vc = dependenciesFactory.makeScanQRCodeViewController()
        dependenciesFactory.buyNavigationController?.pushViewController(vc, animated: true)
    }

    func showCoffeePreviewViewController(_ model: Coffee) {
        let vc = dependenciesFactory.makeCoffeePreviewViewController(model)
        dependenciesFactory.buyNavigationController?.pushViewController(vc, animated: true)
    }

    func showRecipesListViewController() {
        let vc = dependenciesFactory.makeRecipesListViewController()
        dependenciesFactory.buyNavigationController?.pushViewController(vc, animated: true)
    }

    // MARK: - PRIVATE METHODS

    private func onBackTap() {
        rootNavigationController.popViewController(animated: true)
    }

    private func onBackTapError(message: String) {
        rootNavigationController.popViewController(animated: true)
        rootNavigationController.showBottomErrorPopUp(text: message)
    }

    private func onBackTapSuccess(message: String) {
        rootNavigationController.popViewController(animated: true)
        rootNavigationController.showBottomPopUp(text: message)
    }

    private func onShowConfirmationTap(text: String) {
        rootNavigationController.showBottomPopUp(text: text)
    }

    private func onErrorTap(text: String) {
        rootNavigationController.showBottomErrorPopUp(text: text)
    }

    private func onLogutTap() {
        onFlowFinished?()
    }

    private func startObservingLogoutEvent() {
        RxBus.shared
            .asObservable(event: Events.LoggedOut.self)
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] _ in
                self?.logout()
            }.disposed(by: disposeBag)
    }

    private func logout() {
        authDomainManager
            .logout()
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] event in
                switch event {
                case .completed:
                    // Handle succes.
                    self?.handleLogoutSuccess()

                case let .error(error):

                    // Handle error.
                    self?.handleLogoutError(error: error)
                }
            }
            .disposed(by: disposeBag)
    }

    // MARK: - PRIVATE METHODS

    private func handleLogoutSuccess() {
        // Log the fact.
        print("Logout user success.")

        // Call navigation callback.
        onLogutTap()
    }

    private func handleLogoutError(error: Error) {
        // Log the fact.
        print("Logout user error. Error = \(error.localizedDescription)")
    }
}
