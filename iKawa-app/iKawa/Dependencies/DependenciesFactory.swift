import Foundation
import UIKit

// MARK: - PROTOCOL DEFINITION

protocol ViewControllerFactory {
    var buyNavigationController: UINavigationController? { get }
    var roastNavigationController: UINavigationController? { get }
    var journalNavigationController: UINavigationController? { get }

    func startNetworkObserving()
    func makeSplashViewController(onUserLogged: @escaping (() -> Void), onLoginRequired: @escaping (() -> Void)) -> SplashViewController
    func makeRootOnboardingViewController(onSkipTap: @escaping (() -> Void)) -> RootOnboardingViewController
    func makeLoginViewController(onLoginButtonTap: @escaping (() -> Void)) -> LoginViewController
    func makeTabBarController() -> IKTabBarController
    func makeMainViewController() -> MainViewController
    func makeScanQRCodeViewController() -> ScanQRCodeViewController
    func makeCoffeePreviewViewController(_ model: Coffee) -> CoffeePreviewViewController
    func makeRecipesListViewController() -> RecipesListViewController
}

// MARK: - CLASS DEFINITION

final class DependenciesFactory {
    // MARK: - PUBLIC PROPERTIES

    var buyNavigationController: UINavigationController?
    var roastNavigationController: UINavigationController?
    var journalNavigationController: UINavigationController?

    // MARK: - PRIVATE PROPERTIES

    private lazy var networkObserver: Network.Observer = NetworkObserver()

    // MARK: - DOMAIN DEPENDENCIES

    static func authDomainManager() -> AuthDomainManager {
        return AuthDomainManager(
            authenticationManager: AuthNetworkManager(),
            networkExceptionManager: NetworkExceptionManager(),
            userStorageManager: UserPersistenceManager(),
            keychainManager: KeychainManager()
        )
    }
}

extension DependenciesFactory: Factory.ViewController {
    func startNetworkObserving() {
        networkObserver.startObserving()
    }

    func makeSplashViewController(onUserLogged: @escaping (() -> Void), onLoginRequired: @escaping (() -> Void)) -> SplashViewController {
        // Splash view model.
        var splashViewModel: Splash.ViewModel = SplashViewModel(authDomainManager: DependenciesFactory.authDomainManager())
        splashViewModel.onUserLogged = {
            onUserLogged()
        }
        splashViewModel.onLoginRequired = {
            onLoginRequired()
        }

        let splashView: Splash.View = SplashView(viewModel: splashViewModel)
        return SplashViewController(rootView: splashView, viewModel: splashViewModel)
    }

    // Onboarding View.
    func makeRootOnboardingViewController(onSkipTap: @escaping (() -> Void)) -> RootOnboardingViewController {
        var onboardingViewModel: Onboarding.ViewModel = RootOnboardingViewModel(authDomainManager: DependenciesFactory.authDomainManager())
        onboardingViewModel.onSkipTap = {
            onSkipTap()
        }

        return RootOnboardingViewController(viewModel: onboardingViewModel)
    }

    // Login view.
    func makeLoginViewController(onLoginButtonTap: @escaping (() -> Void)) -> LoginViewController {
        var loginViewModel: Login.ViewModel = LoginViewModel()
        loginViewModel.onLoginButtonTap = {
            onLoginButtonTap()
        }

        let loginView: Login.View = LoginView(viewModel: loginViewModel)
        return LoginViewController(rootView: loginView, viewModel: loginViewModel)
    }

    // Tab bar.
    func makeTabBarController() -> IKTabBarController {
        let tabBarViewController = IKTabBarController()

        let firstNavigationController = UINavigationController(rootViewController: makeMainViewController())
        firstNavigationController.setNavigationBarHidden(true, animated: false)
        firstNavigationController.title = "Buy"
        // firstNavigationController.tabBarItem.image = UIImage(named: "tabbar_buy")

        let secondNavigationController = UINavigationController(rootViewController: UIViewController())
        secondNavigationController.setNavigationBarHidden(true, animated: false)
        secondNavigationController.title = "Roast"
        // secondNavigationController.tabBarItem.image = UIImage(named: "tabbar_roast")

        let thirdNavigationController = UINavigationController(rootViewController: UIViewController())
        thirdNavigationController.setNavigationBarHidden(true, animated: false)
        thirdNavigationController.title = "Journal"
        // thirdNavigationController.tabBarItem.image = UIImage(named: "tabbar_journal")

        buyNavigationController = firstNavigationController
        roastNavigationController = secondNavigationController
        journalNavigationController = thirdNavigationController

        tabBarViewController.setViewControllers([firstNavigationController,
                                                 secondNavigationController,
                                                 thirdNavigationController], animated: false)

        return tabBarViewController
    }

    // Main view.
    func makeMainViewController() -> MainViewController {
        let mainViewModel: Main.ViewModel = MainViewModel(networkObserver: networkObserver)

        let mainView: Main.View = MainView(viewModel: mainViewModel)
        return MainViewController(rootView: mainView, viewModel: mainViewModel)
    }

    // Scan QR Code.
    func makeScanQRCodeViewController() -> ScanQRCodeViewController {
        let scanQRCodeViewModel: ScanQRCode.ViewModel = ScanQRCodeViewModel(networkObserver: networkObserver)

        let scanQRCodeView: ScanQRCode.View = ScanQRCodeView(viewModel: scanQRCodeViewModel)
        return ScanQRCodeViewController(rootView: scanQRCodeView, viewModel: scanQRCodeViewModel)
    }

    // Coffee Preview.
    func makeCoffeePreviewViewController(_ model: Coffee) -> CoffeePreviewViewController {
        let coffeePreviewViewModel: CoffeePreview.ViewModel = CoffeePreviewViewModel(model: model, networkObserver: networkObserver)

        let coffeePreviewView: CoffeePreview.View = CoffeePreviewView(viewModel: coffeePreviewViewModel)
        return CoffeePreviewViewController(rootView: coffeePreviewView, viewModel: coffeePreviewViewModel)
    }

    // Recipes List
    func makeRecipesListViewController() -> RecipesListViewController {
        let recipesListViewModel: RecipesList.ViewModel = RecipesListViewModel(networkObserver: networkObserver)

        let recipesListView: RecipesList.View = RecipesListView(viewModel: recipesListViewModel)
        return RecipesListViewController(rootView: recipesListView, viewModel: recipesListViewModel)
    }
}
