import Foundation

// MARK: - PATTERNS DEFINTION

enum RegExPattern: String {
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    case password = "[a-zA-Z0-9!@#$&`.+,]{8,50}"
}

// MARK: - CLASS DEFINITION

final class ValidationHelper {
    // MARK: - PRIVATE PROPERTIES

    private var fields = [(FieldType, String?)]()

    // MARK: - NAVIGATION CALLBACKS

    var doOnSuccess: (() -> Void)?
    var doOnError: ((ValidationResult) -> Void)?

    // MARK: - INITIALIZER

    init() {}

    // MARK: - CONFIGURATION

    /**
     * Add field to validate.
     */
    @discardableResult
    func addField(key: FieldType, value: String?) -> ValidationHelper {
        fields.append((key, value))
        return self
    }

    /**
     * Execute on validation success.
     */
    func onSuccess(success: @escaping (() -> Void)) -> ValidationHelper {
        doOnSuccess = success
        return self
    }

    /**
     * Execute on validation failed.
     */
    func onError(error: @escaping ((ValidationResult) -> Void)) -> ValidationHelper {
        doOnError = error
        return self
    }

    // MARK: - VALIDATION

    func validateForm() {
        var result = ValidationResult()

        // Loop through fields.
        fields.forEach { field, value in

            // Log current field.
            print("Validating field \(field) value \(String(describing: value))")

            // Check field type.
            switch field {
            case .firstName: result = validateName(result: result, field: field, value: value)
            case .lastName: result = validateName(result: result, field: field, value: value)
            case .email: result = validateEmail(result: result, field: field, value: value)
            case .phone: result = validatePhone(result: result, field: field, value: value)
            case .password: result = validateEmail(result: result, field: field, value: value)
            }
        }

        if result.isValid() {
            // Field validated successfully.
            doOnSuccess?()
        } else {
            // Field validation failed.
            doOnError?(result)
        }
    }

    // MARK: - PUBLIC METHODS

    // Email validation.

    private func validateEmail(result: ValidationResult, field: FieldType, value: String?) -> ValidationResult {
        // Check if value is not nil.
        guard let value = value else {
            _ = result.add(key: field, message: "")
            return result
        }

        // Setup regex validation.
        let emailRegEx = RegExPattern.email.rawValue
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        let isEmailValid = emailTest.evaluate(with: value)

        // Check if value is not empty.
        switch value.isEmpty {
        case true: _ = result.add(key: field, message: "")
        default: break
        }

        // Check if value has proper length.
        switch 1 ... 100 ~= value.count {
        case false: _ = result.add(key: field, message: "")
        default: break
        }

        // Check if value has proper format.
        switch isEmailValid {
        case false: _ = result.add(key: field, message: "")
        default: break
        }

        return result
    }

    // Name validation.

    private func validateName(result: ValidationResult, field: FieldType, value: String?) -> ValidationResult {
        // Check if value is not nil.
        guard let value = value else {
            _ = result.add(key: field, message: "invalid_name_empty".localized)
            return result
        }

        // Check if value has proper length.
        guard value.count >= 2 else {
            _ = result.add(key: field, message: "invalid_name_length".localized)
            return result
        }

        return result
    }

    // Phone validation.

    private func validatePhone(result: ValidationResult, field: FieldType, value: String?) -> ValidationResult {
        // Check if value is not nil.
        guard let value = value else {
            _ = result.add(key: field, message: "invalid_phone_empty".localized)
            return result
        }

        // Check if value contains only digits.
        switch CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: value)) {
        case true: _ = result.add(
            key: field,
            message: "invalid_phone_digits".localized
        )
        default: break
        }

        // Check if value has proper length.
        switch value.count > 0 {
        case false: _ = result.add(
            key: field,
            message: "invalid_phone_length".localized
        )
        default: break
        }

        return result
    }

    // Password validation.

    private func validatePassword(result: ValidationResult, field: FieldType, value: String?) -> ValidationResult {
        // Check if value is not nil.
        guard let value = value else {
            _ = result.add(key: field, message: "invalid_password".localized)
            return result
        }

        // Check if value has proper length.
        guard 2 ... 50 ~= value.count else {
            _ = result.add(key: field, message: "invalid_password_length".localized)
            return result
        }

        return result
    }
}

// MARK: - VALIDATION RESULT

final class ValidationResult {
    // Error.
    private var errors = [(FieldType, String)]()

    // Validation key.
    func add(key: FieldType, message: String) -> ValidationResult {
        errors.append((key, message))
        return self
    }

    // Error message.
    func message() -> String {
        return errors.first?.1 ?? emptyString
    }

    // Validation method.
    func isValid() -> Bool {
        return errors.isEmpty
    }
}

// MARK: - VALIDATION FIELD TYPES

enum FieldType {
    case firstName
    case lastName
    case email
    case phone
    case password
}
