import UIKit

class ClickableContentScrollView: UIScrollView {

    override init(frame _: CGRect) {
        super.init(frame: CGRect.zero)

        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func touchesShouldCancel(in view: UIView) -> Bool {
        if view.isKind(of: UITextField.self) || view.isKind(of: UIButton.self) {
            return true
        }
        return super.touchesShouldCancel(in: view)
    }
}
