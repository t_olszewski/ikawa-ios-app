import Foundation

// MARK: - STATUS CODES

enum StatusCode: Int {
    case ok = 200
    case noContent = 204
    case badRequest = 400
    case unauthorized = 401
    case forbidden = 403
    case notFound = 404
    case conflict = 409
    case validationError = 460
    case generalError = 461
    case businessError = 463
    case resourceNotFound = 464
    case internalServerError = 500

    var description: String {
        switch self {
        case .badRequest:
            return "bad_request_error".localized
        case .unauthorized:
            return "unauthorized".localized
        case .forbidden:
            return "forbidden".localized
        case .notFound:
            return "not_found".localized
        case .conflict:
            return "conflict".localized
        case .validationError:
            return "validation_error".localized
        case .generalError:
            return "general_error".localized
        case .businessError:
            return "business_error".localized
        case .resourceNotFound:
            return "resource_not_found".localized
        case .internalServerError:
            return "internal_server_error".localized

        default: return emptyString
        }
    }
}

// MARK: - VIEW TAG

enum ViewTag: Int {
    case activityIndicator = 1000
}

// MARK: - CUSTOM AUTH ERROR

struct NetworkingError: Error {
    let message: String
    let statusCode: StatusCode

    init(message: String, statusCode: StatusCode) {
        self.message = message
        self.statusCode = statusCode
    }
}
