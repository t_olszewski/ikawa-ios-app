import RxBus
import UIKit

// Constants.
let emptyString = ""
let zeroInt = 0
let zeroDouble = 0.0
let defaultColor: UIColor = .red

// MARK: - EVENTS DEFINITION

enum Events {
    struct LoggedOut: BusEvent {}
}

struct MockError: Error {
    let message: String

    init(message: String) {
        self.message = message
    }
}

extension MockError: LocalizedError {
    var errorDescription: String? {
        return NSLocalizedString(message, comment: "")
    }
}

func sketchSize(_ size: Float) -> Float {
    let screenWidth = Float(UIScreen.main.bounds.size.width)
    let iPhonePlusWidth = Float(414.0)
    let scale = screenWidth / iPhonePlusWidth
    let scaledSize = size * scale
    return abs(scaledSize) <= abs(size) ? scaledSize : size
}

/**
 Sketch is designed for iPhone 6,6s,7,8 size, so we need to adjust it for other sizes
 */
func sketchSizeHeight(_ size: Float) -> Float {
    let screenHeight = Float(UIScreen.main.bounds.size.height)
    let iPhoneHeight = Float(667.0)
    let scale = screenHeight / iPhoneHeight
    let scaledSize = size * scale
    return scaledSize
}

/**
 Sketch is designed for iPhone 6,6s,7,8 size, so we need to adjust it for other sizes
 */
func sketchSizeWidth(_ size: Float) -> Float {
    let screenWidth = Float(UIScreen.main.bounds.size.width)
    let iPhoneWidth = Float(375.0)
    let scale = screenWidth / iPhoneWidth
    let scaledSize = size * scale
    return scaledSize
}
