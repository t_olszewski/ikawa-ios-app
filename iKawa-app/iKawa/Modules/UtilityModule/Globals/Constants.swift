import Foundation
import UIKit

// Constants
struct Constants {
    // MARK: - KEYCHAIN

    static let keychainService: String = "com.compsoft.ikawa"

    // MARK: - DEFAULT VALUES Constants

    static let emptyString: String = ""

    // MARK: - UI ELEMENTS

    static let defaultLeading: Float = 20
    static let defaultTrailing: Float = -20
    static let defaultSepartorHeight: Float = 1
    static let defaultButtonHeight: Float = sketchSizeHeight(44)
    static let bigButtonHeight: Float = sketchSizeHeight(54)
    static let searchBarHeight: Float = sketchSizeHeight(40)
}
