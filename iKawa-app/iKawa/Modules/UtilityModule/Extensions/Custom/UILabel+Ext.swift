import UIKit

extension UILabel {
    convenience init(text: String? = nil, font: UIFont = .montRegular(ofSize: 16), textColor: UIColor? = .black, textAlignment: NSTextAlignment = .left, numberOfLines: Int = 1, minimumScaleFactor: CGFloat = 0.0) {
        self.init()
        self.text = text
        self.textAlignment = textAlignment
        self.numberOfLines = numberOfLines
        self.textColor = textColor
        self.font = font
        if minimumScaleFactor > 0 {
            adjustsFontSizeToFitWidth = true
            self.minimumScaleFactor = minimumScaleFactor
        }
    }

    static func regularLeft(text: String? = nil, fontSize: Double, textColor: UIColor? = .black, numberOfLines: Int = 1, minimumScaleFactor: CGFloat = 0.0) -> UILabel {
        let view: UILabel = .init(text: text, font: .montRegular(ofSize: CGFloat(fontSize)), textColor: textColor, numberOfLines: numberOfLines, minimumScaleFactor: minimumScaleFactor)
        view.isUserInteractionEnabled = true
        return view
    }

    static func regularCenter(text: String? = nil, fontSize: Double, textColor: UIColor? = .black, numberOfLines: Int = 1, minimumScaleFactor: CGFloat = 0.0) -> UILabel {
        return UILabel(text: text, font: .montRegular(ofSize: CGFloat(fontSize)), textColor: textColor, textAlignment: .center, numberOfLines: numberOfLines, minimumScaleFactor: minimumScaleFactor)
    }

    static func boldLeft(text: String? = nil, fontSize: Double, textColor: UIColor? = .black, numberOfLines: Int = 1, minimumScaleFactor: CGFloat = 0.0) -> UILabel {
        return UILabel(text: text, font: .montBlack(ofSize: CGFloat(fontSize)), textColor: textColor, numberOfLines: numberOfLines, minimumScaleFactor: minimumScaleFactor)
    }

    static func boldCenter(text: String = Constants.emptyString, fontSize: Double, textColor: UIColor? = .black, numberOfLines: Int = 1, minimumScaleFactor: CGFloat = 0.0) -> UILabel {
        return UILabel(text: text, font: .montBlack(ofSize: CGFloat(fontSize)), textColor: textColor, textAlignment: .center, numberOfLines: numberOfLines, minimumScaleFactor: minimumScaleFactor)
    }
}
