import UIKit

// MARK: - EXTENSION DEFINITION

extension UIViewController {
    var className: String {
        NSStringFromClass(classForCoder).components(separatedBy: ".").last ?? Constants.emptyString
    }

    class var toString: String {
        String(describing: self)
    }

    // Show custom error view.
    func showErrorMessage(title: String, message: String, closure: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            if let closure = closure {
                closure()
            }
        }))

        present(alert, animated: true, completion: nil)
    }

    // Show custom message view.
    func showConfirmationMessage(title: String, message: String, closure: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            if let closure = closure {
                closure()
            }
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))

        present(alert, animated: true, completion: nil)
    }

    func showBottomConfirmationMessage(title: String, subtitle: String, confirmMessage: String, confirm: (() -> Void)? = nil, cancel: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: confirmMessage, style: .default, handler: { _ in
            if let confirm = confirm {
                confirm()
            }
        }))

        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .destructive, handler: { _ in
            if let cancel = cancel {
                cancel()
            }
        }))

        present(alert, animated: true, completion: nil)
    }

    func showAlert(title: String? = nil, message: String? = nil, actions: [UIAlertAction] = [], preferredStyle: UIAlertController.Style = .alert) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        for action in actions {
            alertVC.addAction(action)
        }
        present(alertVC, animated: true)
    }

    // Add all subviews.
    func addSubviews(_ views: [UIView]) {
        _ = views.map {
            view.addSubview($0)
        }
    }

    // Show bottom pop up.
    func showBottomPopUp(text: String) {
        // Create pop up object.
        let popUp = BottomPopup(text: text)

        // Add popup to main view.
        view.addSubview(popUp)

        // Set constraints.
        popUp.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.height.equalTo(sketchSizeHeight(110))
        }

        // Set alpha for animation.
        popUp.alpha = 0

        // Animate.
        popUp.showAnimation()
    }

    // Show bottom error pop up.
    func showBottomErrorPopUp(text: String) {
        // Create pop up object.
        let popUp = BottomPopup(text: text)
        popUp.setError()

        // Add popup to main view.
        view.addSubview(popUp)

        // Set constraints.
        popUp.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.height.equalTo(sketchSizeHeight(110))
        }

        // Set alpha for animation.
        popUp.alpha = 0

        // Animate.
        popUp.showAnimation()
    }
}
