import UIKit

// MARK: - EXTENSION DEFINITION

extension UIColor {
    // Cobalt blue.
    open class var cobaltBlue: UIColor? {
        return UIColor(named: Colors.cobaltBlue)
    }

    // Cobalt iris.
    open class var iris: UIColor? {
        return UIColor(named: Colors.iris)
    }

    // Battleship grey.
    open class var battleshipGrey: UIColor? {
        return UIColor(named: Colors.battleshipGrey)
    }

    // Pale grey.
    open class var paleGrey: UIColor? {
        return UIColor(named: Colors.paleGrey)
    }

    // Silver.
    open class var silver: UIColor? {
        return UIColor(named: Colors.silver)
    }

    // Silver Two.
    open class var silverTwo: UIColor? {
        return UIColor(named: Colors.silverTwo)
    }

    // Gunmetal.
    open class var gunmetal: UIColor? {
        return UIColor(named: Colors.gunmetal)
    }

    // Corduroy.
    open class var corduroy: UIColor? {
        return UIColor(named: Colors.corduroy)
    }

    // Steel.
    open class var steel: UIColor? {
        return UIColor(named: Colors.steel)
    }

    // Pastel blue.
    open class var pastelBlue: UIColor? {
        return UIColor(named: Colors.pastelBlue)
    }

    // Steel grey.
    open class var steelGrey: UIColor? {
        return UIColor(named: Colors.steelGrey)
    }

    // Algae green.
    open class var algaeGreen: UIColor? {
        return UIColor(named: Colors.algaeGreen)
    }

    // Slate grey.
    open class var slateGrey: UIColor? {
        return UIColor(named: Colors.slateGrey)
    }

    // Silver three.
    open class var silverThree: UIColor? {
        return UIColor(named: Colors.silverThree)
    }

    // Blue ocean.
    open class var blueOcean: UIColor? {
        return UIColor(named: Colors.blueOcean)
    }

    // Plum.
    open class var plum: UIColor? {
        return UIColor(named: Colors.plum)
    }

    // Tomato.
    open class var tomato: UIColor? {
        return UIColor(named: Colors.tomato)
    }

    // Yellow orange.
    open class var yellowOrange: UIColor? {
        return UIColor(named: Colors.yellowOrange)
    }

    // Silver four.
    open class var silverFour: UIColor? {
        return UIColor(named: Colors.silverFour)
    }

    // Pampas
    open class var pampas: UIColor? {
        return UIColor(named: Colors.pampas)
    }

    // Athens Gray
    open class var athensGray: UIColor? {
        return UIColor(named: Colors.athensGray)
    }
}
