extension Array {
    func last() -> Int {
        count - 1
    }
}

extension Array where Element: Equatable {
    func subtracting(_ array: [Element]) -> [Element] {
        filter { !array.contains($0) }
    }
}
