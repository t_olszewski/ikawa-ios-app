import UIKit

extension UITableView {
    // Create custom table view.
    static func createTableView(style: UITableView.Style = .plain, cellClasses: [UITableViewCell.Type] = []) -> UITableView {
        // Set properties.
        let tableView = UITableView(frame: CGRect.zero, style: style)
        tableView.backgroundColor = .white
        tableView.sectionIndexBackgroundColor = .white
        tableView.showsVerticalScrollIndicator = false
        tableView.alwaysBounceVertical = false
        tableView.bounces = false
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.keyboardDismissMode = .onDrag
        cellClasses.forEach {
            tableView.register($0, forCellReuseIdentifier: String(describing: $0))
        }
        return tableView
    }
}
