import UIKit

// MARK: - EXTENSION DEFINITION

extension UIButton {
    static func standard(text: String) -> UIButton {
        let view = RoundedButton(
            text: text.localized,
            backgroundColor: .silver,
            textColor: .battleshipGrey
        )

        return view
    }

    static func highlighted(text: String) -> UIButton {
        let view = RoundedButton(
            text: text.localized,
            backgroundColor: .cobaltBlue,
            textColor: .white
        )

        return view
    }

    func setTitle(_ title: String?) {
        setTitle(title, for: .normal)
        setTitle(title, for: .highlighted)
        setTitle(title, for: .selected)
    }

    func setTitleColor(_ color: UIColor?) {
        setTitleColor(color, for: .normal)
        setTitleColor(color, for: .highlighted)
        setTitleColor(color, for: .selected)
    }

    func setImage(_ image: UIImage?) {
        setImage(image, for: .normal)
        setImage(image, for: .highlighted)
    }
}
