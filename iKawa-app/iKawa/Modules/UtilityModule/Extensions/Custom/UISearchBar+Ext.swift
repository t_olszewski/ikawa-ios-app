import UIKit

extension UISearchBar {
    convenience init(searchBarStyle: UISearchBar.Style, placeholder: String, font: UIFont = .montRegular(ofSize: 15)) {
        self.init()
        getTextField()?.font = font
        setTextField(color: .athensGray)
        self.searchBarStyle = searchBarStyle
        self.placeholder = placeholder
        layer.borderWidth = 1
        layer.borderColor = UIColor.white.cgColor
        returnKeyType = .done
    }

    func getTextField() -> UITextField? {
        return value(forKey: "searchField") as? UITextField
    }

    func setTextField(color: UIColor?) {
        guard let textField = getTextField() else { return }
        switch searchBarStyle {
        case .minimal:
            textField.layer.backgroundColor = color?.cgColor
            textField.layer.cornerRadius = 6
        case .prominent, .default:
            textField.backgroundColor = color
        @unknown default: break
        }
    }
}
