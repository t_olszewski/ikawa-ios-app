import UIKit

// MARK: - EXTENSION DEFINITION

extension UIPageViewController {
    func goToNextPage() {
        if let currentViewController = viewControllers?[0] {
            if let nextPage = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) {
                setViewControllers([nextPage], direction: .forward, animated: true, completion: nil)
            }
        }
    }
}
