import Foundation

// MARK: - EXTENSION DEFINITION

extension String {
    // Get localized string.
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
