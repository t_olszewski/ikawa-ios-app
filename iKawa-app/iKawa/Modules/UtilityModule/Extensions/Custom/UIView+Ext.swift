import NVActivityIndicatorView
import SnapKit
import UIKit

// MARK: - EXTENSION DEFINITION

extension UIView {
    // Get view name.
    static func toString() -> String {
        return String(describing: self)
    }

    // Fill parent view.
    func fillParent() {
        snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    // Get parent view controller.
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }

    // Add all subviews.
    func addSubviews(_ views: [UIView]) {
        _ = views.map {
            addSubview($0)
        }
    }

    // Create default activity indicator.
    func createActivityIndicator() -> NVActivityIndicatorView {
        let view = NVActivityIndicatorView(
            frame: CGRect.zero,
            type: .ballRotateChase,
            color: UIColor(named: Colors.cobaltBlue),
            padding: nil
        )
        view.tag = ViewTag.activityIndicator.rawValue
        return view
    }

    // Show loading progress.
    func showLoadingProgress() {
        if let activityIndicator = viewWithTag(ViewTag.activityIndicator.rawValue) as? NVActivityIndicatorView {
            activityIndicator.startAnimating()
            subviews.forEach { $0.isUserInteractionEnabled = false }
            subviews.forEach { $0.alpha = 0.7 }
        }
    }

    // Hide loading progress.
    func hideLoadingProgress() {
        if let activityIndicator = viewWithTag(ViewTag.activityIndicator.rawValue) as? NVActivityIndicatorView {
            activityIndicator.stopAnimating()
            subviews.forEach { $0.isUserInteractionEnabled = true }
            subviews.forEach { $0.alpha = 1.0 }
        }
    }
}

// MARK: - ANIMATIONS

extension UIView {
    func showAnimation() {
        // Start animation.
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            // Setup properties to animate.
            self.center.y -= CGFloat(sketchSizeHeight(110))
            self.alpha = 1
        }, completion: { (completed: Bool) -> Void in

            // Start hide animation after show animation is completed.
            if completed {
                self.hideAnimation()
            }
        })
    }

    func hideAnimation() {
        // Start animation.
        UIView.animate(withDuration: 0.3, delay: 0.5, options: [.curveLinear], animations: {
            // Setup properties to animate.
            self.center.y += CGFloat(sketchSizeHeight(110))
            self.alpha = 0
        }, completion: { (completed: Bool) -> Void in

            // Remove view form super view after animation is completed.
            if completed {
                self.removeFromSuperview()
            }
        })
    }
}
