import Foundation
import UIKit

extension NSAttributedString {
    static func attributedText(image: UIImage? = nil, bold: String, normal: String, fontSize: CGFloat = 15) -> NSAttributedString {
        let boldAttrs = [NSAttributedString.Key.font: UIFont.montBlack(ofSize: fontSize)]
        let boldAttributedString = NSMutableAttributedString(string: bold, attributes: boldAttrs)

        let normalAttrs = [NSAttributedString.Key.font: UIFont.montRegular(ofSize: fontSize)]
        let normalAttributedString = NSMutableAttributedString(string: normal, attributes: normalAttrs)

        let textAttributedString = NSMutableAttributedString()
        if let image = image {
            let imageAttachment = NSTextAttachment()
            imageAttachment.image = image
            imageAttachment.bounds = CGRect(x: 0, y: -10, width: image.size.width, height: image.size.height)
            let imageAttributedString = NSAttributedString(attachment: imageAttachment)
            textAttributedString.append(imageAttributedString)
        }
        textAttributedString.append(boldAttributedString)
        textAttributedString.append(normalAttributedString)
        return textAttributedString
    }
}
