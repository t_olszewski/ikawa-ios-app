import UIKit

extension UIView {
    convenience init(backgroundColor: UIColor?) {
        self.init()
        self.backgroundColor = backgroundColor
    }

    convenience init(backgroundColor: UIColor?, cornerRadius: CGFloat) {
        self.init(backgroundColor: backgroundColor)
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }

    convenience init(cornerRadius: CGFloat) {
        self.init()
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }

    convenience init(backgroundColor: UIColor?, cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: UIColor? = .white, isUserInteractionEnabled: Bool = true) {
        self.init(backgroundColor: backgroundColor)
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        if let borderColor = borderColor {
            layer.borderColor = borderColor.cgColor
        }
        clipsToBounds = true
        self.isUserInteractionEnabled = isUserInteractionEnabled
    }
}
