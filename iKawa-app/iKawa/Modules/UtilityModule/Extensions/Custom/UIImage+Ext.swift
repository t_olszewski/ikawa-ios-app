import UIKit

// MARK: - CLASS DEFINITION

extension UIImageView {
    static func aspectFit(image: UIImage?) -> UIImageView {
        let view = UIImageView(frame: CGRect.zero)
        view.image = image
        view.contentMode = .scaleAspectFit
        return view
    }

    convenience init(image: UIImage?, contentMode: UIView.ContentMode) {
        self.init(image: image)
        self.contentMode = contentMode
    }
}

class ImageWithoutRender: UIImage {
    override func withRenderingMode(_: UIImage.RenderingMode) -> UIImage {
        return self
    }
}
