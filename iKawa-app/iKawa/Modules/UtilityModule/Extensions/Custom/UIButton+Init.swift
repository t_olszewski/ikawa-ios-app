import UIKit

extension UIButton {
    convenience init(image: UIImage?, backgroundColor: UIColor? = .clear, cornerRadius: CGFloat = 0.0) {
        self.init()
        self.backgroundColor = backgroundColor
        setImage(image)
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }

    convenience init(image: UIImage?, backgroundColor: UIColor? = .clear) {
        self.init()
        self.backgroundColor = backgroundColor
        setImage(image)
    }

    convenience init(title: String = Constants.emptyString, font: UIFont = .montRegular(ofSize: 18), titleColor: UIColor? = .white, backgroundColor: UIColor? = .clear, cornerRadius: CGFloat = 0.0) {
        self.init()
        setTitle(title)
        titleLabel?.font = font
        if let titleColor = titleColor {
            setTitleColor(titleColor)
        }
        self.backgroundColor = backgroundColor
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }
}
