import UIKit

// MARK: - EXTENSION DEFINITION

extension UIFont {
    // Mont Bold
    open class func montBlack(ofSize fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: "Mont-Black", size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }

    // Mont Regular
    open class func montRegular(ofSize fontSize: CGFloat) -> UIFont {
        if let font = UIFont(name: "Mont-Regular", size: fontSize) {
            return font
        }
        return UIFont.systemFont(ofSize: fontSize)
    }
}
