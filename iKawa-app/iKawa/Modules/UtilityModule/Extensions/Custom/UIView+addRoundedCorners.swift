import UIKit

extension UIView {
    func addRoundedCorners(cornerRadius: CGFloat = 0,
                           borderWidth: CGFloat = 0,
                           borderColor: UIColor = .white)
    {
        layer.cornerRadius = cornerRadius == 0 ? frame.size.width / 2 : cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
}
