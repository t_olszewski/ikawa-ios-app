struct Coffee {
    var name: String
    var origin: String
    var description: String

    static var testModel: Coffee {
        return Coffee(name: "Las Mercedes", origin: "El Salvador", description: "test description #1")
    }
}
