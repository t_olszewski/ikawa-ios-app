import SwiftUI
import UIKit

// MARK: - CLASS DEFINITION

final class MainViewController: BaseViewController {
    // MARK: - PRIVATE PROPERTIES

    private let rootView: Main.View
    private let viewModel: Main.ViewModel

    // MARK: - INITIALIZERS

    init(rootView: Main.View, viewModel: Main.ViewModel) {
        // Assign properties.
        self.rootView = rootView
        self.viewModel = viewModel

        // Call super.
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - VIEW LIFECYCLE

    override func loadView() {
        super.loadView()

        // Create main view.
        createView()
    }

    // MARK: - PRIVATE METHODS

    private func createView() {
        // Add main view to view controller.
        view.addSubview(rootView)

        // Fill view with contraints.
        rootView.fillParent()
    }
}

// swiftlint:disable force_cast
#if DEBUG
    struct MainViewControllerContainerView: UIViewControllerRepresentable {
        typealias UIViewControllerType = MainViewController

        func makeUIViewController(context _: Context) -> UIViewControllerType {
            return (UIApplication.shared.delegate as! AppDelegate).dependenciesFactory.makeMainViewController()
        }

        func updateUIViewController(_: UIViewControllerType, context _: Context) {}
    }

    struct MainViewControllerContainerView_Previews: PreviewProvider {
        static var previews: some View {
            MainViewControllerContainerView().colorScheme(.light) // or .dark
        }
    }
#endif
// swiftlint:enable force_cast
