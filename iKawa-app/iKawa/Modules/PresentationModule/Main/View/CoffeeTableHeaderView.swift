import SwiftUI
import UIKit

class CoffeeTableHeaderView: UIView {
    // MARK: - PRIVATE PROPERTIES

    private let titleLabel: UILabel = .boldLeft(text: "Coffee library", fontSize: 24)
    private let searchBar: UISearchBar = .init(searchBarStyle: .default, placeholder: "Search")

    private var didSetupConstraints: Bool = false

    // MARK: - INITIALIZERS

    override init(frame _: CGRect) {
        super.init(frame: CGRect.zero)

        setupView()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - UI LAYOUT

extension CoffeeTableHeaderView {
    private func setupView() {
        backgroundColor = .white
        addSubviews()
        addConstraints()
    }

    private func addSubviews() {
        addSubviews([titleLabel, searchBar])
        setNeedsUpdateConstraints()
    }

    private func addConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(12)
            $0.leading.equalToSuperview().offset(28)
            $0.trailing.equalToSuperview().offset(-28)
        }

        searchBar.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(3)
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }

        didSetupConstraints = true
        super.updateConstraints()
    }
}

// MARK: SwiftUI Preview

#if DEBUG
    struct CoffeeTableHeaderViewContainer: UIViewRepresentable {
        typealias UIViewType = CoffeeTableHeaderView

        func makeUIView(context _: Context) -> UIViewType {
            return CoffeeTableHeaderView()
        }

        func updateUIView(_: CoffeeTableHeaderView, context _: Context) {}
    }

    struct CoffeeTableHeaderView_Previews: PreviewProvider {
        static var previews: some View {
            Group {
                CoffeeTableHeaderViewContainer()
                    .previewDevice("iPhone 8")
                CoffeeTableHeaderViewContainer()
                    .previewDevice("iPhone 8 Plus")
                CoffeeTableHeaderViewContainer()
                    .previewDevice("iPhone 12")
                CoffeeTableHeaderViewContainer()
                    .previewDevice("iPhone 12 Pro")
                CoffeeTableHeaderViewContainer()
                    .previewDevice("iPhone 12 Pro Max")
                CoffeeTableHeaderViewContainer()
                    .previewDevice("iPhone 12 Mini")
                CoffeeTableHeaderViewContainer()
                    .previewDevice("iPhone SE (2nd generation)")
            }.previewLayout(.fixed(width: UIScreen.main.bounds.width, height: 100))
        }
    }
#endif
