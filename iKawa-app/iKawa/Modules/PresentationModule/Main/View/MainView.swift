import RxCocoa
import RxSwift
import SnapKit
import SwiftUI
import UIKit

// MARK: PROTOCOL DEFINITION

protocol MainViewProtocol: UIView {}

// MARK: - CLASS DEFINITION

final class MainView: UIView, Main.View {
    // MARK: - PRIVATE PROPERTIES

    private var viewModel: Main.ViewModel

    // MARK: - UI ELEMENTS

    private let topLabel: UILabel = .boldCenter(text: "How are you going to\nchoose your coffee?", fontSize: 30, numberOfLines: 0)
    private let roundedView: RoundedView = .init(cornerRadius: 20, shadowOffset: CGSize(width: 10, height: 10), shadowRadius: 20, shadowOpacity: 1.0)
    private let scanQRCodeButton: UIButton = .init(image: UIImage(named: "scanQRCode"))
    private lazy var tableView: UITableView = createTableView()

    // MARK: - INTIALIZERS

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(viewModel: Main.ViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)

        setupView()
    }

    // MARK: - SETUP UI

    private func setupView() {
        addSubviews()
        addConstraints()
        bindUI()
    }
}

// MARK: - UI CREATION

extension MainView {

    private func createTableView() -> UITableView {
        let view: UITableView = .createTableView(cellClasses: [CoffeeLibraryTableViewCell.self])
        view.addRoundedCorners(cornerRadius: 20)
        view.delegate = self
        return view
    }
}

// MARK: - UI LAYOUT

extension MainView {
    private func addSubviews() {
        backgroundColor = .pampas
        roundedView.addSubviews([scanQRCodeButton])
        addSubviews([topLabel, roundedView, tableView])
    }

    private func addConstraints() {
        topLabel.snp.makeConstraints {
            $0.top.equalTo(safeAreaLayoutGuide.snp.top).offset(26)
            $0.leading.equalToSuperview().offset(Constants.defaultLeading).priority(.high)
            $0.trailing.equalToSuperview().offset(Constants.defaultTrailing).priority(.high)
        }

        roundedView.snp.makeConstraints {
            $0.top.equalTo(topLabel.snp.bottom).offset(20)
            $0.leading.equalToSuperview().offset(60).priority(.high)
            $0.trailing.equalToSuperview().offset(-60).priority(.high)
            $0.height.equalTo(200)
        }

        scanQRCodeButton.snp.makeConstraints {
            $0.center.equalToSuperview()
        }

        tableView.snp.makeConstraints {
            $0.top.equalTo(roundedView.snp.bottom).offset(28).priority(.high)
            $0.leading.trailing.equalToSuperview().priority(.high)
            $0.bottom.equalToSuperview()
        }
    }
}

// MARK: - UI BINDING

extension MainView {
    private func bindUI() {
        bindScanQRCodeButton()
        bindCoffees()
        bindItemSelection()
    }

    private func bindScanQRCodeButton() {
        scanQRCodeButton.tap.bind { [weak self] in
            self?.viewModel.appCoordinator?.mainFlowCoordicator?.showScanQRCodeViewController()
        }.disposed(by: viewModel.disposeBag)
    }

    private func bindCoffees() {
        viewModel.coffees.bind(to: tableView.rx.items(cellIdentifier: CoffeeLibraryTableViewCell.toString(), cellType: CoffeeLibraryTableViewCell.self)) { _, model, cell in
            cell.configure(model)
        }.disposed(by: viewModel.disposeBag)
    }

    private func bindItemSelection() {
        tableView.rx.modelSelected(Coffee.self).subscribe(onNext: { [weak self] model in
            self?.viewModel.appCoordinator?.mainFlowCoordicator?.showCoffeePreviewViewController(model)
        }).disposed(by: viewModel.disposeBag)
    }
}

// MARK: - DELEGATES

extension MainView: UITableViewDelegate {
    func tableView(_: UITableView, heightForHeaderInSection _: Int) -> CGFloat {
        return 100
    }

    func tableView(_: UITableView, viewForHeaderInSection _: Int) -> UIView? {
        let headerView = CoffeeTableHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        return headerView
    }
}

// MARK: SwiftUI Preview

#if DEBUG
    struct MainViewContainer: UIViewRepresentable {
        typealias UIViewType = MainView

        func makeUIView(context _: Context) -> UIViewType {
            return MainView(viewModel: MainViewModel())
        }

        func updateUIView(_: MainView, context _: Context) {}
    }

    struct MainViewContainer_Previews: PreviewProvider {
        static var previews: some View {
            Group {
                MainViewContainer()
                    .previewDevice("iPhone 8")
                MainViewContainer()
                    .previewDevice("iPhone 8 Plus")
                MainViewContainer()
                    .previewDevice("iPhone 12")
                MainViewContainer()
                    .previewDevice("iPhone 12 Pro")
                MainViewContainer()
                    .previewDevice("iPhone 12 Pro Max")
                MainViewContainer()
                    .previewDevice("iPhone 12 Mini")
                MainViewContainer()
                    .previewDevice("iPhone SE (2nd generation)")
            }
        }
    }
#endif
