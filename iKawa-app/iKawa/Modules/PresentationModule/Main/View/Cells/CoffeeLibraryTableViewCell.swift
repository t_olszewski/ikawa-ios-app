import SwiftUI
import UIKit

class CoffeeLibraryTableViewCell: BaseTableViewCell {
    // MARK: - PRIVATE PROPERTIES

    private var titleLabel: UILabel = .boldLeft(fontSize: 16)
    private var subtitleLabel: UILabel = .regularLeft(fontSize: 14, textColor: .steelGrey)
    private var nextImageView: UIImageView = .init(image: UIImage(named: "next"))
    private var separatorView: UIView = .init(backgroundColor: .silverTwo)

    private var didSetupConstraints: Bool = false

    // MARK: - INITIALIZERS

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - PUBLIC METHODS

    func configure(_ model: Coffee) {
        titleLabel.text = "\(model.name), \(model.origin)"
        subtitleLabel.text = model.description
    }
}

// MARK: - UI LAYOUT

extension CoffeeLibraryTableViewCell {
    private func setupView() {
        selectionStyle = .none
        addSubviews()
        addConstraints()
    }

    private func addSubviews() {
        contentView.addSubviews([titleLabel, subtitleLabel, nextImageView, separatorView])
    }

    private func addConstraints() {
        if !didSetupConstraints {
            titleLabel.snp.makeConstraints {
                $0.height.equalTo(20)
                $0.top.equalToSuperview().offset(24)
                $0.leading.equalToSuperview().offset(28)
                $0.trailing.equalTo(nextImageView.snp.leading).offset(-10)
            }
            subtitleLabel.snp.makeConstraints {
                $0.top.equalTo(titleLabel.snp.bottom).offset(8)
                $0.leading.trailing.equalTo(titleLabel)
            }
            nextImageView.snp.makeConstraints {
                $0.trailing.equalToSuperview().offset(-28)
                $0.centerY.equalToSuperview()
                $0.size.equalTo(CGSize(width: 36, height: 37))
            }
            separatorView.snp.makeConstraints {
                $0.top.equalTo(subtitleLabel.snp.bottom).offset(28)
                $0.bottom.equalToSuperview()
                $0.height.equalTo(Constants.defaultSepartorHeight)
                $0.leading.equalTo(titleLabel.snp.leading)
                $0.trailing.equalTo(nextImageView.snp.trailing)
            }
            didSetupConstraints = true
        }
    }
}

// MARK: SwiftUI Preview

#if DEBUG
    struct CoffeeLibraryTableViewCellContainer: UIViewRepresentable {
        typealias UIViewType = CoffeeLibraryTableViewCell

        func makeUIView(context _: Context) -> UIViewType {
            let cell = CoffeeLibraryTableViewCell()
            cell.configure(Coffee.testModel)
            return cell
        }

        func updateUIView(_: CoffeeLibraryTableViewCell, context _: Context) {}
    }

    struct CoffeeLibraryTableViewCell_Previews: PreviewProvider {
        static var previews: some View {
            Group {
                CoffeeLibraryTableViewCellContainer()
                    .previewDevice("iPhone 8")
                CoffeeLibraryTableViewCellContainer()
                    .previewDevice("iPhone 8 Plus")
                CoffeeLibraryTableViewCellContainer()
                    .previewDevice("iPhone 12")
                CoffeeLibraryTableViewCellContainer()
                    .previewDevice("iPhone 12 Pro")
                CoffeeLibraryTableViewCellContainer()
                    .previewDevice("iPhone 12 Pro Max")
                CoffeeLibraryTableViewCellContainer()
                    .previewDevice("iPhone 12 Mini")
                CoffeeLibraryTableViewCellContainer()
                    .previewDevice("iPhone SE (2nd generation)")
            }.previewLayout(.fixed(width: UIScreen.main.bounds.width, height: 100))
        }
    }
#endif
