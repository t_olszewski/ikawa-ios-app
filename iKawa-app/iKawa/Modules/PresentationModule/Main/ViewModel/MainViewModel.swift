import RxCocoa
import RxSwift

// MARK: - PROTOCOL DEFINITION

protocol MainViewModelProtocol: AnyObject {
    var coffees: BehaviorRelay<[Coffee]> { get }
}

// MARK: - CLASS DEFINITION

final class MainViewModel: Main.ViewModel {

    var coffees = BehaviorRelay<[Coffee]>(value: [])

    // MARK: - PRIVATE PROPERTIES

    private let networkObserver: Network.Observer?

    // MARK: - INITIALIZER

    init(networkObserver: Network.Observer? = nil) {
        self.networkObserver = networkObserver

        coffees.accept([Coffee(name: "Las Mercedes", origin: "El Salvador", description: "test description #1"),
                        Coffee(name: "Ayehu Farm", origin: "Etiopia", description: "test description #2"),
                        Coffee(name: "Dumerso", origin: "Etiopia", description: "test description #3"),
                        Coffee(name: "Seleccion Poaquil", origin: "Guatemala", description: "test description #4"),
                        Coffee(name: "test name #5", origin: "El Salvador", description: "test description #5"),
                        Coffee(name: "test name #6", origin: "El Salvador", description: "test description #6"),
                        Coffee(name: "test name #7", origin: "El Salvador", description: "test description #7"),
                        Coffee(name: "test name #8", origin: "El Salvador", description: "test description #8"),
                        Coffee(name: "test name #9", origin: "El Salvador", description: "test description #9"),
                        Coffee(name: "test name #10", origin: "El Salvador", description: "test description #10"),
                        Coffee(name: "test name #11", origin: "El Salvador", description: "test description #11"),
                        Coffee(name: "test name #12", origin: "El Salvador", description: "test description #12")])
    }
}
