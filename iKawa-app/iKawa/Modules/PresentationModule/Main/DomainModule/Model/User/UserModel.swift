struct UserModel {
    // MARK: - PUBLIC PROPERTIES

    var id: Int
    var userName: String
    var email: String
    var role: String

    // MARK: - PUBLIC INITIALIZER

    init(object: UserModelRealm) {
        id = object.id
        userName = object.userName
        email = object.email
        role = object.role
    }

    init(model: UserModel) {
        id = model.id
        userName = model.userName
        email = model.email
        role = model.role
    }

    init() {
        id = zeroInt
        userName = emptyString
        email = emptyString
        role = emptyString
    }
}

// MARK: - EQUATABLE

extension UserModel: Equatable {
    public static func == (lhs: UserModel, rhs: UserModel) -> Bool {
        return lhs.id == rhs.id
    }
}
