struct CartProductStatusModel {
    let statusType: StatusType
    let statusTitle: String
    let statusSubitle: String
    let statusImageName: String
}
