import RxSwift

// MARK: - PROTOCOL DEFINITION

protocol AuthDomainProtocol {
    func setFirstAppLunch()
    func firstAppLunch() -> Bool
    func storeSessionToken(token: String) -> Completable
    func logout() -> Completable
    func parseNetworkError(error: Error) -> NetworkingError?
    func loginUser(model: AuthModel) -> Completable
    func userProfile(token: String) -> Completable
    func updatedUserProfile(model: UserModel) -> Completable
    func observeUserProfile() -> Observable<UserModel>
    func isUserLogged() -> Single<Bool>
    func validateAccessToken() -> Completable
}

// MARK: - CLASS DEFINITION

final class AuthDomainManager: Domain.Authentication {
    // MARK: - PRIVATE PROPERTIES

    private let authenticationManager: Network.Authentication
    private let networkExceptionManager: Network.Exception
    private let userStorageManager: Persistence.User
    private let keychainManager: Persistence.Session

    // MARK: - PUBLIC INITIALIZER

    init(
        authenticationManager: Network.Authentication,
        networkExceptionManager: Network.Exception,
        userStorageManager: Persistence.User,
        keychainManager: Persistence.Session
    ) {
        self.authenticationManager = authenticationManager
        self.networkExceptionManager = networkExceptionManager
        self.userStorageManager = userStorageManager
        self.keychainManager = keychainManager
    }

    // MARK: - PUBLIC METHODS

    // Parse network error.

    func parseNetworkError(error: Error) -> NetworkingError? {
        return networkExceptionManager.parseNetworkError(error: error)
    }

    // First app lunch.

    func setFirstAppLunch() {
        keychainManager.setFirstAppLunch()
    }

    func firstAppLunch() -> Bool {
        keychainManager.firstAppLunch()
    }

    // Store access token.

    func storeSessionToken(token: String) -> Completable {
        keychainManager
            .storeSessionToken(token: token)
            .do(afterError: { [weak self] _ in
                self?.keychainManager.clearSeesionTokenSync()
            })
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
    }

    func logout() -> Completable {
        keychainManager
            .clearAll()
            .andThen(userStorageManager.deleteUserModel())
    }

    // Login user.

    func loginUser(model: AuthModel) -> Completable {
        authenticationManager
            .loginUser(request: AuthRequest(model: model))
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .flatMapCompletable { [weak self] (userResponse) -> Completable in
                guard let self = self else { return Completable.empty() }
                return Completable.zip(
                    self.userStorageManager
                        .createUserModel(model: UserModelRealm(model: userResponse)),
                    self.storeSessionToken(token: userResponse.accessToken)
                )
            }
    }

    // Store user profile.

    func userProfile(token: String) -> Completable {
        authenticationManager
            .userProfile(token: token)
            .asCompletable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
    }

    // Updated user profile.

    func updatedUserProfile(model: UserModel) -> Completable {
        userStorageManager
            .updateUserModel(model: UserModelRealm(model: model))
    }

    // Observe user profile.

    func observeUserProfile() -> Observable<UserModel> {
        userStorageManager
            .observeUserProfile()
            .map { UserModel(object: $0) }
    }

    // Login status.

    func isUserLogged() -> Single<Bool> {
        keychainManager
            .getSessionToken()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .map { token in
                token.count > 0
            }
    }

    func validateAccessToken() -> Completable {
        keychainManager
            .getSessionToken()
            .flatMapCompletable { [weak self] (token) -> Completable in
                guard let self = self else { return Completable.empty() }
                return self.userProfile(token: token)
            }
    }
}
