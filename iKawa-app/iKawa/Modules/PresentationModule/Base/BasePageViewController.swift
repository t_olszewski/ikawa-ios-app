import DeallocationChecker
import UIKit

// MARK: - CLASS DEFINITION

class BasePageViewController: UIPageViewController {
    // MARK: - VIEW LIFECYCLE

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        // Check deallocation status.
        DeallocationChecker.shared.checkDeallocation(of: self)
    }
}
