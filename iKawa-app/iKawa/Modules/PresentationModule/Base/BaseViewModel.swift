import RxCocoa
import RxSwift
import UIKit

protocol BaseViewModelProtocol {
    var appCoordinator: Coordinator.Base? { get }
    var disposeBag: DisposeBag { get }
}

class BaseViewModel: BaseViewModelProtocol {
    var disposeBag = DisposeBag()
}

extension BaseViewModelProtocol {
    var appCoordinator: Coordinator.Base? {
        (UIApplication.shared.delegate as? AppDelegate)?.applicationCoordinator
    }
}
