import DeallocationChecker
import UIKit

// MARK: - CLASS DEFINITION

class BaseViewController: UIViewController {
    // MARK: - VIEW LIFECYCLE

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        DeallocationChecker.shared.checkDeallocation(of: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print(className)
    }
}
