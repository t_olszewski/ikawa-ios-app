import SwiftUI
import UIKit

// MARK: - CLASS DEFINITION

final class CoffeePreviewViewController: BaseViewController {
    // MARK: - PRIVATE PROPERTIES

    private let rootView: CoffeePreview.View
    private let viewModel: CoffeePreview.ViewModel

    // MARK: - INITIALIZERS

    init(rootView: CoffeePreview.View, viewModel: CoffeePreview.ViewModel) {
        // Assign properties.
        self.rootView = rootView
        self.viewModel = viewModel

        // Call super.
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - VIEW LIFECYCLE

    override func loadView() {
        super.loadView()

        // Create main view.
        createView()
    }

    // MARK: - PRIVATE METHODS

    private func createView() {
        // Add main view to view controller.
        view.addSubview(rootView)

        // Fill view with contraints.
        rootView.fillParent()
    }
}

// swiftlint:disable force_cast
#if DEBUG
    struct CoffeePreviewViewControllerContainerView: UIViewControllerRepresentable {
        typealias UIViewControllerType = CoffeePreviewViewController

        func makeUIViewController(context _: Context) -> UIViewControllerType {
            return (UIApplication.shared.delegate as! AppDelegate).dependenciesFactory.makeCoffeePreviewViewController(Coffee.testModel)
        }

        func updateUIViewController(_: UIViewControllerType, context _: Context) {}
    }

    struct CoffeePreviewViewControllerContainerView_Previews: PreviewProvider {
        static var previews: some View {
            CoffeePreviewViewControllerContainerView().colorScheme(.light) // or .dark
        }
    }
#endif
// swiftlint:enable force_cast
