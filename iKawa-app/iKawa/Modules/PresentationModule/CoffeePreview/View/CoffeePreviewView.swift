import NVActivityIndicatorView
import RxCocoa
import RxSwift
import SnapKit
import SwiftUI
import UIKit

// MARK: PROTOCOL DEFINITION

protocol CoffeePreviewViewProtocol: UIView {}

// MARK: - CLASS DEFINITION

final class CoffeePreviewView: UIView, CoffeePreview.View {
    // MARK: - PRIVATE PROPERTIES

    private var viewModel: CoffeePreview.ViewModel

    // MARK: - UI ELEMENTS

    private let scrollView = ClickableContentScrollView()
    private let contentView = UIView()
    private lazy var mainView: UIView = createMainView()
    private let coffeeLabel: UILabel = .regularLeft(text: "Coffee", fontSize: 16, textColor: .tomato)
    private lazy var coffeeNameLabel: UILabel = .boldLeft(text: viewModel.model.name, fontSize: 30)
    private lazy var coffeeOriginLabel: UILabel = .regularLeft(text: viewModel.model.origin, fontSize: 26, textColor: .corduroy)
    private lazy var producerLabel: UILabel = createProducerLabel()
    private lazy var varietiesLabel: UILabel = createVarietiesLabel()
    private lazy var processLabel: UILabel = createProcessLabel()
    private lazy var elevationLabel: UILabel = createElevationLabel()
    private let learnMoreLabel: UILabel = .regularLeft(text: "Learn more", fontSize: 26, textColor: .tomato)
    private let descriptionLabel: UILabel = .regularLeft(text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed arcu tristique metus tincidunt faucibus. Phasellus non posuere elit, id fermentum libero. Nunc eu risus sed nulla dictum porttitor nec non nibh. Phasellus orci nibh, euismod sit amet porttitor vel, dignissim non urna. Proin suscipit velit a erat blandit, convallis maximus nibh iaculis. Duis volutpat nisl arcu, eget eleifend mauris venenatis eget. Duis et lorem facilisis nunc posuere mollis ac eget orci.", fontSize: 15, numberOfLines: 0)
    private let chooseRecipeButton: RoundedButton = .init(text: "Choose recipe", backgroundColor: .tomato, textColor: .white, fontSize: 19)

    // MARK: - INTIALIZERS

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(viewModel: CoffeePreview.ViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)

        setupView()
    }

    // MARK: - SETUP UI

    private func setupView() {
        backgroundColor = .pampas
        addSubviews()
        addConstraints()
        bindUI()
    }

    private func attributedText(image: UIImage?, bold: String, normal: String, fontSize: CGFloat = 15) -> NSAttributedString {
        return NSAttributedString.attributedText(image: image, bold: "      \(bold):  ", normal: normal, fontSize: fontSize)
    }
}

// MARK: - UI CREATION

extension CoffeePreviewView {
    private func createMainView() -> UIView {
        let view = UIView(backgroundColor: .white)
        view.roundCorners([.topLeft, .topRight], radius: 60)
        return view
    }

    private func createProducerLabel() -> UILabel {
        let view: UILabel = .regularLeft(fontSize: 16)
        view.attributedText = attributedText(image: UIImage(named: "producerIcon"), bold: "Producer", normal: "Jose Fernando Aguilar")
        return view
    }

    private func createVarietiesLabel() -> UILabel {
        let view: UILabel = .regularLeft(fontSize: 16)
        view.attributedText = attributedText(image: UIImage(named: "varietiesIcon"), bold: "Varieties", normal: "Marsellesa")
        return view
    }

    private func createProcessLabel() -> UILabel {
        let view: UILabel = .regularLeft(fontSize: 16)
        view.attributedText = attributedText(image: UIImage(named: "processIcon"), bold: "Process", normal: "Semi-washed 'Honey'")
        return view
    }

    private func createElevationLabel() -> UILabel {
        let view: UILabel = .regularLeft(fontSize: 16)
        view.attributedText = attributedText(image: UIImage(named: "elevationIcon"), bold: "Elevation", normal: "1,3500 - 1,700 masl")
        return view
    }
}

// MARK: - UI LAYOUT

extension CoffeePreviewView {
    private func addSubviews() {
        backgroundColor = .pampas
        contentView.backgroundColor = .pampas

        addSubviews([scrollView])
        scrollView.addSubview(contentView)
        contentView.addSubviews([mainView])
        mainView.addSubviews([coffeeLabel, coffeeNameLabel, coffeeOriginLabel, producerLabel, varietiesLabel, processLabel, elevationLabel, learnMoreLabel, descriptionLabel, chooseRecipeButton])
    }

    private func addConstraints() {
        scrollView.snp.makeConstraints {
            $0.top.equalTo(safeAreaLayoutGuide.snp.top)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(safeAreaLayoutGuide.snp.bottom)
        }

        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalToSuperview().priority(.low)
            $0.width.equalToSuperview().priority(.high)
        }

        mainView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(180)
            $0.leading.trailing.bottom.equalToSuperview()
        }

        coffeeLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(30)
            $0.leading.equalToSuperview().offset(50)
            $0.trailing.equalToSuperview().offset(-50)
        }

        coffeeNameLabel.snp.makeConstraints {
            $0.top.equalTo(coffeeLabel.snp.bottom).offset(20)
            $0.leading.trailing.equalTo(coffeeLabel)
        }

        coffeeOriginLabel.snp.makeConstraints {
            $0.top.equalTo(coffeeNameLabel.snp.bottom).offset(16)
            $0.leading.trailing.equalTo(coffeeLabel)
        }

        producerLabel.snp.makeConstraints {
            $0.top.equalTo(coffeeOriginLabel.snp.bottom).offset(30)
            $0.leading.trailing.equalTo(coffeeLabel)
            $0.height.equalTo(40)
        }

        varietiesLabel.snp.makeConstraints {
            $0.top.equalTo(producerLabel.snp.bottom).offset(4)
            $0.leading.trailing.equalTo(producerLabel)
            $0.height.equalTo(producerLabel.snp.height)
        }

        processLabel.snp.makeConstraints {
            $0.top.equalTo(varietiesLabel.snp.bottom).offset(4)
            $0.leading.trailing.equalTo(producerLabel)
            $0.height.equalTo(producerLabel.snp.height)
        }

        elevationLabel.snp.makeConstraints {
            $0.top.equalTo(processLabel.snp.bottom).offset(4)
            $0.leading.trailing.equalTo(producerLabel)
            $0.height.equalTo(producerLabel.snp.height)
        }

        learnMoreLabel.snp.makeConstraints {
            $0.top.equalTo(elevationLabel.snp.bottom).offset(40)
            $0.leading.trailing.equalTo(coffeeLabel)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(learnMoreLabel.snp.bottom).offset(34)
            $0.leading.trailing.equalTo(coffeeLabel)
        }

        chooseRecipeButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(34)
            $0.height.equalTo(Constants.defaultButtonHeight)
            $0.leading.trailing.equalTo(coffeeLabel)
            $0.bottom.equalToSuperview().offset(-60)
        }
    }
}

// MARK: - UI BINDING

extension CoffeePreviewView {
    private func bindUI() {
        chooseRecipeButton.tap.bind { [weak self] in
            self?.viewModel.appCoordinator?.mainFlowCoordicator?.showRecipesListViewController()
        }.disposed(by: viewModel.disposeBag)
    }
}

// MARK: SwiftUI Preview

#if DEBUG
    struct CoffeePreviewViewContainer: UIViewRepresentable {
        typealias UIViewType = CoffeePreviewView

        func makeUIView(context _: Context) -> UIViewType {
            return CoffeePreviewView(viewModel: CoffeePreviewViewModel(model: Coffee.testModel))
        }

        func updateUIView(_: CoffeePreviewView, context _: Context) {}
    }

    struct CoffeePreviewViewContainer_Previews: PreviewProvider {
        static var previews: some View {
            CoffeePreviewViewContainer()
            /*
             Group {
                 CoffeePreviewViewContainer()
                     .previewDevice("iPhone 8")
                 CoffeePreviewViewContainer()
                     .previewDevice("iPhone 8 Plus")
                 CoffeePreviewViewContainer()
                     .previewDevice("iPhone 12")
                 CoffeePreviewViewContainer()
                     .previewDevice("iPhone 12 Pro")
                 CoffeePreviewViewContainer()
                     .previewDevice("iPhone 12 Pro Max")
                 CoffeePreviewViewContainer()
                     .previewDevice("iPhone 12 Mini")
                 CoffeePreviewViewContainer()
                     .previewDevice("iPhone SE (2nd generation)")
             }*/
        }
    }
#endif
