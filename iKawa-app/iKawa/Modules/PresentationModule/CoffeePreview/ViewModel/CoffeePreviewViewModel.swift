import RxCocoa
import RxSwift

// MARK: - PROTOCOL DEFINITION

protocol CoffeePreviewViewModelProtocol {
    var model: Coffee { get }
}

// MARK: - CLASS DEFINITION

final class CoffeePreviewViewModel: CoffeePreview.ViewModel {

    // MARK: - PUBLIC PROPERTIES

    let model: Coffee

    // MARK: - PRIVATE PROPERTIES

    private let networkObserver: Network.Observer?

    // MARK: - INITIALIZER

    init(model: Coffee, networkObserver: Network.Observer? = nil) {
        self.model = model
        self.networkObserver = networkObserver
    }
}
