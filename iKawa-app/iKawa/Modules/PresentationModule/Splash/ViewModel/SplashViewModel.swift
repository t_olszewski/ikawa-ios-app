import RxCocoa
import RxSwift

// MARK: - PROTOCOL DEFINITION

protocol SplashViewModelProtocol {
    var onUserLogged: (() -> Void)? { get set }
    var onLoginRequired: (() -> Void)? { get set }
    func checkLoginStatus()
}

// MARK: - CLASS DEFINITION

final class SplashViewModel: Splash.ViewModel {
    // MARK: - NAVIGATION CALLBACKS

    var onUserLogged: (() -> Void)?
    var onLoginRequired: (() -> Void)?

    // MARK: - PRIVATE PROPERTIES

    private let authDomainManager: Domain.Authentication

    // MARK: - INITIALIZER

    init(authDomainManager: Domain.Authentication) {
        self.authDomainManager = authDomainManager
    }

    // MARK: - PUBLIC FUNCTIONS

    func checkLoginStatus() {
        authDomainManager
            .isUserLogged()
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] event in
                switch event {
                case let .success(isLogged):

                    // handle succes.
                    self?.handleIsLoggedSuccess(isLogged: isLogged)

                case let .error(error):
                    // Handle error.
                    self?.handleIsLoggedError(error: error)
                }
            }
            .disposed(by: disposeBag)
    }

    // MARK: - PRIVATE METHODS

    private func checkAccessTokenStatus() {
        authDomainManager
            .validateAccessToken()
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] event in
                switch event {
                case .completed:

                    // handle succes.
                    self?.handleAccessTokenSuccess()

                case let .error(error):
                    // Handle error.
                    self?.handleAccessTokenError(error: error)
                }
            }
            .disposed(by: disposeBag)
    }

    private func logout() {
        authDomainManager
            .logout()
            .subscribe { [weak self] event in
                switch event {
                case .completed:
                    // Handle succes.
                    self?.handleLogoutSuccess()

                case let .error(error):

                    // Handle error.
                    self?.handleLogoutError(error: error)
                }
            }
            .disposed(by: disposeBag)
    }

    private func handleIsLoggedSuccess(isLogged: Bool) {
        // Log the fact.
        print("Checking login status success. Login status = \(isLogged)")

        // Act based on login status.
        switch isLogged {
        case true:
            checkAccessTokenStatus()
        case false:
            onLoginRequired?()
        }
    }

    private func handleIsLoggedError(error: Error) {
        // Log the fact.
        print("Checking login status failed. Error = \(error.localizedDescription)")
    }

    private func handleAccessTokenSuccess() {
        // Log the fact.
        print("Checking access token success. Token is valid")

        // Send user to the main view.
        onUserLogged?()
    }

    private func handleAccessTokenError(error: Error) {
        // Get network error.
        let parsedError = authDomainManager.parseNetworkError(error: error)

        // Log the fact.
        print("Access token can be invalid. Error code = \(String(describing: parsedError?.statusCode))")

        // Log out user.
        logout()
    }

    private func handleLogoutSuccess() {
        // Log the fact.
        print("Logout user success.")

        // Send user to the login view.
        onLoginRequired?()
    }

    private func handleLogoutError(error: Error) {
        // Log the fact.
        print("Logout user error. Error = \(error.localizedDescription)")
    }
}
