import UIKit

// MARK: - CLASS DEFINITION

final class SplashViewController: BaseViewController {
    // MARK: - PRIVATE PROPERTIES

    private let rootView: Splash.View
    private let viewModel: Splash.ViewModel

    // MARK: - INITIALIZERS

    init(rootView: Splash.View, viewModel: Splash.ViewModel) {
        // Assign properties.
        self.rootView = rootView
        self.viewModel = viewModel

        // Call super.
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - VIEW LIFECYCLE

    override func loadView() {
        super.loadView()

        // Create main view.
        createView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // Check user login status.
        viewModel.checkLoginStatus()
    }

    // MARK: - PRIVATE METHODS

    private func createView() {
        // Add main view to view controller.
        view.addSubview(rootView)

        // Fill view with contraints.
        rootView.fillParent()
    }
}
