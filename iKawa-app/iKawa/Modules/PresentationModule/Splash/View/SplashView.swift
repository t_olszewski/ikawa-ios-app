import RxCocoa
import RxSwift
import SnapKit
import UIKit

// MARK: PROTOCOL DEFINITION

protocol SplashViewProtocol: UIView {}

// MARK: - CLASS DEFINITION

final class SplashView: UIView, Splash.View {
    // MARK: - PRIVATE PROPERTIES

    private var viewModel: Splash.ViewModel

    // MARK: - UI ELEMENTS

    private lazy var logoImageView = createLogoImageView()

    // MARK: - INTIALIZERS

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(viewModel: Splash.ViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)

        setupView()
    }

    // MARK: - SETUP UI

    private func setupView() {
        addSubviews()
        addConstraints()
    }
}

// MARK: - UI CREATION

extension SplashView {
    private func createLogoImageView() -> UIImageView {
        let view = UIImageView(frame: CGRect.zero)
        let image: UIImage = #imageLiteral(resourceName: "logo")
        view.image = image
        view.contentMode = .scaleAspectFill
        return view
    }
}

// MARK: - UI LAYOUT

extension SplashView {
    private func addSubviews() {
        backgroundColor = .white

        addSubview(logoImageView)
    }

    private func addConstraints() {
        logoImageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
            $0.height.equalTo(sketchSizeHeight(200))
            $0.width.equalTo(sketchSizeHeight(200))
        }
    }
}
