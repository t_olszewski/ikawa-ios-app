import RxCocoa
import RxSwift
import UIKit

// MARK: - CLASS DEFINITION

final class RootOnboardingViewController: UIPageViewController {
    // MARK: - PRIVATE PROPERTIES

    private var viewModel: Onboarding.ViewModel
    enum ButtonType { case next, start }

    // MARK: - UI ELEMENTS

    private var onboardingViewControllers = [UIViewController]()
    private lazy var pageControl = cratePageController()
    private lazy var nextButton = createNextButton(text: "next".localized, isHidden: false, buttonType: .next)
    private lazy var getStartedButton = createNextButton(text: "get_started".localized, isHidden: true, buttonType: .start)
    private lazy var skipButton = createSkipButton()

    // MARK: - INITIALIZER

    init(viewModel: Onboarding.ViewModel) {
        self.viewModel = viewModel
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal)

        setupDetailsControllers()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - VIEW LIFECYCLE

    override func viewDidLoad() {
        super.viewDidLoad()

        // How we can chain this? Try rx chaining
        // Setup delegates.
        setupDelegates()

        // Setup page view controller.
        setupPageViewController()

        // Add subiews.
        addSubviews()

        // Add constraints.
        addConstraints()
    }

    // MARK: PRIVATE METHODS

    private func setupDelegates() {
        // Setup delegates for page view controller.
        dataSource = self
        delegate = self
    }

    private func setupPageViewController() {
        // Get first detail view.
        guard let firstViewController = onboardingViewControllers.first else { return }

        // Set first view controller.
        setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
    }

    // MARK: - INTERACTIONS

    private func onSkipTap() {
        // Call navigation callback.
        viewModel.skipOnBoarding()
    }

    private func onNextPageTap() {
        // Show next page.
        goToNextPage()
        pageControl.currentPage += 1

        // Handle button states.
        handleButtonsState(index: pageControl.currentPage)
    }
}

// MARK: - UI CREATION

extension RootOnboardingViewController {
    private func cratePageController() -> UIPageControl {
        // Crate new page view.
        let view = UIPageControl(frame: .zero)

        // Configure page view.
        view.numberOfPages = onboardingViewControllers.count
        view.currentPage = 0
        view.pageIndicatorTintColor = .iris
        view.currentPageIndicatorTintColor = .white
        view.isUserInteractionEnabled = false

        return view
    }

    private func setupDetailsControllers() {
        // Setup all detail views.
        let availabilityVc = DetailOnboardingViewController(
            viewModel: viewModel,
            configuration: (title: "availability_title_onboarding".localized,
                            icon: #imageLiteral(resourceName: "failMark"),
                            description: "availability_description".localized)
        )

        let searchVc = DetailOnboardingViewController(
            viewModel: viewModel,
            configuration: (title: "search_title".localized,
                            icon: #imageLiteral(resourceName: "failMark"),
                            description: "search_description".localized)
        )

        let changeVc = DetailOnboardingViewController(
            viewModel: viewModel,
            configuration: (title: "change_title".localized,
                            icon: #imageLiteral(resourceName: "failMark"),
                            description: "change_description".localized)
        )

        let ggpVc = DetailOnboardingViewController(
            viewModel: viewModel,
            configuration: (title: "profit_title_onboarding".localized,
                            icon: #imageLiteral(resourceName: "failMark"),
                            description: "profit_description".localized)
        )

        let sharingVc = DetailOnboardingViewController(
            viewModel: viewModel,
            configuration: (title: "sharing_title".localized,
                            icon: #imageLiteral(resourceName: "failMark"),
                            description: "sharing_description".localized)
        )

        // Store details in one collection.
        onboardingViewControllers = [
            availabilityVc, searchVc, changeVc, ggpVc, sharingVc,
        ]
    }

    private func createNextButton(text: String, isHidden: Bool, buttonType: ButtonType) -> RoundedButton {
        // Setup view properties.
        let view = RoundedButton(text: text)
        view.isHidden = isHidden

        // Add tap button target.
        view.rx.tap.throttle(.seconds(1), scheduler: MainScheduler.instance).bind { [weak self] in

            switch buttonType {
            case .next:
                self?.onNextPageTap()
            case .start:
                self?.onSkipTap()
            }
        }.disposed(by: viewModel.disposeBag)

        return view
    }

    private func createSkipButton() -> UIButton {
        // Setup view properties.
        let view = UIButton(type: .system)
        view.titleLabel?.adjustsFontSizeToFitWidth = true
        view.titleLabel?.minimumScaleFactor = 0.5
        view.titleLabel?.font = .montRegular(ofSize: 20)
        view.titleLabel?.textAlignment = .center
        view.setTitle("skip".localized, for: .normal)
        view.setTitleColor(.white)
        view.isUserInteractionEnabled = true

        // Add tap button target.
        view.rx.tap.throttle(.seconds(1), scheduler: MainScheduler.instance).bind { [weak self] in
            self?.viewModel.skipOnBoarding()
        }.disposed(by: viewModel.disposeBag)

        return view
    }
}

// MARK: - UI LAYOUT

extension RootOnboardingViewController {
    private func addSubviews() {
        // Set background.
        view.backgroundColor = .cobaltBlue

        // Create subviews collection.
        let subViews = [
            skipButton, pageControl, nextButton, getStartedButton,
        ]

        // Add all subviews.
        addSubviews(subViews)
    }

    private func addConstraints() {
        // Set constraints for the skip button.
        skipButton.snp.makeConstraints {
            $0.top.equalToSuperview().offset(sketchSizeHeight(26))
            $0.trailing.equalToSuperview().offset(sketchSizeWidth(-10))
            $0.width.equalTo(sketchSizeWidth(40))
        }

        // Set constraints for the page control.
        pageControl.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(sketchSizeHeight(-80))
            $0.centerX.equalToSuperview()
            $0.leading.equalToSuperview().offset(sketchSizeWidth(Constants.defaultLeading))
            $0.trailing.equalToSuperview().offset(-sketchSizeWidth(Constants.defaultLeading))
        }

        // Set constraints for the next button.
        nextButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(sketchSizeHeight(-25))
            $0.centerX.equalToSuperview()
            $0.leading.equalToSuperview().offset(sketchSizeWidth(Constants.defaultLeading))
            $0.trailing.equalToSuperview().offset(-sketchSizeWidth(Constants.defaultLeading))
            $0.height.equalTo(sketchSizeHeight(55))
        }

        // Set constraints for the get started button.
        getStartedButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(sketchSizeHeight(-25))
            $0.centerX.equalToSuperview()
            $0.leading.equalToSuperview().offset(sketchSizeWidth(Constants.defaultLeading))
            $0.trailing.equalToSuperview().offset(-sketchSizeWidth(Constants.defaultLeading))
            $0.height.equalTo(sketchSizeHeight(55))
        }
    }
}

// MARK: - EXTENSION DEFINITION

extension RootOnboardingViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        // Get current index.
        guard let currentIndex = onboardingViewControllers.firstIndex(of: viewController) else { return nil }

        // Get previous index.
        let previousIndex = onboardingViewControllers.index(before: currentIndex)

        // Return proper index.
        return (previousIndex >= 0) ? onboardingViewControllers[previousIndex] : nil
    }

    func pageViewController(_: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        // Get current index.
        guard let currentIndex = onboardingViewControllers.firstIndex(of: viewController) else { return nil }

        // Get next index.
        let nextIndex = onboardingViewControllers.index(after: currentIndex)

        // Return proper index.
        return (nextIndex < onboardingViewControllers.count) ? onboardingViewControllers[nextIndex] : nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating _: Bool, previousViewControllers _: [UIViewController], transitionCompleted _: Bool) {
        // Get page content view controller.
        guard let pageContentViewController = pageViewController.viewControllers?[0] else { return }

        // Get current index.
        guard let currentIndex = onboardingViewControllers.firstIndex(of: pageContentViewController) else { return }

        // Set current page.
        pageControl.currentPage = currentIndex

        // Handle button states.
        handleButtonsState(index: currentIndex)
    }

    private func handleButtonsState(index: Int) {
        // Set skip button state.
        setSkipButtonState(forIndex: index)

        // Set next button state.
        setNextButtonState(forIndex: index)
    }

    private func setSkipButtonState(forIndex index: Int) {
        if index == onboardingViewControllers.last() {
            skipButton.isHidden = true
        } else {
            skipButton.isHidden = false
        }
    }

    private func setNextButtonState(forIndex index: Int) {
        if index == onboardingViewControllers.last() {
            nextButton.isHidden = true
            getStartedButton.isHidden = false
        } else {
            nextButton.isHidden = false
            getStartedButton.isHidden = true
        }
    }
}
