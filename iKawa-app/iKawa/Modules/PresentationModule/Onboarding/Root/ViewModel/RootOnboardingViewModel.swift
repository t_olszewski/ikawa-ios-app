import RxCocoa
import RxSwift

// MARK: PROTOCOL DEFINITION

protocol RootOnboardingViewModelProtocol {
    var onSkipTap: (() -> Void)? { get set }
    var onGetStartedTap: (() -> Void)? { get set }
    func skipOnBoarding()
}

// MARK: CLASS DEFINITION

final class RootOnboardingViewModel: Onboarding.ViewModel {
    // MARK: NAVIGATION CALLBACKS

    var onSkipTap: (() -> Void)?
    var onGetStartedTap: (() -> Void)?

    // MARK: - PUBLIC PROPERTIES

    // MARK: - PRIVATE PROPERTIES

    private let authDomainManager: Domain.Authentication

    // MARK: - INITIALIZER

    init(authDomainManager: Domain.Authentication) {
        self.authDomainManager = authDomainManager
    }

    // MARK: PUBLIC METHODS

    func skipOnBoarding() {
        // Set value for first app lunch.
        authDomainManager.setFirstAppLunch()

        // Call naviation.
        onSkipTap?()
    }
}
