import RxCocoa
import RxSwift
import SnapKit
import UIKit

// MARK: - CLASS DEFINITION

final class DetailOnboardingView: UIView {
    // MARK: - PRIVATE PROPERTIES

    private let viewModel: Onboarding.ViewModel

    // MARK: - UI ELEMENTS

    private lazy var headerLabel = createHeaderLabel()
    private lazy var imageView = createImageView()
    private lazy var subtitleLabel = createSubtitleLabel()

    // MARK: INTIALIZERS

    init(
        viewModel: Onboarding.ViewModel,
        configuration: (title: String, icon: UIImage, description: String)
    ) {
        // Assign properties.
        self.viewModel = viewModel

        // Super initializer.
        super.init(frame: .zero)

        // Setup view.
        setupView(configuration: configuration)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - SETUP UI

    private func setupView(configuration: (title: String, icon: UIImage, description: String)) {
        // Add subiews.
        addSubviews()

        // Add constraints.
        addConstraints()

        // Setup initial state.
        setupInitialState(configuration: configuration)
    }

    // MARK: - INTERACTIONS

    @objc private func getStartedTapped() {
        viewModel.onGetStartedTap?()
    }

    @objc private func skipTapped() {
        viewModel.onSkipTap?()
    }
}

// MARK: - UI CREATION

extension DetailOnboardingView {
    private func setupInitialState(configuration: (title: String, icon: UIImage, description: String)) {
        // Setup initial state based on configuration.
        headerLabel.text = configuration.title
        imageView.image = configuration.icon
        subtitleLabel.text = configuration.description
        backgroundColor = .cobaltBlue
    }

    private func createImageView() -> UIImageView {
        // Setup view properties.
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }

    private func createHeaderLabel() -> UILabel {
        // Setup view properties.
        let view = UILabel()
        view.adjustsFontSizeToFitWidth = true
        view.minimumScaleFactor = 0.5
        view.textColor = .white
        view.font = .montBlack(ofSize: 32)
        view.textAlignment = .center

        return view
    }

    private func createSubtitleLabel() -> UILabel {
        // Setup view properties.
        let view = UILabel()
        view.adjustsFontSizeToFitWidth = true
        view.minimumScaleFactor = 0.5
        view.textColor = .pastelBlue
        view.font = .montRegular(ofSize: 18)
        view.textAlignment = .center
        view.numberOfLines = 3

        return view
    }
}

// MARK: - UI LAYOUT

extension DetailOnboardingView {
    private func addSubviews() {
        // Create subviews collection.
        let subViews = [
            headerLabel, subtitleLabel, imageView,
        ]

        // Add all subviews.
        addSubviews(subViews)
    }

    private func addConstraints() {
        // Set constraints for the image view.
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(sketchSizeHeight(70))
            $0.height.equalTo(sketchSizeHeight(248))
            $0.width.equalTo(sketchSizeWidth(210))
            $0.centerX.equalToSuperview()
        }

        // Set constraints for the header label.
        headerLabel.snp.makeConstraints { make in
            make.top.equalTo(imageView.snp.bottom).offset(sketchSizeHeight(40))
            make.centerX.equalToSuperview()
            make.leading.equalToSuperview().offset(sketchSizeWidth(Constants.defaultLeading))
            make.trailing.equalToSuperview().offset(-sketchSizeWidth(Constants.defaultLeading))
        }

        // Set constraints for the subtitle label.
        subtitleLabel.snp.makeConstraints { make in
            make.top.equalTo(headerLabel.snp.bottom).offset(sketchSizeHeight(30))
            make.centerX.equalToSuperview()
            make.width.equalTo(265.5).priority(750)
            make.leading.equalToSuperview().offset(sketchSizeWidth(30))
            make.trailing.equalToSuperview().offset(sketchSizeWidth(-30))
        }
    }
}
