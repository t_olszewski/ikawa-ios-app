import UIKit

// MARK: - CLASS DEFINITION

final class DetailOnboardingViewController: UIViewController {
    // MARK: - PRIVATE PROPERTIES

    private let viewModel: Onboarding.ViewModel
    private let configuration: (String, UIImage, String)

    // MARK: - INITIALIZERS

    init(viewModel: Onboarding.ViewModel, configuration: (title: String, icon: UIImage, description: String)) {
        self.viewModel = viewModel
        self.configuration = configuration
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - VIEW LIFECYCLE

    override func loadView() {
        super.loadView()

        // Crate detail view.
        let mainView = DetailOnboardingView(
            viewModel: viewModel,
            configuration: configuration
        )

        // Add detail view to main view.
        view.addSubview(mainView)

        // Set constraints on detail view.
        mainView.fillParent()
    }
}
