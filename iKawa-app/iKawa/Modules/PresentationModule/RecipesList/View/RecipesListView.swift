import NVActivityIndicatorView
import RxCocoa
import RxSwift
import SnapKit
import SwiftUI
import UIKit

// MARK: PROTOCOL DEFINITION

protocol RecipesListViewProtocol: UIView {}

// MARK: - CLASS DEFINITION

final class RecipesListView: UIView, RecipesList.View {
    // MARK: - PRIVATE PROPERTIES

    private var viewModel: RecipesList.ViewModel

    // MARK: - UI ELEMENTS

    private let coffeeLabel: UILabel = .regularLeft(text: "Coffee", fontSize: 16, textColor: .tomato)
    private let coffeeNameLabel: UILabel = .boldLeft(text: "Las Mercedes", fontSize: 30)
    private lazy var coffeeOriginLabel: UILabel = .regularLeft(text: "El Salvador", fontSize: 26, textColor: .corduroy)
    private lazy var tableView: UITableView = createTableView()

    // MARK: - INTIALIZERS

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(viewModel: RecipesList.ViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)

        setupView()
    }

    // MARK: - SETUP UI

    private func setupView() {
        addSubviews()
        addConstraints()
        bindUI()
    }
}

// MARK: - UI CREATION

extension RecipesListView {
    private func createTableView() -> UITableView {
        let view: UITableView = .createTableView(cellClasses: [RecipeTableViewCell.self])
        view.delegate = self
        return view
    }
}

// MARK: - UI LAYOUT

extension RecipesListView {
    private func addSubviews() {
        backgroundColor = .white

        addSubviews([coffeeLabel, coffeeNameLabel, coffeeOriginLabel, tableView])
    }

    private func addConstraints() {
        coffeeLabel.snp.makeConstraints {
            $0.top.equalTo(safeAreaLayoutGuide.snp.top).offset(30)
            $0.leading.equalToSuperview().offset(50)
            $0.trailing.equalToSuperview().offset(-50)
        }

        coffeeNameLabel.snp.makeConstraints {
            $0.top.equalTo(coffeeLabel.snp.bottom).offset(20)
            $0.leading.trailing.equalTo(coffeeLabel)
        }

        coffeeOriginLabel.snp.makeConstraints {
            $0.top.equalTo(coffeeNameLabel.snp.bottom).offset(16)
            $0.leading.trailing.equalTo(coffeeLabel)
        }

        tableView.snp.makeConstraints {
            $0.top.equalTo(coffeeOriginLabel.snp.bottom).offset(28).priority(.high)
            $0.leading.trailing.equalToSuperview().priority(.high)
            $0.bottom.equalToSuperview()
        }
    }
}

// MARK: - UI BINDING

extension RecipesListView {
    private func bindUI() {
        bindRecipes()
        bindItemSelection()
    }

    private func bindRecipes() {
        viewModel.recipes.bind(to: tableView.rx.items(cellIdentifier: RecipeTableViewCell.toString(), cellType: RecipeTableViewCell.self)) { _, model, cell in
            cell.configure(model)
        }.disposed(by: viewModel.disposeBag)
    }

    private func bindItemSelection() {
        tableView.rx.modelSelected(Recipe.self).subscribe(onNext: { _ in
            // self?.viewModel.appCoordinator?.mainFlowCoordicator?.showCoffeePreviewViewController(model)
        }).disposed(by: viewModel.disposeBag)
    }
}

// MARK: - DELEGATES

extension RecipesListView: UITableViewDelegate {
    func tableView(_: UITableView, heightForHeaderInSection _: Int) -> CGFloat {
        return 90
    }

    func tableView(_: UITableView, viewForHeaderInSection _: Int) -> UIView? {
        let headerView = RecipesTableHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 90))
        return headerView
    }
}

// MARK: SwiftUI Preview

#if DEBUG
    struct RecipesListViewContainer: UIViewRepresentable {
        typealias UIViewType = RecipesListView

        func makeUIView(context _: Context) -> UIViewType {
            return RecipesListView(viewModel: RecipesListViewModel())
        }

        func updateUIView(_: RecipesListView, context _: Context) {}
    }

    struct RecipesListViewContainer_Previews: PreviewProvider {
        static var previews: some View {
            Group {
                RecipesListViewContainer()
                    .previewDevice("iPhone 8")
                RecipesListViewContainer()
                    .previewDevice("iPhone 8 Plus")
                RecipesListViewContainer()
                    .previewDevice("iPhone 12")
                RecipesListViewContainer()
                    .previewDevice("iPhone 12 Pro")
                RecipesListViewContainer()
                    .previewDevice("iPhone 12 Pro Max")
                RecipesListViewContainer()
                    .previewDevice("iPhone 12 Mini")
                RecipesListViewContainer()
                    .previewDevice("iPhone SE (2nd generation)")
            }
        }
    }
#endif
