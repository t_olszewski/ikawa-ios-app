import SwiftUI
import UIKit

class RecipeTableViewCell: BaseTableViewCell {
    // MARK: - PRIVATE PROPERTIES

    private let titleLabel: UILabel = .boldLeft(fontSize: 21)
    private lazy var starReview: StarReview = createStarReview()
    private let rateLabel: UILabel = .boldLeft(fontSize: 14)
    private let reviewsCounterLabel: UILabel = .regularLeft(fontSize: 14)
    private let roastTimeLabel: UILabel = .regularLeft(fontSize: 16)
    private let developmentTimeLenghtLabel: UILabel = .regularLeft(fontSize: 16, numberOfLines: 0)
    private let developmentTimeRatioLabel: UILabel = .regularLeft(fontSize: 16, numberOfLines: 0)
    private let flavourLabel: UILabel = .regularLeft(fontSize: 16, numberOfLines: 0)
    private let recommendedBrewRecipeLabel: UILabel = .regularLeft(fontSize: 16, numberOfLines: 0)
    private let nextImageView: UIImageView = .init(image: UIImage(named: "next"))
    private let separatorView: UIView = .init(backgroundColor: .silverTwo)

    private var didSetupConstraints: Bool = false

    // MARK: - INITIALIZERS

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - PUBLIC METHODS

    func configure(_ model: Recipe) {
        titleLabel.text = model.name
        starReview.value = model.rate
        rateLabel.text = "\(model.rate)"
        reviewsCounterLabel.text = "(\(model.reviewsCounter) reviews)"
        roastTimeLabel.attributedText = NSAttributedString.attributedText(bold: "Roast time: ", normal: model.roastTime, fontSize: 16)
        developmentTimeLenghtLabel.attributedText = NSAttributedString.attributedText(bold: "Development Time Lenght: ", normal: model.developmentTimeLenght, fontSize: 16)
        developmentTimeRatioLabel.attributedText = NSAttributedString.attributedText(bold: "Development Time Ratio: ", normal: "\(model.developmentTimeRatio)%", fontSize: 16)
        flavourLabel.attributedText = NSAttributedString.attributedText(bold: "Flavour: ", normal: model.flavour, fontSize: 16)
        recommendedBrewRecipeLabel.attributedText = NSAttributedString.attributedText(bold: "Recommended Brew Recipe: ", normal: model.recommendedBrewRecipe, fontSize: 16)
    }
}

// MARK: - UI CREATION

extension RecipeTableViewCell {
    private func createStarReview() -> StarReview {
        let view = StarReview()
        view.starFillColor = (.tomato ?? .red)
        view.starBackgroundColor = .white
        view.starCount = 5
        view.isUserInteractionEnabled = false
        return view
    }
}

// MARK: - UI LAYOUT

extension RecipeTableViewCell {
    private func setupView() {
        selectionStyle = .none
        addSubviews()
        addConstraints()
    }

    private func addSubviews() {
        contentView.addSubviews([titleLabel, starReview, rateLabel, reviewsCounterLabel, roastTimeLabel, developmentTimeLenghtLabel, developmentTimeRatioLabel, flavourLabel, recommendedBrewRecipeLabel, nextImageView, separatorView])
    }

    private func addConstraints() {
        if !didSetupConstraints {
            titleLabel.snp.makeConstraints {
                $0.height.equalTo(20)
                $0.top.equalToSuperview().offset(24)
                $0.leading.equalToSuperview().offset(28)
                $0.trailing.equalTo(nextImageView.snp.leading).offset(-10)
            }
            starReview.snp.makeConstraints {
                $0.top.equalTo(titleLabel.snp.bottom).offset(8)
                $0.leading.equalTo(titleLabel.snp.leading)
                $0.size.equalTo(CGSize(width: 100, height: 25))
            }
            rateLabel.snp.makeConstraints {
                $0.leading.equalTo(starReview.snp.trailing).offset(8)
                $0.centerY.equalTo(starReview.snp.centerY).offset(2)
            }
            reviewsCounterLabel.snp.makeConstraints {
                $0.leading.equalTo(rateLabel.snp.trailing).offset(8)
                $0.centerY.equalTo(rateLabel.snp.centerY)
            }
            roastTimeLabel.snp.makeConstraints {
                $0.top.equalTo(starReview.snp.bottom).offset(12)
                $0.leading.equalTo(titleLabel.snp.leading).offset(18)
                $0.trailing.equalTo(titleLabel.snp.trailing)
            }
            developmentTimeLenghtLabel.snp.makeConstraints {
                $0.top.equalTo(roastTimeLabel.snp.bottom)
                $0.leading.trailing.equalTo(roastTimeLabel)
            }
            developmentTimeRatioLabel.snp.makeConstraints {
                $0.top.equalTo(developmentTimeLenghtLabel.snp.bottom)
                $0.leading.trailing.equalTo(roastTimeLabel)
            }
            flavourLabel.snp.makeConstraints {
                $0.top.equalTo(developmentTimeRatioLabel.snp.bottom)
                $0.leading.trailing.equalTo(roastTimeLabel)
            }
            recommendedBrewRecipeLabel.snp.makeConstraints {
                $0.top.equalTo(flavourLabel.snp.bottom)
                $0.leading.trailing.equalTo(roastTimeLabel)
            }
            nextImageView.snp.makeConstraints {
                $0.trailing.equalToSuperview().offset(-28)
                $0.centerY.equalToSuperview()
                $0.size.equalTo(CGSize(width: 36, height: 37))
            }
            separatorView.snp.makeConstraints {
                $0.top.equalTo(recommendedBrewRecipeLabel.snp.bottom).offset(26)
                $0.bottom.equalToSuperview()
                $0.height.equalTo(Constants.defaultSepartorHeight)
                $0.leading.equalTo(titleLabel.snp.leading)
                $0.trailing.equalTo(nextImageView.snp.trailing)
            }
            didSetupConstraints = true
        }
    }
}

// MARK: SwiftUI Preview

#if DEBUG
    struct RecipeTableViewCellContainer: UIViewRepresentable {
        typealias UIViewType = RecipeTableViewCell

        func makeUIView(context _: Context) -> UIViewType {
            let cell = RecipeTableViewCell()
            cell.configure(Recipe.testModel)
            return cell
        }

        func updateUIView(_: RecipeTableViewCell, context _: Context) {}
    }

    struct RecipeTableViewCellContainer_Previews: PreviewProvider {
        static var previews: some View {
            Group {
                RecipeTableViewCellContainer()
                    .previewDevice("iPhone 8")
                RecipeTableViewCellContainer()
                    .previewDevice("iPhone 8 Plus")
                RecipeTableViewCellContainer()
                    .previewDevice("iPhone 12")
                RecipeTableViewCellContainer()
                    .previewDevice("iPhone 12 Pro")
                CoffeeLibraryTableViewCellContainer()
                    .previewDevice("iPhone 12 Pro Max")
                RecipeTableViewCellContainer()
                    .previewDevice("iPhone 12 Mini")
                RecipeTableViewCellContainer()
                    .previewDevice("iPhone SE (2nd generation)")
            }.previewLayout(.fixed(width: UIScreen.main.bounds.width, height: 100))
        }
    }
#endif
