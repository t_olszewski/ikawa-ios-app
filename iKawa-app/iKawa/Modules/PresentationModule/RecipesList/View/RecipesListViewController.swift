import SwiftUI
import UIKit

// MARK: - CLASS DEFINITION

final class RecipesListViewController: BaseViewController {
    // MARK: - PRIVATE PROPERTIES

    private let rootView: RecipesList.View
    private let viewModel: RecipesList.ViewModel

    // MARK: - INITIALIZERS

    init(rootView: RecipesList.View, viewModel: RecipesList.ViewModel) {
        // Assign properties.
        self.rootView = rootView
        self.viewModel = viewModel

        // Call super.
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - VIEW LIFECYCLE

    override func loadView() {
        super.loadView()

        // Create main view.
        createView()
    }

    // MARK: - PRIVATE METHODS

    private func createView() {
        // Add main view to view controller.
        view.addSubview(rootView)

        // Fill view with contraints.
        rootView.fillParent()
    }
}

// swiftlint:disable force_cast
#if DEBUG
    struct RecipesListViewControllerContainerView: UIViewControllerRepresentable {
        typealias UIViewControllerType = RecipesListViewController

        func makeUIViewController(context _: Context) -> UIViewControllerType {
            return (UIApplication.shared.delegate as! AppDelegate).dependenciesFactory.makeRecipesListViewController()
        }

        func updateUIViewController(_: UIViewControllerType, context _: Context) {}
    }

    struct RecipesListViewControllerContainerView_Previews: PreviewProvider {
        static var previews: some View {
            RecipesListViewControllerContainerView().colorScheme(.light) // or .dark
        }
    }
#endif
// swiftlint:enable force_cast
