import SwiftUI
import UIKit

class RecipesTableHeaderView: UIView {
    // MARK: - PRIVATE PROPERTIES

    private let chooseRecipeLabel: UILabel = .regularLeft(text: "Choose recipe", fontSize: 17)
    private let ikawaRecipiesLabel: UILabel = .boldLeft(text: "IKAWA recipes", fontSize: 24, textColor: .tomato)

    private var didSetupConstraints: Bool = false

    // MARK: - INITIALIZERS

    override init(frame _: CGRect) {
        super.init(frame: CGRect.zero)

        setupView()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - UI LAYOUT

extension RecipesTableHeaderView {
    private func setupView() {
        backgroundColor = .white
        addSubviews()
        addConstraints()
    }

    private func addSubviews() {
        addSubviews([chooseRecipeLabel, ikawaRecipiesLabel])
        setNeedsUpdateConstraints()
    }

    private func addConstraints() {
        chooseRecipeLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(12)
            $0.leading.equalToSuperview().offset(50)
            $0.trailing.equalToSuperview().offset(-50)
        }

        ikawaRecipiesLabel.snp.makeConstraints {
            $0.top.equalTo(chooseRecipeLabel.snp.bottom).offset(16)
            $0.leading.trailing.equalTo(chooseRecipeLabel)
        }

        didSetupConstraints = true
        super.updateConstraints()
    }
}

// MARK: SwiftUI Preview

#if DEBUG
    struct RecipesTableHeaderViewContainer: UIViewRepresentable {
        typealias UIViewType = RecipesTableHeaderView

        func makeUIView(context _: Context) -> UIViewType {
            return RecipesTableHeaderView()
        }

        func updateUIView(_: RecipesTableHeaderView, context _: Context) {}
    }

    struct RecipesTableHeaderView_Previews: PreviewProvider {
        static var previews: some View {
            Group {
                RecipesTableHeaderViewContainer()
                    .previewDevice("iPhone 8")
                RecipesTableHeaderViewContainer()
                    .previewDevice("iPhone 8 Plus")
                RecipesTableHeaderViewContainer()
                    .previewDevice("iPhone 12")
                RecipesTableHeaderViewContainer()
                    .previewDevice("iPhone 12 Pro")
                RecipesTableHeaderViewContainer()
                    .previewDevice("iPhone 12 Pro Max")
                RecipesTableHeaderViewContainer()
                    .previewDevice("iPhone 12 Mini")
                RecipesTableHeaderViewContainer()
                    .previewDevice("iPhone SE (2nd generation)")
            }.previewLayout(.fixed(width: UIScreen.main.bounds.width, height: 90))
        }
    }
#endif
