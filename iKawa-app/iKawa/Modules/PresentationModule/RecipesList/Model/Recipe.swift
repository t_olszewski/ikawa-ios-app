struct Recipe {
    var name: String
    var rate: Float
    var reviewsCounter: Int
    var roastTime: String
    var developmentTimeLenght: String
    var developmentTimeRatio: Int
    var flavour: String
    var recommendedBrewRecipe: String

    static var testModel: Recipe {
        return Recipe(name: "Escpresso", rate: 4.75, reviewsCounter: 322, roastTime: "07:00 min", developmentTimeLenght: "02:15 min", developmentTimeRatio: 30, flavour: "Orange zest, red fruits, black tea, baker's chocolate", recommendedBrewRecipe: "19g of coffee, 35mL, 25 seconds")
    }
}
