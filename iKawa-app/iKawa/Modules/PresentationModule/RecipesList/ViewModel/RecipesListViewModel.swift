import RxCocoa
import RxSwift

// MARK: - PROTOCOL DEFINITION

protocol RecipesListViewModelProtocol {
    var recipes: BehaviorRelay<[Recipe]> { get }
}

// MARK: - CLASS DEFINITION

final class RecipesListViewModel: RecipesList.ViewModel {

    var recipes = BehaviorRelay<[Recipe]>(value: [])

    // MARK: - PRIVATE PROPERTIES

    private let networkObserver: Network.Observer?

    // MARK: - INITIALIZER

    init(networkObserver: Network.Observer? = nil) {
        self.networkObserver = networkObserver

        recipes.accept([Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel,
                        Recipe.testModel])
    }
}
