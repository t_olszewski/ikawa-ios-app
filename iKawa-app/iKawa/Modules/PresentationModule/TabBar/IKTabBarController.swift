import Foundation
import UIKit

class IKTabBarController: UITabBarController {

    init() {
        super.init(nibName: nil, bundle: nil)

        tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabBar.layer.shadowRadius = 2
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOpacity = 0.3
        tabBar.barTintColor = .white
        tabBar.tintColor = .red

        object_setClass(tabBar, IKTabBar.self)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    class IKTabBar: UITabBar {

        override func sizeThatFits(_ size: CGSize) -> CGSize {
            var sizeThatFits = super.sizeThatFits(size)
            sizeThatFits.height = 120
            return sizeThatFits
        }
    }
}
