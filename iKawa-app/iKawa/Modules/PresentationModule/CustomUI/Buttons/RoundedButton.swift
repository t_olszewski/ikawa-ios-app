import SnapKit
import UIKit

// MARK: - CLASS DEFINITION

final class RoundedButton: UIButton {
    // MARK: - PRIVATE PROPERTIES

    private let textLabel = UILabel()

    // MARK: - INITIALISER

    init(
        text: String,
        backgroundColor: UIColor? = .white,
        textColor: UIColor? = .black,
        fontSize: CGFloat = 19
    ) {
        // Call super.
        super.init(frame: .zero)

        // Set title.
        setTitle(text: text, fontSize: fontSize)

        // Add subviews.
        addSubviews()

        // Apply constraints.
        applyConstraints()

        // Setup design.
        setUpAppearance(
            background: backgroundColor,
            textColor: textColor
        )
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - PUBLIC METHODS

    func setActive(active: Bool) {
        _ = active ? activeState() : inactiveState()
    }

    func setTitle(text: String, fontSize: CGFloat = 19) {
        // Set attributed text.
        let attributedText = NSAttributedString(
            string: text,
            attributes: [NSAttributedString.Key.font: UIFont.montBlack(ofSize: fontSize)]
        )

        // Assign attributed text.
        textLabel.attributedText = attributedText
    }

    // MARK: - PRIVATE METHODS

    private func activeState() {
        textLabel.textColor = .white
        backgroundColor = .cobaltBlue
        isUserInteractionEnabled = true
    }

    private func inactiveState() {
        textLabel.textColor = .battleshipGrey
        backgroundColor = .silver
        isUserInteractionEnabled = false
    }

    private func addSubviews() {
        addSubview(textLabel)
    }

    private func applyConstraints() {
        textLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.trailing.equalToSuperview().offset(sketchSizeWidth(10))
            $0.leading.equalToSuperview().offset(-sketchSizeWidth(10))
        }
    }

    private func setUpAppearance(background: UIColor?, textColor: UIColor?) {
        textLabel.textAlignment = .center
        textLabel.numberOfLines = 1
        textLabel.adjustsFontSizeToFitWidth = true
        textLabel.minimumScaleFactor = 0.5
        textLabel.textColor = textColor

        backgroundColor = background
    }

    override func layoutSubviews() {
        layer.cornerRadius = 20
        clipsToBounds = true
    }
}
