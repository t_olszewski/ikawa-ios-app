import UIKit

class RoundedView: UIView {

    init(
        backgroundColor: UIColor = .white,
        cornerRadius: CGFloat = 0,
        borderWidth: CGFloat = 0,
        borderColor: UIColor = .white,
        shadowColor: UIColor = UIColor.black.withAlphaComponent(0.2),
        shadowOffset: CGSize = CGSize.zero,
        shadowRadius: CGFloat = 0,
        shadowOpacity: Float = 0
    ) {
        super.init(frame: .zero)

        self.backgroundColor = backgroundColor

        addRoundedCorners(cornerRadius: cornerRadius, borderWidth: borderWidth, borderColor: borderColor)
        addShadow(offset: shadowOffset, color: shadowColor, radius: shadowRadius, opacity: shadowOpacity)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
