import UIKit

// MARK: - CLASS DEFINITION

final class BottomPopup: UIView {
    // MARK: - PRIVATE PROPERTIES

    private lazy var content = createContentView()
    private lazy var icon = createIcon()
    private lazy var title = createTitle()

    // MARK: - INITIALISER

    init(text: String) {
        // Call super.
        super.init(frame: .zero)

        // Assign text value.
        title.text = text

        // Add subviews.
        addSubviews()

        // Add constraints.
        addConstraints()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - PUBLIC METHODS

    func setError() {
        content.backgroundColor = .tomato
        icon.image = #imageLiteral(resourceName: "failMark")
    }

    // MARK: - PRIVATE METHODS

    private func addSubviews() {
        // Create subviews collection.
        let subViews = [
            icon, title,
        ]

        // Add subviews to content view.
        content.addSubviews(subViews)

        // Add content view.
        addSubview(content)
    }

    private func createContentView() -> UIView {
        // Set properties.
        let view = UIView()
        view.backgroundColor = .blueOcean

        // Return view.
        return view
    }

    private func createIcon() -> UIImageView {
        // Set properties.
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.image = #imageLiteral(resourceName: "checkMark")

        // Return view.
        return view
    }

    private func createTitle() -> UILabel {
        // Set properties.
        let view = UILabel()
        view.font = .montRegular(ofSize: 18)
        view.textColor = .white
        view.textAlignment = .left
        view.numberOfLines = 2

        // Return view.
        return view
    }

    private func addConstraints() {
        // Set constraints for the content view.
        content.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        // Set constraints for the icon.
        icon.snp.makeConstraints {
            $0.width.height.equalTo(sketchSizeWidth(50))
            $0.leading.top.equalToSuperview().offset(sketchSizeHeight(24))
        }

        // Set constraints for the title.
        title.snp.makeConstraints {
            $0.leading.equalTo(icon.snp.trailing).offset(sketchSizeWidth(16))
            $0.trailing.equalToSuperview().offset(sketchSizeWidth(-16))
            $0.bottom.equalTo(icon.snp.bottom)
            $0.top.equalTo(icon.snp.top)
        }
    }
}
