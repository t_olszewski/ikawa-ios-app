import UIKit

// MARK: - CLASS DEFINITION

final class BottomPopupItem: UIView {
    // MARK: - INTERNAL PROPERTIES

    enum ItemType {
        case base
    }

    // MARK: - PRIVATE PROPERTIES

    private(set) lazy var title = createTitle()
    private(set) lazy var valueField = createValueField()

    // MARK: - INITIALIZERS

    init(title _: String, value _: String) {
        // Call super.
        super.init(frame: .zero)

        // Add subviews.
        addSubviews()

        // Add constraints.
        addConstraints()

        // Set initial state.
        setInitialState()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        // Round view corners.
        layer.cornerRadius = 5
    }

    // MARK: - PRIVATE METHODS

    private func addSubviews() {
        // Create subviews collection.
        let subViews = [
            title, valueField,
        ]

        // Add content view.
        addSubviews(subViews)
    }

    private func setInitialState() {
        backgroundColor = .paleGrey
    }

    private func createTitle() -> UILabel {
        // Set properties.
        let view = UILabel()
        view.font = .montBlack(ofSize: 10)
        view.textColor = .slateGrey
        view.textAlignment = .center
        view.numberOfLines = 1
        view.text = "phase out".uppercased()

        // Return view.
        return view
    }

    private func createValueField() -> UILabel {
        // Set properties.
        let view = UILabel()
        view.font = .montBlack(ofSize: 17)
        view.textColor = .black
        view.textAlignment = .center
        view.numberOfLines = 1
        view.text = "01.01.2020"

        // Return view.
        return view
    }

    private func addConstraints() {
        // Set constraints for the title.
        title.snp.makeConstraints {
            $0.bottom.equalTo(self.snp.centerY)
            $0.trailing.equalToSuperview().offset(24)
            $0.leading.equalToSuperview().offset(-24)
        }

        // Set constraints for the text field.
        valueField.snp.makeConstraints {
            $0.top.equalTo(self.snp.centerY)
            $0.trailing.equalToSuperview().offset(24)
            $0.leading.equalToSuperview().offset(-24)
        }
    }
}
