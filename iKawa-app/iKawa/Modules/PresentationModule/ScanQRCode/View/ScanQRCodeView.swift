import NVActivityIndicatorView
import RxCocoa
import RxSwift
import SnapKit
import SwiftUI
import UIKit

// MARK: PROTOCOL DEFINITION

protocol ScanQRCodeViewProtocol: UIView {}

// MARK: - CLASS DEFINITION

final class ScanQRCodeView: UIView, ScanQRCode.View {
    // MARK: - PRIVATE PROPERTIES

    private var viewModel: ScanQRCode.ViewModel

    // MARK: - UI ELEMENTS

    private let topLabel: UILabel = .boldLeft(text: "Scan the\nQR Code", fontSize: 30, numberOfLines: 0)
    private let scanQRCodeImageView: UIImageView = .aspectFit(image: UIImage(named: "imagePreview"))
    private let indicatorView: NVActivityIndicatorView = .init(frame: CGRect.zero, type: .ballSpinFadeLoader, color: .tomato)
    private let scanningLabel: UILabel = .regularCenter(text: "Scanning...", fontSize: 18)

    // MARK: - INTIALIZERS

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(viewModel: ScanQRCode.ViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)

        setupView()
        indicatorView.startAnimating()
    }

    // MARK: - SETUP UI

    private func setupView() {
        addSubviews()
        addConstraints()
        bindUI()
    }
}

// MARK: - UI CREATION

extension ScanQRCodeView {}

// MARK: - UI LAYOUT

extension ScanQRCodeView {
    private func addSubviews() {
        backgroundColor = .pampas

        addSubviews([topLabel, scanQRCodeImageView, indicatorView, scanningLabel])
    }

    private func addConstraints() {
        topLabel.snp.makeConstraints {
            $0.top.equalTo(safeAreaLayoutGuide.snp.top).offset(26)
            $0.leading.trailing.equalTo(scanQRCodeImageView)
        }

        scanQRCodeImageView.snp.makeConstraints {
            $0.top.equalTo(topLabel.snp.bottom).offset(20)
            $0.centerX.equalToSuperview()
        }

        indicatorView.snp.makeConstraints {
            $0.size.equalTo(CGSize(width: 56, height: 56))
            $0.top.equalTo(scanQRCodeImageView.snp.bottom).offset(36)
            $0.centerX.equalToSuperview()
        }

        scanningLabel.snp.makeConstraints {
            $0.top.equalTo(indicatorView.snp.bottom).offset(36)
            $0.leading.trailing.equalTo(topLabel)
        }
    }
}

// MARK: - UI BINDING

extension ScanQRCodeView {
    private func bindUI() {}
}

// MARK: SwiftUI Preview

#if DEBUG
    struct ScanQRCodeViewContainer: UIViewRepresentable {
        typealias UIViewType = ScanQRCodeView

        func makeUIView(context _: Context) -> UIViewType {
            return ScanQRCodeView(viewModel: ScanQRCodeViewModel())
        }

        func updateUIView(_: ScanQRCodeView, context _: Context) {}
    }

    struct ScanQRCodeViewContainer_Previews: PreviewProvider {
        static var previews: some View {
            Group {
                ScanQRCodeViewContainer()
                    .previewDevice("iPhone 8")
                ScanQRCodeViewContainer()
                    .previewDevice("iPhone 8 Plus")
                ScanQRCodeViewContainer()
                    .previewDevice("iPhone 12")
                ScanQRCodeViewContainer()
                    .previewDevice("iPhone 12 Pro")
                ScanQRCodeViewContainer()
                    .previewDevice("iPhone 12 Pro Max")
                ScanQRCodeViewContainer()
                    .previewDevice("iPhone 12 Mini")
                ScanQRCodeViewContainer()
                    .previewDevice("iPhone SE (2nd generation)")
            }
        }
    }
#endif
