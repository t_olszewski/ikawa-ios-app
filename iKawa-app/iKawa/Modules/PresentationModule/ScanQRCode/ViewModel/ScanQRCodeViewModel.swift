import RxCocoa
import RxSwift

// MARK: - PROTOCOL DEFINITION

protocol ScanQRCodeViewModelProtocol {}

// MARK: - CLASS DEFINITION

final class ScanQRCodeViewModel: ScanQRCode.ViewModel {

    // MARK: - PRIVATE PROPERTIES

    private let networkObserver: Network.Observer?

    // MARK: - INITIALIZER

    init(networkObserver: Network.Observer? = nil) {
        self.networkObserver = networkObserver
    }
}
