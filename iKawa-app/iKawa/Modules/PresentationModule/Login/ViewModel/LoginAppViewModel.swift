import RxCocoa
import RxSwift

// MARK: - PROTOCOL DEFINITION

protocol LoginViewModelProtocol {
    var onLoginButtonTap: (() -> Void)? { get set }
}

// MARK: - CLASS DEFINITION

final class LoginViewModel: Login.ViewModel {
    // MARK: - NAVIGATION CALLBACKS

    var onLoginButtonTap: (() -> Void)?
}
