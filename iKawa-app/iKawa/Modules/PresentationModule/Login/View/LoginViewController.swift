import UIKit

// MARK: - CLASS DEFINITION

final class LoginViewController: BaseViewController {
    // MARK: - PRIVATE PROPERTIES

    private let rootView: Login.View
    private let viewModel: Login.ViewModel

    // MARK: - INITIALIZERS

    init(rootView: Login.View, viewModel: Login.ViewModel) {
        self.rootView = rootView
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - VIEW LIFECYCLE

    override func loadView() {
        super.loadView()

        // Create main view.
        createView()
    }

    // MARK: - PRIVATE METHODS

    private func createView() {
        // Add main view to view controller.
        view.addSubview(rootView)

        // Fill view with contraints.
        rootView.fillParent()
    }
}
