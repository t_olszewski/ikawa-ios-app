import RxCocoa
import RxSwift
import SnapKit
import UIKit

// MARK: PROTOCOL DEFINITION

protocol LoginViewProtocol: UIView {}

// MARK: - CLASS DEFINITION

final class LoginView: UIView, Login.View {
    // MARK: - PRIVATE PROPERTIES

    private var viewModel: Login.ViewModel

    // MARK: - UI ELEMENTS

    private var logo: UIImageView = .aspectFit(image: #imageLiteral(resourceName: "logo"))
    private var loginButton: UIButton = .highlighted(text: "Login")

    // MARK: - INTIALIZERS

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(viewModel: Login.ViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)

        setupView()
    }

    // MARK: - SETUP UI

    private func setupView() {
        addSubviews()
        addConstraints()
        setupTargets()
    }
}

// MARK: - UI SETUP

extension LoginView {
    private func setupTargets() {
        // Add tap button target.
        loginButton.rx.tap.throttle(.seconds(1), scheduler: MainScheduler.instance).bind { [weak self] in
            self?.viewModel.onLoginButtonTap?()
        }.disposed(by: viewModel.disposeBag)
    }
}

// MARK: - UI LAYOUT

extension LoginView {
    private func addSubviews() {
        // Set background.
        backgroundColor = .white

        // Create subviews collection.
        let subViews = [
            logo, loginButton,
        ]

        // Add all subviews.
        addSubviews(subViews)
    }

    private func addConstraints() {
        // Add constraints for the logo image.
        logo.snp.makeConstraints {
            $0.top.equalToSuperview().offset(sketchSizeHeight(90))
            $0.centerX.equalToSuperview()
            $0.height.equalTo(sketchSizeHeight(58))
            $0.width.equalTo(sketchSizeHeight(150))
        }

        // Add constraints for the login button.
        loginButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(sketchSizeHeight(-25))
            $0.centerX.equalToSuperview()
            $0.leading.equalToSuperview().offset(sketchSizeWidth(Constants.defaultLeading))
            $0.trailing.equalToSuperview().offset(sketchSizeWidth(Constants.defaultTrailing))
            $0.height.equalTo(sketchSizeHeight(50))
        }
    }
}
