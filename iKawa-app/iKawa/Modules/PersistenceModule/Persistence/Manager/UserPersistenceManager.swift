import Realm
import RealmSwift
import RxSwift

// MARK: - PROTOCOL DEFINITION

protocol UserPersistenceProtocol {
    func createUserModel(model: UserModelRealm) -> Completable
    func updateUserModel(model: UserModelRealm) -> Completable
    func deleteUserModel() -> Completable
    func readUserProfile() -> Maybe<UserModelRealm>
    func observeUserProfile() -> Observable<UserModelRealm>
}

// MARK: - CLASS DEFINITION

final class UserPersistenceManager: Persistence.User {
    // MARK: - PRIVATE PROPERTIES

    private let realmBase: Persistence.Base
    private var notificationToken: NotificationToken?

    // MARK: - PUBLIC INITIALIZER

    init(realmType: RealmType = .app) {
        realmBase = PersistenceStorage(realmType: realmType)
    }

    // MARK: - PUBLIC METHODS

    func createUserModel(model: UserModelRealm) -> Completable {
        return realmBase.deleteByType(UserModelRealm.self, predicate: nil)
            .andThen(realmBase.create(model, update: false))
    }

    func updateUserModel(model: UserModelRealm) -> Completable {
        return realmBase.create(model, update: true)
    }

    func deleteUserModel() -> Completable {
        return realmBase.deleteByType(UserModelRealm.self, predicate: nil)
    }

    func readUserProfile() -> Maybe<UserModelRealm> {
        return realmBase.read(UserModelRealm.self, predicate: nil).flatMap { [weak self] result in
            guard let self = self else { return Maybe.empty() }
            return self.realmBase.read(
                UserModelRealm.self,
                key: result.first?.id ?? 0
            )
        }
    }

    func observeUserProfile() -> Observable<UserModelRealm> {
        // Create custom stream.
        return Observable<UserModelRealm>.create { [weak self] observer in

            guard let self = self else { return Disposables.create {} }

            // Get realm instance.
            guard let realm = self.realmBase.realm else {
                observer.onError(
                    RealmError(message:
                        "data_base_not_initialized".localized
                    ))

                return Disposables.create {}
            }

            // Get result set.
            let result = realm.objects(UserModelRealm.self)

            // Get user object
            var userModel = realm.objects(UserModelRealm.self).first

            // Observe results notifications.
            self.notificationToken = result.observe { (changes: RealmCollectionChange) in

                // Act based on status change.
                switch changes {
                case let .initial(latestValue):

                    // Assign value.
                    userModel = latestValue.first

                case let .update(latestValue, _, _, _):

                    // Assign value.
                    userModel = latestValue.first

                case let .error(error):
                    observer.on(.error(error))
                }

                // Conditionaly send new value.
                if let value = userModel {
                    observer.onNext(value)
                }
            }

            // Create disposable.
            return Disposables.create { [weak self] in
                self?.notificationToken?.invalidate()
            }
        }
    }
}
