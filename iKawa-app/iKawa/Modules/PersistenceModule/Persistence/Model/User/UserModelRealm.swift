import RealmSwift

// MARK: - CLASS DEFINITION

final class UserModelRealm: Object {
    @objc dynamic var id: Int = zeroInt
    @objc dynamic var userName: String = emptyString
    @objc dynamic var email: String = emptyString
    @objc dynamic var role: String = emptyString

    convenience init(model: UserDataResponse) {
        self.init()
        id = model.user.id
        userName = model.user.username
        email = model.user.email
        role = model.user.role
    }

    convenience init(model: UserModel) {
        self.init()
        id = model.id
        userName = model.userName
        role = model.role
    }

    convenience init(id: Int) {
        self.init()
        self.id = id
    }

    // MARK: - PUBLIC METHODS

    override public class func primaryKey() -> String? {
        return "id"
    }
}
