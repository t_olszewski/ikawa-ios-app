import Realm
import RealmSwift
import RxRealm
import RxSwift

// MARK: - PROTOCOL DEFINITION

protocol BaseStorageProtocol: AnyObject {
    var realm: Realm? { get }

    func create<T: Object>(_ data: T, update: Bool) -> Completable
    func create<T: Object>(_ data: [T], update: Bool) -> Completable
    func read<T: Object>(_ type: T.Type, predicate: NSPredicate?) -> Maybe<Results<T>>
    func read<T: Object>(_ type: T.Type, key: Any) -> Maybe<T>
    func update(action: @escaping () -> Void) -> Completable
    func delete<T: Object>(_ data: Results<T>) -> Completable
    func delete<T: Object>(_ data: T) -> Completable
    func deleteAll() -> Completable
    func deleteByType<T: Object>(_ type: T.Type, predicate: NSPredicate?) -> Completable
}

// MARK: - EXTENSION DEFINITION

enum RealmType {
    case app
    case test
}

final class PersistenceStorage: Persistence.Base {
    let realm: Realm?

    init(realmType: RealmType) {
        switch realmType {
        case .app:
            var config = Realm.Configuration.defaultConfiguration
            config.deleteRealmIfMigrationNeeded = true
            let realm = try? Realm(configuration: config)
            self.realm = realm
        case .test:
            let configuration = Realm.Configuration(inMemoryIdentifier: "TestRealm")
            let realm = try? Realm(configuration: configuration)
            self.realm = realm
        }
    }
}

// MARK: - CREATE

extension PersistenceStorage {
    func create<T: Object>(_ data: T, update: Bool) -> Completable {
        return Completable.create { [weak self] completable in

            guard let realm = self?.realm else {
                completable(.error(RealmError(message: "data_base_not_initialized".localized
                )))

                return Disposables.create {}
            }

            do {
                try realm.write {
                    switch update {
                    case true:
                        realm.add(data, update: Realm.UpdatePolicy.modified)
                    case false:
                        realm.add(data)
                    }
                }
            } catch {
                completable(.error(error))
            }

            completable(.completed)

            return Disposables.create {}
        }
    }

    func create<T: Object>(_ data: [T], update: Bool) -> Completable {
        return Completable.create { [weak self] completable in

            guard let realm = self?.realm else {
                completable(.error(RealmError(message: "data_base_not_initialized".localized
                )))

                return Disposables.create {}
            }

            do {
                try realm.write {
                    switch update {
                    case true:
                        realm.add(data, update: Realm.UpdatePolicy.all)
                    case false:
                        realm.add(data)
                    }
                }
            } catch {
                completable(.error(error))
            }

            completable(.completed)

            return Disposables.create {}
        }
    }
}

// MARK: - READ

extension PersistenceStorage {
    func read<T: Object>(_ type: T.Type, predicate: NSPredicate?) -> Maybe<Results<T>> {
        return Maybe<Results<T>>.create { [weak self] maybe in

            guard let realm = self?.realm else {
                maybe(.error(RealmError(message: "data_base_not_initialized".localized
                )))

                return Disposables.create {}
            }

            switch predicate == nil {
            case true:
                maybe(.success(realm.objects(type)))
            case false:
                maybe(.success(realm.objects(type).filter(predicate!)))
            }

            return Disposables.create {}
        }
    }

    func read<T: Object>(_ type: T.Type, key: Any) -> Maybe<T> {
        return Maybe<T>.create { [weak self] maybe in

            guard let realm = self?.realm else {
                maybe(.error(RealmError(message: "data_base_not_initialized".localized
                )))

                return Disposables.create {}
            }

            let model = realm.object(ofType: type, forPrimaryKey: key)

            switch model == nil {
            case true:
                maybe(.error(RealmError(message: "model_not_found".localized)))
            case false:
                maybe(.success(model!))
            }

            return Disposables.create {}
        }
    }
}

// MARK: - UPDATE

extension PersistenceStorage {
    func update(action: @escaping () -> Void) -> Completable {
        return Completable.create { [weak self] completable in

            guard let realm = self?.realm else {
                completable(.error(RealmError(message: "data_base_not_initialized".localized
                )))

                return Disposables.create {}
            }

            do {
                try realm.write {
                    action()
                }
            } catch {
                completable(.error(error))
            }

            completable(.completed)

            return Disposables.create {}
        }
    }
}

// MARK: - DELETE

extension PersistenceStorage {
    func delete<T: Object>(_ data: Results<T>) -> Completable {
        return Completable.create { [weak self] completable in

            guard let realm = self?.realm else {
                completable(.error(RealmError(message: "data_base_not_initialized".localized
                )))

                return Disposables.create {}
            }

            do {
                try realm.write {
                    realm.delete(data)
                }
            } catch {
                completable(.error(error))
            }

            completable(.completed)

            return Disposables.create {}
        }
    }

    func delete<T: Object>(_ data: T) -> Completable {
        return Completable.create { [weak self] completable in

            guard let realm = self?.realm else {
                completable(.error(RealmError(message: "data_base_not_initialized".localized
                )))

                return Disposables.create {}
            }

            do {
                try realm.write {
                    realm.delete(data)
                }
            } catch {
                completable(.error(error))
            }

            completable(.completed)

            return Disposables.create {}
        }
    }

    func deleteByType<T: Object>(_ type: T.Type, predicate: NSPredicate?) -> Completable {
        return Completable.create { [weak self] completable in

            guard let realm = self?.realm else {
                completable(.error(RealmError(message: "data_base_not_initialized".localized
                )))

                return Disposables.create {}
            }

            let objects = predicate == nil ? realm.objects(type) : realm.objects(type).filter(predicate!)

            do {
                try realm.write {
                    realm.delete(objects)
                }
            } catch {
                completable(.error(error))
            }

            completable(.completed)

            return Disposables.create {}
        }
    }

    func deleteAll() -> Completable {
        return Completable.create { [weak self] completable in

            guard let realm = self?.realm else {
                completable(.error(RealmError(message: "data_base_not_initialized".localized
                )))

                return Disposables.create {}
            }

            do {
                try realm.write {
                    realm.deleteAll()
                }
            } catch {
                completable(.error(error))
            }

            completable(.completed)

            return Disposables.create {}
        }
    }
}

// MARK: - CUSTOM REALM ERROR

struct RealmError: Error {
    let message: String
}

extension RealmError: LocalizedError {
    var errorDescription: String? {
        return NSLocalizedString(message, comment: "")
    }
}
