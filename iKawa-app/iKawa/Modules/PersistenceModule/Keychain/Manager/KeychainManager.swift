import KeychainAccess
import RxSwift

// MARK: - PROTOCOL DEFINITION

protocol KeychainProtocol {
    func setFirstAppLunch()
    func firstAppLunch() -> Bool
    func storeSessionToken(token: String) -> Completable
    func getSessionToken() -> Single<String>
    func clearSessionToken() -> Completable
    func clearSeesionTokenSync()
    func clearAll() -> Completable
    func clearAllSync()
}

// MARK: - CLASS DEFINITION

final class KeychainManager: Persistence.Session {
    // MARK: - PRIVATE PROPERTIES

    private let keychain: Keychain
    private let accessTokenKey = "accessToken"
    private let firstLunch = "firstLunch"

    // MARK: - PUBLIC INITIALIZER

    init(keychian: String = Constants.keychainService) {
        keychain = Keychain(service: keychian)
    }

    // MARK: - PUBLIC METHODS

    func setFirstAppLunch() {
        keychain[firstLunch] = firstLunch
    }

    func firstAppLunch() -> Bool {
        return keychain[firstLunch] == nil
    }

    func storeSessionToken(token: String) -> Completable {
        // Create custom completable.
        return Completable.create { [unowned self] completable in

            // Clear seesion token.
            self.keychain[self.accessTokenKey] = nil

            // Assign new session token.
            self.keychain[self.accessTokenKey] = token

            // Check if new session token was assigned.
            guard self.keychain[self.accessTokenKey] != nil else {
                // Rise custom error.
                completable(.error(KeychainError(message: "token_not_saved")))

                // Create disposable.
                return Disposables.create {}
            }

            // Send .completed event.
            completable(.completed)

            // Create disposable.
            return Disposables.create {}
        }
    }

    func getSessionToken() -> Single<String> {
        // Create custom single stream.
        return Single<String>.create { [unowned self] single in

            // Check if new session token was assigned.
            guard let token = self.keychain[self.accessTokenKey] else {
                // Send value event.
                single(.success(emptyString))

                // Create disposable.
                return Disposables.create {}
            }

            // Send value event.
            single(.success(token))

            // Create disposable.
            return Disposables.create {}
        }
    }

    func clearSessionToken() -> Completable {
        // Create custom completable.
        return Completable.create { [unowned self] completable in

            // Clear seesion token.
            self.keychain[self.accessTokenKey] = nil

            // Check if new session token was assigned.
            guard self.keychain[self.accessTokenKey] == nil else {
                // Rise custom error.
                completable(.error(KeychainError(message: "token_not_cleared")))

                // Create disposable.
                return Disposables.create {}
            }

            // Send .completed event.
            completable(.completed)

            // Create disposable.
            return Disposables.create {}
        }
    }

    func clearSeesionTokenSync() {
        // Clear seesion token.
        keychain[accessTokenKey] = nil
    }

    func clearAll() -> Completable {
        // Create custom completable.
        return Completable.create { [unowned self] completable in

            // Clear seesion token.
            self.keychain[self.accessTokenKey] = nil

            // Check if new session token was assigned.
            guard self.keychain[self.accessTokenKey] == nil else {
                // Rise custom error.
                completable(.error(KeychainError(message: "token_not_cleared")))

                // Create disposable.
                return Disposables.create {}
            }

            // Send .completed event.
            completable(.completed)

            // Create disposable.
            return Disposables.create {}
        }
    }

    func clearAllSync() {
        try? keychain.removeAll()
    }
}

// MARK: - CUSTOM KEYCHAIN ERROR

struct KeychainError: Error {
    let message: String
}

extension KeychainError: LocalizedError {
    var errorDescription: String? {
        return NSLocalizedString(message, comment: "")
    }
}
