import Moya
import RxBus

// MARK: - MAIN DOMAIN PROTOCOL

protocol NetworkExceptionManagerProtocol {
    func parseNetworkError(error: Error) -> NetworkingError?
}

// MARK: - CLASS DEFINITION

final class NetworkExceptionManager: Network.Exception {
    // MARK: - PUBLIC INITIALIZER

    init() {}

    // MARK: - PUBLIC METHODS

    func parseNetworkError(error: Error) -> NetworkingError? {
        guard let error = error as? MoyaError, let moyaResponseCode = error.response?.statusCode else { return nil }
        guard let data = error.response?.data else { return nil }
        guard let statusCode = StatusCode(rawValue: moyaResponseCode) else { return nil }

        // For 401 response send logout event.
        if statusCode.rawValue == 401 {
            RxBus.shared.post(event: Events.LoggedOut())
            return nil
        }

        do {
            if let jsonDictionary = (try JSONSerialization.jsonObject(with: data, options: [])) as? NSDictionary, let fieldErrors = jsonDictionary["fieldErrors"] as? [NSDictionary] {
                for error in fieldErrors {
                    if let errorMessage = error["message"] as? String {
                        return NetworkingError(message: errorMessage, statusCode: statusCode)
                    }
                }
            }
        } catch {
            print(error.localizedDescription)
        }

        return NetworkingError(message: statusCode.description, statusCode: statusCode)
    }
}
