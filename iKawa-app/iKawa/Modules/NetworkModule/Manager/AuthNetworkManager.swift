import Moya
import RxSwift

// MARK: - PROTOCOL DEFINITION

protocol AuthenticateManagerProtocol {
    func loginUser(request: AuthRequest) -> Single<UserDataResponse>
    func userProfile(token: String) -> Single<UserResponse>
}

// MARK: - CLASS DEFINITION

final class AuthNetworkManager: Network.Authentication {
    // MARK: - PRIVATE PROPERTIES

    private let server: Server
    private let provider = MoyaProvider<AuthApi>(plugins: [NetworkLoggerPlugin()])

    // MARK: - PUBLIC INITIALIZER

    init(server: Server = .production) {
        self.server = server
    }

    // MARK: - PUBLIC METHODS

    func loginUser(request: AuthRequest) -> Single<UserDataResponse> {
        return provider.rx
            .request(.authenticate(request: request, server: server))
            .filterSuccessfulStatusAndRedirectCodes()
            .map(UserDataResponse.self)
    }

    func userProfile(token: String) -> Single<UserResponse> {
        return provider.rx
            .request(.user(token: token, server: server))
            .filterSuccessfulStatusAndRedirectCodes()
            .map(UserResponse.self)
    }
}
