struct UserDataResponse: Decodable, Encodable {
    var accessToken: String = emptyString
    var user = UserResponse()
}

// MARK: - STRUCT DEFINITION

struct UserResponse: Decodable, Encodable {
    var id: Int = zeroInt
    var username: String = emptyString
    var email: String = emptyString
    var role: String = emptyString
}
