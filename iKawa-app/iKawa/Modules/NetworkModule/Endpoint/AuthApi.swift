import Foundation
import KeychainAccess
import Moya

enum Server {
    case production
    case test

    var baseUrl: URL {
        switch self {
        case .production:
            return URL(string: BaseUrl.url)!
        case .test:
            return URL(string: BaseUrl.url)!
        }
    }
}

class BaseUrl {
    class var url: String {
        #if DEBUG
            return ""
        #elseif STAGE
            return ""
        #else
            return ""
        #endif
    }
}

enum AuthApi {
    case authenticate(request: AuthRequest, server: Server)
    case user(token: String, server: Server)
}

extension AuthApi: TargetType {
    var baseURL: URL {
        switch self {
        case let .authenticate(_, server):
            return server.baseUrl
        case let .user(_, server):
            return server.baseUrl
        }
    }

    var path: String {
        switch self {
        case .authenticate:
            return "/login"
        case .user:
            return "/user/profile"
        }
    }

    var method: Moya.Method {
        switch self {
        case .authenticate:
            return .post
        default:
            return .get
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case let .authenticate(request, _):
            return .requestJSONEncodable(request)
        default:
            return .requestPlain
        }
    }

    private func requestWithParameters(_ parameters: [String: Any]) -> Task {
        let encoding = URLEncoding(arrayEncoding: .noBrackets)
        return .requestParameters(parameters: parameters, encoding: encoding)
    }

    private func requestWithBodyParameters(_ parameters: [String: Any]) -> Task {
        return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
    }

    var keychain: Keychain {
        return Keychain(service: Constants.keychainService)
    }

    var accessTokenKey: String {
        return "accessToken"
    }

    var headers: [String: String]? {
        switch self {
        case let .user(token, _):
            return ["Content-type": "application/json", "Authorization": "\(token)"]
        default:
            return createHeaders()
        }
    }

    func createHeaders() -> [String: String]? {
        if let token = keychain[accessTokenKey] {
            return ["Content-type": "application/json", "Authorization": "\(token)"]
        } else {
            return ["Content-type": "application/json"]
        }
    }
}
