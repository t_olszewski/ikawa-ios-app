struct AuthRequest: Decodable, Encodable {
    var code: String = emptyString
    var jwtToken: String = emptyString

    init(model: AuthModel) {
        code = model.code
        jwtToken = model.jwtToken
    }
}
