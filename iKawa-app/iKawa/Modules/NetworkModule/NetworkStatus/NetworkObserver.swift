import Foundation
import Reachability

// MARK: - PROTOCOL DEFINITION

protocol NetworkObserverProtocol {
    var isReachable: Bool { get }
    func startObserving()
}

// MARK: - CLASS DEFINITION

final class NetworkObserver: Network.Observer {
    // MARK: - PUBLIC PROPERTIES

    var isReachable: Bool {
        return reachable
    }

    // MARK: - PRIVATE PROPERTIES

    private let reachability = try? Reachability()
    private var reachable: Bool = true

    // MARK: - PUBLIC METHOD

    func startObserving() {
        reachability?.whenReachable = { [weak self] reachability in
            self?.reachable = reachability.connection != .unavailable
        }

        reachability?.whenUnreachable = { [weak self] reachability in
            self?.reachable = reachability.connection != .unavailable
        }

        do {
            try reachability?.startNotifier()
        } catch {
            print(error.localizedDescription)
        }
    }
}
