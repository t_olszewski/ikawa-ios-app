Pod::Spec.new do |s|
  s.name             = 'IKRoasterLib'
  s.version          = '0.1.0'
  s.summary          = 'Library to connect to IKAWA Coffee roasters.'
  s.homepage         = 'http://www.ikawacoffee.com'
  s.license          = { :type => 'No License' }
  s.author           = { 'Tijn Kooijmans' => 'tijn@studiosophisti.nl' }
  s.source           = { :git => 'https://bitbucket.org/studiosophisti/ikawa-roasterlib-ios', :tag => s.version.to_s }
  s.platform         = :is
  s.ios.deployment_target         = '9.0'
  s.source_files     = 'IKRoasterLib/src/**/*.{c,cc,cpp,mm,m,h}'
  s.resource         = 'IKRoasterLib/src/**/*.{plist}'
  s.xcconfig         = { 'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/IKRoasterLib/src" ' + '"${PROJECT_DIR}/../IKRoasterLib/src"' }
end
