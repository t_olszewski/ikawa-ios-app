//
//  RoasterManagerDelegate.h
//  IkawaTool
//
//  Created by Tijn Kooijmans on 15/09/14.
//  Copyright (c) 2014 Studio Sophisti. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Roaster;

@protocol RoasterManagerDelegate <NSObject>

@optional
- (void)roasterDidConnect:(Roaster*)roaster;
- (void)roasterDidDisconnect:(Roaster*)roaster;
- (void)roasterDidUpdateInfo:(Roaster*)roaster;
- (void)roasterDidUpdateStatus:(Roaster*)roaster;
- (void)roaster:(Roaster*)roaster didUpdateProfile:(Profile*)profile;
- (void)roaster:(Roaster*)roaster didReceiveSetting:(int)setting withValue:(NSNumber*)value success:(BOOL)success;
- (void)roasterDidFinishTest:(Roaster*)roaster withStatus:(int)status andFailure:(int)failure;
- (void)roasterDidDidReceiveFirmwareVersion:(Roaster*)roaster;
- (void)roasterDidUpdateMachType:(Roaster*)roaster;

@end
