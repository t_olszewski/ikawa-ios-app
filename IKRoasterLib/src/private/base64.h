//
//  base64.h
//  Ikawa
//
//  Created by Ruben van Rooij on 01/04/14.
//
//

#ifndef __Ikawa__base64__
#define __Ikawa__base64__

#include <string>

std::string base64_encode(const std::string &s);
std::string base64_encode(unsigned char const* , unsigned int len);
std::string base64_decode(std::string const& s);

#endif /* defined(__Ikawa__base64__) */
