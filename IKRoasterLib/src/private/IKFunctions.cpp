//
//  IKFunctions.cpp
//  IkawaTool
//
//  Created by Tijn Kooijmans on 24/09/14.
//  Copyright (c) 2014 Studio Sophisti. All rights reserved.
//

#include "IKFunctions.h"

static const std::string base64_chars =
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";

uint16_t crc16(char* data, int length, uint16_t initialValue)
{
    uint16_t c = initialValue;
    
    for (int i = 0; i < length; i++) {
        uint8_t x = data[i];
        x ^= c & 0xff;
        x ^= (x << 4) & 0xff;
        
        c = ( (x << 8) & 0xffff ) | ( (c >> 8) & 0xff );
        c ^= x >> 4;
        c ^= (x << 3) & 0xffff;
    }
    return c;
}

std::string base64_encode(unsigned char const* bytes_to_encode, unsigned long in_len) {
    std::string ret;
    int i = 0;
    int j = 0;
    unsigned char char_array_3[3];
    unsigned char char_array_4[4];
    
    while (in_len--) {
        char_array_3[i++] = *(bytes_to_encode++);
        if (i == 3) {
            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;
            
            for(i = 0; (i <4) ; i++)
                ret += base64_chars[char_array_4[i]];
            i = 0;
        }
    }
    
    if (i)
    {
        for(j = i; j < 3; j++)
            char_array_3[j] = '\0';
        
        char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
        char_array_4[3] = char_array_3[2] & 0x3f;
        
        for (j = 0; (j < i + 1); j++)
            ret += base64_chars[char_array_4[j]];
        
        while((i++ < 3))
            ret += '=';
        
    }
    
    return ret;
    
}

std::string string_format(const std::string fmt, ...) {
    int size = 100;
    std::string str;
    va_list ap;
    while (1) {
        str.resize(size);
        va_start(ap, fmt);
        int n = vsnprintf((char *)str.c_str(), size, fmt.c_str(), ap);
        va_end(ap);
        if (n > -1 && n < size) {
            str.resize(n);
            return str;
        }
        if (n > -1)
            size = n + 1;
        else
            size *= 2;
    }
    return str;
}

std::string asciiToHex(std::string string) {
    
    std::string str;
    
    for(int i=0; i<string.size(); i++)
        str += string_format("%2.2X", (unsigned char)string[i]);
    
    return str;
}

void hexToAscii(const std::string &in, std::string &out) {
    
    std::string _in = in;
    _in.erase (std::remove(_in.begin(), _in.end(), ' '), _in.end());
    _in.erase (std::remove(_in.begin(), _in.end(), '-'), _in.end());
    
    out.clear();
    for(int i=0; i<_in.length(); i+=2)
    {
        out += std::stol(_in.substr(i,2).c_str(), NULL, 16);
    }
}
