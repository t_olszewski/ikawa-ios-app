//
//  Profile.h
//  FFGlobalAlertController
//
//  Created by Tijn Kooijmans on 05/03/2018.
//

#import <Foundation/Foundation.h>
#import "IKTypes.h"

typedef enum ProfileType : NSInteger {
    ProfileTypeUndefined = -1,
    ProfileTypeUser = 0,
    ProfileTypeIkawa = 1,
    ProfileType3rdParty = 2
} ProfileType;

@interface RoastTempPoint : NSObject
@property (nonatomic, assign) float time;
@property (nonatomic, assign) float temperature;
@end

@interface RoastFanPoint : NSObject
@property (nonatomic, assign) float time;
@property (nonatomic, assign) float power;
@end

@interface Profile : NSObject

@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSString *parentUuid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) int schemaVersion;
@property (nonatomic, assign) int revision;
@property (nonatomic, strong) RoastFanPoint *cooldownPoint;
@property (nonatomic, strong) NSArray *roastPoints;
@property (nonatomic, strong) NSArray *fanPoints;
@property (nonatomic, strong) NSDate *dateCreated;
@property (nonatomic, strong) NSDate *dateLastRoasted;
@property (nonatomic, strong) NSDate *dateLastEdited;
@property (nonatomic, assign) ProfileType type;
@property (nonatomic, assign) TempSensor tempSensor;
@property (nonatomic, assign) BOOL showInLibrary;
@property (nonatomic, assign) BOOL archived;
@property (nonatomic, assign) BOOL favorite;
@property (nonatomic, strong) NSString *cropsterId;
@property (nonatomic, assign) int onServer;

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *coffeeName;
@property (nonatomic, strong) NSString *coffeeId;
@property (nonatomic, strong) NSString *coffeeWebUrl;

@property (nonatomic) BOOL shouldOpenInEditMode;

- (NSData*)convertToProtobuf;
+ (Profile*)convertFromProtobuf:(NSData*)data;
- (BOOL)compare:(Profile *)profile;
- (NSDate *)dateLastUsed;

@end
