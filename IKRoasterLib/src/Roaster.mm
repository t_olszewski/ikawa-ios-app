//
//  Roaster.m
//  IkawaTool
//
//  Created by Tijn Kooijmans on 15/09/14.
//  Copyright (c) 2014 Studio Sophisti. All rights reserved.
//

#import "Roaster.h"
#include "IKTypes.h"

@implementation Roaster

- (id)init {
    if ((self = [super init])) {
        _settings = [NSMutableDictionary dictionaryWithCapacity:50];
        _status = [[RoasterStatus alloc] init];
    }
    return self;
}

- (NSString*)variantName {
    switch ((RoasterVariant)_variant) {
        case RoasterVariantPRO:
            return @"PRO";
        case RoasterVariantNESPRESSO:
            return @"NESPRESSO";
        case RoasterVariantBARE:
            return @"BARE";
        case RoasterVariantHOME:
            return @"HOME";
        default:
            return @"Unknown variant";
    }
}

- (NSString*)typeName {
    switch ((RoasterType)_type) {
        case RoasterTypeV1:
            return @"v1";
        case RoasterTypeV2:
            return @"v2";
        case RoasterTypeV3:
            return @"v3";
        case RoasterTypeV4:
            return @"v4";
        case RoasterTypeV5:
            return @"v5";
        default:
            return @"Unknown type";
    }
}

- (NSString*)errorName {
    switch (_error) {
        case RoasterErrorNone:
            return @"-";
        case RoasterErrorBoardTooHot:
            return @"PCB Board Too Hot";
        case RoasterErrorAboveBeansTooHot:
            return @"Above Beans Sensor > 250 degC";
        case RoasterErrorAboveBeansTooCold:
            return @"Above Beans Sensor < 10 degC";
        case RoasterErrorFanNotSpinning:
            return @"Fan too Slow";
        case RoasterErrorMotorFailure:
            return @"Motor Fault";
        case RoasterErrorBelowBeansTooHot:
            return @"Below Beans Sensor > 300 degC";
        case RoasterErrorBelowBeansTooCold:
            return @"Below Beans Sensor < 10 degC";
        default:
            return @"Unknown Cause";
    }
}

- (NSArray*)sortedSettings {
    return [[_settings allValues] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"settingsId" ascending:YES]]];
}

- (void)setError:(int)error {
    
    if (_error != error && error > RoasterErrorNone) {        
        _error = error;
        
        //TODO: Show error message?
    }
}

@end

@implementation RoasterStatus

- (NSString*)stateName {
    switch (_state) {
        case IK_STATUS_BUSY:
            return @"busy";
        case IK_STATUS_COOLING:
            return @"cooling";
        case IK_STATUS_HEATING:
            return @"pre-heating";
        case IK_STATUS_IDLE:
            return @"idle";
        case IK_STATUS_OPEN:
            return @"doser open";
        case IK_STATUS_PROBLEM:
            return @"error";
        case IK_STATUS_READY:
            return @"ready for roast";
        case IK_STATUS_ROASTING:
            return @"roasting";
        case IK_STATUS_READY_TO_BLOWOVER:
            return @"ready to blowover";
        case IK_STATUS_TEST_MODE:
            return @"test mode";
        default:
            return @"unknown";
    }
}

@end
