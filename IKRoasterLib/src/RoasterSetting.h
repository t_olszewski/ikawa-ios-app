//
//  RoasterSetting.h
//  IkawaTool
//
//  Created by Tijn Kooijmans on 12/09/14.
//  Copyright (c) 2014 Studio Sophisti. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "IKTypes.h"

@interface RoasterSetting : NSObject

@property (nonatomic, assign) int settingsId;
@property (nonatomic, assign) SettingsType type;
@property (nonatomic, strong) NSNumber *value;
@property (nonatomic, strong) NSNumber *defaultValue;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *units;

+ (RoasterSetting*)settingWithId:(int)settingsId;

- (NSString*)displayName;
- (NSString*)displayValue;

@end
