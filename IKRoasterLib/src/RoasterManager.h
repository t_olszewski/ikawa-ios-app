//
//  RoasterManager.h
//  IkawaTool
//
//  Created by Tijn Kooijmans on 11/09/14.
//  Copyright (c) 2014 Studio Sophisti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IKInterfaceDelegate.h"

@class Roaster;
@class RoasterSetting;
@class Roast;
@class FirmwareManager;
@class Profile;

typedef void (^TestResultCallback)(bool, NSError*);

@interface RoasterManager : NSObject <IKInterfaceDelegate>
{
    NSTimer *timeoutTimer, *statusTimer;
    
    int m_txRetries;
    int m_messageSeq;
    int settingListOffset;
    
    TestResultCallback testCallback;
}

@property (nonatomic, strong) FirmwareManager *fwManager;
@property (nonatomic, strong) Roaster *roaster;
@property (nonatomic, strong) NSMutableArray *delegates;
@property (nonatomic, strong) Profile *profile;
@property (nonatomic, assign) float roomTemperature;

+ (RoasterManager*)instance;
- (void)initRoaster;
- (void)flashFirmware;
- (void)requestRoasterData;
- (void)sendProfile:(Profile*)profile;
- (void)updateSetting:(int)settingId withValue:(NSNumber*)value;
- (void)requestSetting:(int)settingId;
- (void)reboot;
- (void)endTest;
- (void)runTest:(int)test;
- (void)runTest:(int)test withCallback:(void (^)(bool, NSError*))callbackBlock;
- (void)runTest:(int)test withDuration:(int)duration andCallback:(void (^)(bool, NSError*))callbackBlock;
- (NSString*)serializeRoasterData;

@end
