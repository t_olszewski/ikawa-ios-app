//
//  IKFirmwareManager.h
//
//  Created by Tijn Kooijmans on 13/7/14.
//
//

#import <Foundation/Foundation.h>
#import "FirmwareManagerDelegate.h"

#define BOOTLOADER_CMD_GET_VERSION      0
#define BOOTLOADER_CMD_JUMP_TO_APP      1
#define BOOTLOADER_CMD_LOAD_DATA        3
#define BOOTLOADER_CMD_PROGRAM_PAGE     4
#define BOOTLOADER_CMD_CRC_BLOCK        5

#define BOOTLOADER_RESP_SUCCESS         0x2B
#define BOOTLOADER_RESP_FAIL            0x2C

#define BOOTLOADER_WRITE_STEP_COMPARE   0
#define BOOTLOADER_WRITE_STEP_LOAD      1
#define BOOTLOADER_WRITE_STEP_PROGRAM   2
#define BOOTLOADER_WRITE_STEP_CHECK     3

#define BOOTLOADER_PAGE_SIZE            256

@class Roaster;
@class Roast;

@interface FirmwareManager : NSObject
{
    unsigned char currentCommand;
    unsigned char *firmwareData;
    int totalPages;
    int currentPageIndex;
    int currentPageAddress;
    int hardwareVersion;
    int bootloaderVersion;
    unsigned char currentStep;
    int pageRetry;
    bool isUpdating;
    
    
}
@property (nonatomic, strong) NSMutableArray *delegates;
@property (nonatomic) int firmwareUpdateVersion;

- (void)startUpdate;
- (void)failUpdate;
- (void)completeUpdate;

- (void)nextPage;
- (void)loadData: (unsigned char*)buffer;
- (void)programCurrentPage;
- (void)crcCurrentBlock;
- (void)sendData: (unsigned char*)data withLength: (int)length;
- (void)sendCommand: (unsigned char)command;
- (void)processPayload: (void*)data;

//firmware update related functions
- (void)setFirmwareData:(NSData*)firmwareData;
- (void)updateFirmware;
- (void)getVersion;
- (void)jumpToApp;

- (bool)hasFirmwareData;
- (bool)isUpdating;


@end
