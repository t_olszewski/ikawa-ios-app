//
//  RoasterManager.m
//  IkawaTool
//
//  Created by Tijn Kooijmans on 11/09/14.
//  Copyright (c) 2014 Studio Sophisti. All rights reserved.
//

#import "RoasterManager.h"
#import "IKInterface.h"
#import "RoasterSetting.h"
#import "Profile.h"
#import "Roaster.h"
#import "RoasterManagerDelegate.h"
#import "IKPeripheral.h"
#include "IKFunctions.h"
#include "spec.pb.h"
#include <queue>
#include "base64.h"
#import "FirmwareManager.h"
#import "ProfileFunctions.h"

#define FRAME_BYTE 0x7e
#define ESCAPE_BYTE 0x7d

#define DATA_BLOCK_SIZE 18

static RoasterManager *__instance = nil;

static std::vector<uint8_t> *m_rxbytes;
static std::queue<Cmd> *m_txQueue;

@interface RoasterManager (Private)
- (void)initRoaster;
@end

@implementation RoasterManager

+ (RoasterManager*)instance {
    if (!__instance) {
        
        m_rxbytes = new std::vector<uint8_t>();
        m_txQueue = new std::queue<Cmd>();
        
        __instance = [[RoasterManager alloc] init];
    }
    return __instance;
}

- (id)init {
    if ((self = [super init])) {
        _delegates = [NSMutableArray arrayWithCapacity:10];
        [IKInterface instance].delegate = self;
        
        m_txRetries = 0;
        m_messageSeq = 0;
        _roomTemperature = 25.0;
        
        _fwManager = [[FirmwareManager alloc] init];
    }
    return self;
}


- (void)peripheralDidConnect:(IKPeripheral*)peripheral {
    
    self.roaster = [[Roaster alloc] init];
    self.roaster.name = [peripheral.peripheral name];
    
    [self initRoaster];
    
    for (id<RoasterManagerDelegate> delegate in _delegates) {
        if ([delegate respondsToSelector:@selector(roasterDidConnect:)])
            [delegate roasterDidConnect:_roaster];
    }
    
    statusTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateStatus) userInfo:nil repeats:YES];
    
}

- (void)peripheralDidDisconnect:(IKPeripheral*)peripheral {
    
    self.roaster = nil;
    self.profile = nil;
    
    NSArray *tempArray = [_delegates copy];
    for (id<RoasterManagerDelegate> delegate in tempArray) {
        if ([delegate respondsToSelector:@selector(roasterDidDisconnect:)])
            [delegate roasterDidDisconnect:_roaster];
    }
    
    if (timeoutTimer) {
        [timeoutTimer invalidate];
        timeoutTimer = nil;
    }
    
    if (statusTimer) {
        [statusTimer invalidate];
        statusTimer = nil;
    }
}

- (void)peripheral:(IKPeripheral*)peripheral didReceiveData:(NSData*)data {
    
    [self processData:data];
}

- (NSString*)serializeRoasterData {
    NSMutableString *s = [NSMutableString new];
    
    [s appendFormat:@"Roaster name: %@\n", self.roaster.name];
    [s appendFormat:@"Roaster id: %d\n", self.roaster.roasterId];
    [s appendFormat:@"Roaster type: %@\n", [self.roaster typeName]];
    [s appendFormat:@"Roaster variant: %@\n", [self.roaster variantName]];
    [s appendFormat:@"Firmware version: %d\n\n", self.roaster.firmwareVersion];
    
    [s appendFormat:@"Profile name: %@\n", _profile.name];
    [s appendFormat:@"Profile uuid: %@\n", _profile.uuid];
    RoastProfile rp = [ProfileFunctions convertToProtobuf:_profile];
    std::string protoProfile;
    rp.SerializeToString(&protoProfile);
    std::string encProfile = base64_encode(protoProfile);
    [s appendFormat:@"Profile url: https://share-ikawa.rhcloud.com/profile/?%@",
     [NSString stringWithUTF8String:encProfile.c_str()]];
    return s;
    
}

- (void)initRoaster {
    NSLog(@"**** INITIALIZING ROASTER ****");
    // clear queue
    while(!m_txQueue->empty()) m_txQueue->pop();
    m_rxbytes->clear();
    
    m_txRetries = 0;
    m_messageSeq = 0;
     // in case we are still in the bootloader, jump back to the app!
    [_fwManager getVersion];
    [self requestRoasterData];
}

- (void)requestRoasterData {
    
    [self sendCommand:MACH_PROP_GET_TYPE];
    [self sendCommand:MACH_PROP_GET_ID];
    [self sendCommand:MACH_PROP_GET_SUPPORT_INFO];
    [self sendCommand:BOOTLOADER_GET_VERSION];
    [self sendCommand:HIST_GET_TOTAL_ROAST_COUNT];
    
    [self requestSetting:SettingIdAboveSensorType];
    [self requestSetting:SettingIdRoasterVoltage];
    [self requestSetting:SettingIdTimeMultiplier];
    [self requestSetting:SettingIdFanOpenLoopControl];
    [self requestSetting:SettingIdRoastCountSinceService];
    [self requestSetting:SettingIdLastServiceDate];
    [self sendCommand:PROFILE_GET];
}

- (void)flashFirmware {
    [self sendCommand:BOOTLOADER_JUMP];
    
    [_fwManager performSelector:@selector(updateFirmware) withObject:nil afterDelay:1.0];
}

- (void)updateStatus {
    if (!m_txQueue->empty()) return;
    
    [self sendCommand:MACH_STATUS_GET_ERROR];
    [self sendCommand:MACH_STATUS_GET_ALL];
    
    if (self.roaster.status.state == IK_STATUS_TEST_MODE) {        
        [self sendCommand:TEST_STATUS_GET];
    }
}

- (void)requestSetting:(int)settingId {
    CmdSettingGet *settingGet = new CmdSettingGet();
    settingGet->set_number(settingId);
    [self sendCommand:SETTING_GET withSetting:settingGet];
}

- (void)updateSetting:(RoasterSetting*)setting {
    CmdSettingSet *setSetting = new CmdSettingSet();
    setSetting->set_number(setting.settingsId);
    if (setting.type == SettingsTypeFloat)
        setSetting->set_val_u32_float([setting.value floatValue] * 1000000);
    else
        setSetting->set_val_u32([setting.value intValue]);
    
    [self sendCommand:SETTING_SET withSetSetting:setSetting];
    
    CmdSettingGet *settingGet = new CmdSettingGet();
    settingGet->set_number(setting.settingsId);
    [self sendCommand:SETTING_GET withSetting:settingGet];
}

- (void)updateSetting:(int)settingId withValue:(NSNumber*)value {
    RoasterSetting *setting = [RoasterSetting settingWithId:settingId];
    setting.value = value;
    [self updateSetting:setting];
}

- (void)reboot {
    NSLog(@"Rebooting");
    
    // clear queue
    //while(!m_txQueue->empty()) m_txQueue->pop();
    
    [self sendCommand:MACH_REBOOT];
    
    // simulate reconnect to reload data and UI
    //[self peripheralDidDisconnect:[IKInterface instance].connectedPeripheral];
    //[self peripheralDidConnect:[IKInterface instance].connectedPeripheral];
}

- (void)endTest {    
    [self sendCommand:END_TEST];
}

- (void)runTest:(int)test {
    [self runTest:test withDuration:-1 andCallback:NULL];
}

- (void)runTest:(int)test withCallback:(void (^)(bool, NSError*))callbackBlock {
    [self runTest:test withDuration:-1 andCallback:callbackBlock];
}

- (void)runTest:(int)test withDuration:(int)duration andCallback:(void (^)(bool, NSError*))callbackBlock {
    testCallback = callbackBlock;
    
    CmdStartTest *startTest = new CmdStartTest();
    startTest->set_test((Test)test);
    switch ((Test)test) {
        case BUTTON_TEST:
            startTest->set_test_time(10000);
            break;
        case CALIBRATE_TEMP_SENSORS:
            startTest->set_test_time(5000);
            break;
        case TEST_TEMP_SENSORS:
            startTest->set_test_time(5000);
            break;
        case TEST_FAN_ERROR:
            startTest->set_test_time(3000);
            break;
        case TEST_FAN_SPIN:
            startTest->set_test_time(5000);
            break;
        case TEST_FAN_STOP:
            startTest->set_test_time(2000);
            break;
        case TEST_HEATER_RELAY:
            startTest->set_test_time(5000);
            break;
        case TEST_HEATER_PWM:
            startTest->set_test_time(1000);
            break;
        case TEST_DOSER:
            startTest->set_test_time(1000);
            break;
        case TEST_BOARD_TEMP:
            startTest->set_room_temp(_roomTemperature);
            startTest->set_test_time(5000);
            break;
        default:
            startTest->set_test_time(1000);
    }
    if (duration != -1) {
        startTest->set_test_time(duration);
    }
    
    NSLog(@"Running test %@", [NSString stringWithUTF8String:Test_Name((Test)test).c_str()]);
    
    _roaster.testStatus = TEST_IN_PROGRESS;
    _roaster.currentTest = test;
    
    [self sendCommand:START_TEST withStartTest:startTest];
}

- (void)sendCommand:(CmdType)command {
    
    Cmd cmd;
    cmd.set_cmd_type(command);
    cmd.set_seq(m_messageSeq++);
    
    if (m_txQueue->empty()) {
        m_txQueue->push(cmd);
        [self sendNextInQueue:NO];
    } else {
        m_txQueue->push(cmd);
    }
}

- (void)sendCommand:(CmdType)command withStartTest:(CmdStartTest*)startTest
{
    Cmd cmd;
    cmd.set_cmd_type(command);
    cmd.set_seq(m_messageSeq++);
    
    if (startTest)
        cmd.set_allocated_test_start(startTest);
    
    if (m_txQueue->empty()) {
        m_txQueue->push(cmd);
        [self sendNextInQueue:NO];
    } else {
        m_txQueue->push(cmd);
    }
}

- (void)sendCommand:(CmdType)command withSetting:(CmdSettingGet*)getSetting
{
    Cmd cmd;
    cmd.set_cmd_type(command);
    cmd.set_seq(m_messageSeq++);
    
    if (getSetting)
        cmd.set_allocated_setting_get(getSetting);
    
    if (m_txQueue->empty()) {
        m_txQueue->push(cmd);
        [self sendNextInQueue:NO];
    } else {
        m_txQueue->push(cmd);
    }
}

- (void)sendCommand:(CmdType)command withProfileSet:(CmdProfileSet*)setProfile
{
    Cmd cmd;
    cmd.set_cmd_type(command);
    cmd.set_seq(m_messageSeq++);
    
    if (setProfile)
        cmd.set_allocated_profile_set(setProfile);
    
    if (m_txQueue->empty()) {
        m_txQueue->push(cmd);
        [self sendNextInQueue:NO];
    } else {
        m_txQueue->push(cmd);
    }
}

- (void)sendCommand:(CmdType)command withSetSetting:(CmdSettingSet*)setSetting
{
    Cmd cmd;
    cmd.set_cmd_type(command);
    cmd.set_seq(m_messageSeq++);
    
    if (setSetting)
        cmd.set_allocated_setting_set(setSetting);
    
    if (m_txQueue->empty()) {
        m_txQueue->push(cmd);
        [self sendNextInQueue:NO];
    } else {
        m_txQueue->push(cmd);
    }
}

- (void)sendCommand:(CmdType)command withGetSensors:(CmdMachStatusGetSensors*)getSensors
{
    Cmd cmd;
    cmd.set_cmd_type(command);
    cmd.set_seq(m_messageSeq++);
    
    if (getSensors)
        cmd.set_allocated_mach_status_get_sensors(getSensors);
    
    if (m_txQueue->empty()) {
        m_txQueue->push(cmd);
        [self sendNextInQueue:NO];
    } else {
        m_txQueue->push(cmd);
    }
}

- (void)sendCommand:(CmdType)command withSettingList:(CmdSettingGetList*)getSettingList
{
    Cmd cmd;
    cmd.set_cmd_type(command);
    cmd.set_seq(m_messageSeq++);
    
    if (getSettingList) {
        cmd.set_allocated_setting_get_list(getSettingList);
        settingListOffset = getSettingList->offset();
    }
    if (m_txQueue->empty()) {
        m_txQueue->push(cmd);
        [self sendNextInQueue:NO];
    } else {
        m_txQueue->push(cmd);
    }
}

- (void)sendCommand:(CmdType)command withSettingInfo:(CmdSettingGetInfo*)getSettingInfo
{
    Cmd cmd;
    cmd.set_cmd_type(command);
    cmd.set_seq(m_messageSeq++);
    
    if (getSettingInfo)
        cmd.set_allocated_setting_get_info(getSettingInfo);
    
    if (m_txQueue->empty()) {
        m_txQueue->push(cmd);
        [self sendNextInQueue:NO];
    } else {
        m_txQueue->push(cmd);
    }
}

- (void)sendNextInQueue:(BOOL)isRetry {
   
    if ([_fwManager isUpdating]) {
        return;
    }
    
    if (m_txQueue->empty()) {
        return;
    } else {
        if (timeoutTimer) {
            [timeoutTimer invalidate];
            timeoutTimer = nil;
        }
        timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(sendTimout) userInfo:nil repeats:NO];
    }
    
    Cmd cmd = m_txQueue->front();
    
    if (cmd.cmd_type() == MACH_REBOOT) {
        // clear queue
        while(!m_txQueue->empty()) m_txQueue->pop();
        
        //don't retry this!
        [timeoutTimer invalidate];
        timeoutTimer = nil;
    }
    
    std::string cmdString;
    cmd.SerializeToString(&cmdString);
    
    //calculate crc
    uint16_t crc = crc16((char*)cmdString.c_str(), (int)cmdString.length(), 0xFFFF);
    
    //append crc to string
    cmdString.push_back((char)(crc >> 8)); //crc msb
    cmdString.push_back((char)crc); //crc lsb
    
    //frame and escape the string
    std::vector<uint8_t> bytes;
    bytes.push_back(FRAME_BYTE); //start frame
    std::string::iterator it;
    for (it = cmdString.begin(); it != cmdString.end(); ++it) {
        if (*it == ESCAPE_BYTE) { //escape the escape character 0x7d
            bytes.push_back(ESCAPE_BYTE);
            bytes.push_back(ESCAPE_BYTE ^ 0x20);
        } else if (*it == FRAME_BYTE) { //escape the frame character 0x7e
            bytes.push_back(ESCAPE_BYTE);
            bytes.push_back(FRAME_BYTE ^ 0x20);
        } else {
            bytes.push_back(*it);
        }
    }
    bytes.push_back(FRAME_BYTE); //end frame
    
    uint8_t *data = new uint8_t[bytes.size()];
    copy(bytes.begin(), bytes.end(), data);
    
    //send the data in DATA_BLOCK_SIZE chunks
    int byteIndex = 0;
    int chunkSize = 0;
    while (byteIndex < bytes.size()) {
        chunkSize = MIN(DATA_BLOCK_SIZE, (int)bytes.size() - byteIndex);
        [[IKInterface instance] streamData:data + byteIndex andLength:chunkSize];
        byteIndex += DATA_BLOCK_SIZE;
    }
    
    delete[] data;
}

- (void)sendTimout
{
    m_txRetries++;
    
    if (m_txRetries == 3) {
        m_txRetries = 0;
        NSLog(@"Timeout sending command, purging!");
        
        if (!m_txQueue->empty())
            m_txQueue->pop();
        
        [self sendNextInQueue:NO];
    } else {
        NSLog(@"Timeout sending command, retrying #%d", m_txRetries);
        [self sendNextInQueue:YES];
    }
}

- (void)sendProfile:(Profile*)profile;
{
    RoastProfile pb = [ProfileFunctions convertToProtobuf:profile];
    CmdProfileSet *cmd = new CmdProfileSet();
    cmd->set_allocated_profile(new RoastProfile(pb));
    [self sendCommand:PROFILE_SET withProfileSet:cmd];
    
    [self sendCommand:PROFILE_GET];
}

- (void)processSettingsList:(const RespSettingGetList&)list {
    
    for (int i = 0; i < list.number_size(); i++) {
        RoasterSetting *setting = nil;
        setting = [RoasterSetting settingWithId:list.number(i)];
        if (setting) {
            [_roaster.settings setObject:setting forKey:@(list.number(i))];
            
            CmdSettingGet *settingGet = new CmdSettingGet();
            settingGet->set_number(list.number(i));
            [self sendCommand:SETTING_GET withSetting:settingGet];
        }
    }
    
    if (list.number_size() == 32) {
        CmdSettingGetList *settingGetList = new CmdSettingGetList();
        settingGetList->set_offset(settingListOffset + 32);
        [self sendCommand:SETTING_GET_LIST withSettingList:settingGetList];
    }
}

- (void)processSettingGet:(const RespSettingGet&)sGet {
    NSNumber *value = nil;
    if (sGet.has_val_u32()) {
        value = @(sGet.val_u32());
    } else if (sGet.has_val_u32_float()) {
        value = @(sGet.val_u32_float() / 1000000.0f);
    }
    Cmd cmd = m_txQueue->front();
    int settingId = cmd.setting_get().number();
    if (value) {
        if (cmd.has_setting_get()) {
            RoasterSetting *setting = [_roaster.settings objectForKey:@(cmd.setting_get().number())];
            setting.value = value;
            if (settingId == SettingIdRoasterVoltage) {
                _roaster.roasterVoltage = (RoasterVoltage)value.integerValue;
            } else if (settingId == SettingIdAboveSensorType) {
                _roaster.aboveSensorType = (AboveSensorType)value.integerValue;
            } else if (settingId == SettingIdTimeMultiplier) {
                _roaster.timeMultiplier = value.floatValue;

            } else if (settingId == SettingIdFanOpenLoopControl) {
                if (value.floatValue > 0) {
                    _roaster.fanOpenLoopControl = @"Open Loop";
                } else {
                    _roaster.fanOpenLoopControl = @"Closed Loop";
                }
            } else if (settingId == SettingIdLastServiceDate) {
                _roaster.roastCountSinceService = value.integerValue;
            } else if (settingId == SettingIdLastServiceDate) {
                if (value.integerValue > 0) {
                    _roaster.lastServiceDate = [NSDate dateWithTimeIntervalSince1970:value.integerValue];
                }
            }
            
            for (id<RoasterManagerDelegate> delegate in _delegates) {
                if ([delegate respondsToSelector:@selector(roaster:didReceiveSetting:withValue:success:)])
                    [delegate roaster:_roaster didReceiveSetting:cmd.setting_get().number() withValue:value success:YES];
            }
        }
    }

    

    
}

- (void)processSettingGetInfo:(const RespSettingGetInfo&)sGetInfo {
    Cmd cmd = m_txQueue->front();
    if (cmd.has_setting_get_info()) {
        RoasterSetting *setting = [_roaster.settings objectForKey:@(cmd.setting_get_info().number())];
        setting.name = [NSString stringWithUTF8String:sGetInfo.name().c_str()];
        setting.type = (SettingsType)sGetInfo.type();
        NSLog(@"%@ = %@", setting.name, setting.value);
    }
}


- (void)processStatusGetSensors:(const RespMachStatusGetSensors&)statusSensors {
    NSLog(@"Received %d sensor readings", statusSensors.reading_size());
    
    if (statusSensors.reading_size() > 0) {
        SensorReadings readings = statusSensors.reading(statusSensors.reading_size() - 1);
     
        _roaster.status.temp_above = readings.temp_above() / 10.0f;
        _roaster.status.temp_below = readings.temp_below() / 10.0f;

    }
}

- (void)processTestStatus:(const RespTestStatusGet&)testStatus {
    if (_roaster.testStatus == TEST_IN_PROGRESS || _roaster.testStatus == NO_TEST_STARTED) {
        if (testStatus.status() == TEST_SUCCESS && _roaster.currentTest == testStatus.test()) {
            for (id<RoasterManagerDelegate> delegate in _delegates) {
                if ([delegate respondsToSelector:@selector(roasterDidFinishTest:withStatus:andFailure:)])
                    [delegate roasterDidFinishTest:_roaster withStatus:testStatus.status() andFailure:NO_FAILURE];
            }
            if (testCallback != NULL) {
                testCallback(true, nil);
                testCallback = NULL;
            }
            _roaster.testStatus = testStatus.status();
        } else if (testStatus.status() == TEST_FAILED && _roaster.currentTest == testStatus.test()) {
            for (id<RoasterManagerDelegate> delegate in _delegates) {
                if ([delegate respondsToSelector:@selector(roasterDidFinishTest:withStatus:andFailure:)])
                    [delegate roasterDidFinishTest:_roaster withStatus:testStatus.status() andFailure:testStatus.failure()];
            }
            
            if (testCallback != NULL) {
                NSError *error = [NSError errorWithDomain:NSCocoaErrorDomain code:testStatus.failure() userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithUTF8String:TestFailure_Name(testStatus.failure()).c_str()]}];
                testCallback(false, error);
                testCallback = NULL;
            }
            _roaster.testStatus = testStatus.status();
        }
    }
}

- (void)processProfileGet:(const RoastProfile&)profile {
    Profile *convertedProfile = [ProfileFunctions convertFromProtobuf:profile];
//    if (_profile != nil && [convertedProfile.uuid isEqualToString:_profile.uuid]) {
//        return;
//    }
    _profile = convertedProfile;
    NSMutableArray *delegates = [_delegates mutableCopy];
    for (id<RoasterManagerDelegate> delegate in delegates) {
        if ([delegate respondsToSelector:@selector(roaster:didUpdateProfile:)])
            [delegate roaster:_roaster didUpdateProfile:_profile];
    }
}

- (void)processStatus:(const RespMachStatusGetAll&)statusAll {
    RoasterStatus *status = [[RoasterStatus alloc] init];
    status.time = statusAll.time() / 10.0f;
    status.fanSet = statusAll.fan() / 2.55f;
    status.fanSpeed = (statusAll.fan_measured() / 12.0f) * 60.0f;
    status.state = (RoasterState)statusAll.state();
    status.heater = statusAll.heater() * 2;
    status.p = (int32_t)statusAll.p() / 10.0f;
    status.i = (int32_t)statusAll.i() / 10.0f;
    status.d = (int32_t)statusAll.d() / 10.0f;
    if (statusAll.has_fan_p())
        status.fan_p = (int32_t)statusAll.fan_p() / 10.0f;
    if (statusAll.has_fan_i())
        status.fan_i = (int32_t)statusAll.fan_i() / 10.0f;
    if (statusAll.has_fan_d())
        status.fan_d = (int32_t)statusAll.fan_d() / 10.0f;
    status.setpoint = statusAll.setpoint() / 10.0f;
    if (statusAll.has_board_temp())
        status.temp_board = statusAll.board_temp() / 10.0f;
    
    
    if (statusAll.has_temp_below_filtered()) {
        status.temp_below = statusAll.temp_below_filtered() / 10.0f;
    } else {
        status.temp_below = statusAll.temp_below() / 10.0f;
    }

    
    if (statusAll.has_ror_above()) {
        status.ror_above = statusAll.ror_above() / 10.0f;
    } else {
        status.ror_above = 9999.9f;
    }
    
    if (statusAll.has_temp_above())
        status.temp_above = statusAll.temp_above() / 10.0f;
    if (statusAll.has_fan_rpm_measured())
        status.fanSpeed = statusAll.fan_rpm_measured();
    if (statusAll.has_fan_rpm_setpoint())
        status.fan_rpm_set = statusAll.fan_rpm_setpoint();
    if (statusAll.has_fan_power())
        status.fan_power = statusAll.fan_power() / 2.55f;
    if (statusAll.has_j())
        status.j = statusAll.j() / 10.0f;
    if (statusAll.has_relay_state())
        status.relay_state = (bool)statusAll.relay_state();
    _roaster.status = status;
    status.error_code = _roaster.error;
    
    for (id<RoasterManagerDelegate> delegate in _delegates) {
        if ([delegate respondsToSelector:@selector(roasterDidUpdateStatus:)])
            [delegate roasterDidUpdateStatus:_roaster];
    }
}

- (void)processData:(NSData*)data  {
    
    unsigned char* bytes = (unsigned char*)[data bytes];
    
    for (int i = 0; i < [data length]; i++) {
        if (bytes[i] == FRAME_BYTE) {
            if (!m_rxbytes->empty()) {
                [self processPayload];
                m_rxbytes->clear();
            }
        } else {
            m_rxbytes->push_back(bytes[i]);
        }
    }
}

- (void)processPayload
{
    //unescape the payload
    std::vector<uint8_t> bytes;
    std::vector<uint8_t>::iterator it;
    for (it = m_rxbytes->begin(); it != m_rxbytes->end(); ++it) {
        if (*it == ESCAPE_BYTE) { //unescape the next character
            ++it;
            bytes.push_back(*it ^ 0x20);
        } else {
            bytes.push_back(*it);
        }
    }
    if (bytes.size() < 2) {
        return;
    }
    
    std::string rspString(bytes.begin(), bytes.end()-2);
        
    //calculate crc
    uint16_t crc = crc16((char*)rspString.c_str(), (int)rspString.length(), 0xFFFF);
    uint16_t crc2 = crc16((char*)rspString.c_str(), (int)rspString.length(), 0xAAAA);
    
    uint16_t r_crc = ((uint16_t)bytes.at(bytes.size() - 2) << 8) | bytes.at(bytes.size() - 1) ;
    
    if (crc2 == r_crc) {
        [_fwManager processPayload: &bytes];
        
    } else if (crc == r_crc) {
        Response resp;
        resp.ParseFromString(rspString);
        
        if (resp.resp() == Response_GenericResp_OK) {
            
            if (resp.has_resp_bootloader_get_version()) {
                NSLog(@"Firmware version = %d", resp.resp_bootloader_get_version().version());
                
                _roaster.firmwareVersion = resp.resp_bootloader_get_version().version();
                _roaster.firmwareBuild = [NSString stringWithUTF8String:resp.resp_bootloader_get_version().revision().c_str()];
                for (id<RoasterManagerDelegate> delegate in _delegates) {
                    if ([delegate respondsToSelector:@selector(roasterDidDidReceiveFirmwareVersion:)])
                        [delegate roasterDidDidReceiveFirmwareVersion:_roaster];
                }
                
            } else if (resp.has_resp_mach_prop_type()) {
                NSLog(@"Roaster type = %d", resp.resp_mach_prop_type().type());
                NSLog(@"Roaster variant = %d", resp.resp_mach_prop_type().variant());
                
                _roaster.type = (RoasterType)resp.resp_mach_prop_type().type();
                _roaster.variant = (RoasterVariant)resp.resp_mach_prop_type().variant();
                for (id<RoasterManagerDelegate> delegate in _delegates) {
                    if ([delegate respondsToSelector:@selector(roasterDidUpdateMachType:)])
                        [delegate roasterDidUpdateMachType:_roaster];
                }
            } else if (resp.has_resp_mach_id()) {
                NSLog(@"Roaster id = %d", resp.resp_mach_id().id());
                
                _roaster.roasterId = resp.resp_mach_id().id();
                
            } else if (resp.has_resp_mach_status_get_all()) {
                [self processStatus:resp.resp_mach_status_get_all()];
                
            } else if (resp.has_resp_profile_get()) {
                [self processProfileGet:resp.resp_profile_get().profile()];
                
            } else if(resp.has_resp_mach_status_get_error()) {
                _roaster.error = (RoasterError)resp.resp_mach_status_get_error().error();
                
            } else if(resp.has_resp_hist_get_total_roast_count()) {
                NSLog(@"Roast count = %d", resp.resp_hist_get_total_roast_count().count());
                
                _roaster.roastCount = resp.resp_hist_get_total_roast_count().count();
                
                for (id<RoasterManagerDelegate> delegate in _delegates) {
                    if ([delegate respondsToSelector:@selector(roasterDidUpdateInfo:)])
                        [delegate roasterDidUpdateInfo:_roaster];
                }
                
            } else if(resp.has_resp_setting_get_list()) {
                [self processSettingsList:resp.resp_setting_get_list()];
                
            } else if(resp.has_resp_setting_get()) {
                [self processSettingGet:resp.resp_setting_get()];
                
            } else if(resp.has_resp_setting_get_info()) {
                [self processSettingGetInfo:resp.resp_setting_get_info()];
                
            } else if(resp.has_resp_mach_status_get_sensors()) {
                [self processStatusGetSensors:resp.resp_mach_status_get_sensors()];
                
            } else if(resp.has_resp_test_status_get()) {
                [self processTestStatus:resp.resp_test_status_get()];
                
            }
            
            
        } else if (resp.resp() == Response_GenericResp_ERROR) {
            NSLog(@"Error response!");
            
            /* check if we requested a setting */
            int settingId = -1;
            if (m_txQueue->size() > 0) {
                Cmd cmd = m_txQueue->front();
                if (cmd.has_setting_get()) {
                    settingId = cmd.setting_get().number();
                    
                    for (id<RoasterManagerDelegate> delegate in _delegates) {
                        if ([delegate respondsToSelector:@selector(roaster:didReceiveSetting:withValue:success:)])
                            [delegate roaster:_roaster didReceiveSetting:cmd.setting_get().number() withValue:@(0) success:NO];
                    }
                    
                }
            }
            
        } else {
            NSLog(@"Unknown response!");
        }
        
        if (!m_txQueue->empty()) {
            //check if this is the front message in the queue
            Cmd cmd = m_txQueue->front();
            
            if (cmd.seq() == resp.seq()) {
                m_txQueue->pop();
                m_txRetries = 0;
                [self sendNextInQueue:NO];
            }
        }
       
        
    } else {
        NSLog(@"Checksum error!");
        return;
    }
    
}


@end
