//
//  RoasterSetting.m
//  IkawaTool
//
//  Created by Tijn Kooijmans on 12/09/14.
//  Copyright (c) 2014 Studio Sophisti. All rights reserved.
//

#import "RoasterSetting.h"
#include "IKTypes.h"

@implementation RoasterSetting

+ (RoasterSetting*)settingWithId:(int)settingsId {
    NSString* path = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"plist"];
    NSDictionary *rootDict = [NSDictionary dictionaryWithContentsOfFile:path];
    NSDictionary *settingsDict = rootDict[@"settings"];
    NSDictionary *settingDict = settingsDict[[NSString stringWithFormat:@"%d", settingsId]];
    
    if (!settingsDict) {
        return nil;
    }
    
    RoasterSetting *setting = [[RoasterSetting alloc] init];
    setting.settingsId = settingsId;
    setting.name = settingDict[@"name"];
    setting.desc = settingDict[@"desc"];
    setting.defaultValue = settingDict[@"default"];
    setting.units = settingDict[@"units"];
    setting.value = nil;
    
    NSString *typeString = settingDict[@"type"];
    if ([typeString isEqualToString:@"uint8"]) {
        setting.type = SettingsTypeUint8;
    } else if ([typeString isEqualToString:@"uint16"]) {
        setting.type = SettingsTypeUint16;
    } else if ([typeString isEqualToString:@"uint32"]) {
        setting.type = SettingsTypeUint32;
    } else if ([typeString isEqualToString:@"float"]) {
        setting.type = SettingsTypeFloat;
    }
    
    return setting;
}

- (NSString*)displayName {
    return [[_name stringByReplacingOccurrencesOfString:@"_" withString:@" "] capitalizedString];
}

- (NSString*)displayValue {
    if (!self.value) return @"";
    
    if (_type == SettingsTypeFloat) {
        return [NSString stringWithFormat:@"%.3f", _value.floatValue];
    } else {
        return [NSString stringWithFormat:@"%d", _value.intValue];
    }
}

@end
