//
//  Created by Tijn Kooijmans on 05-10-13.
//  Copyright (c) 2013 Studio Sophisti. All rights reserved.
//

#import "IKPeripheral.h"

@implementation IKPeripheral

@synthesize delegate;
@synthesize peripheral = peripheral_;

- (id)init {
    if ((self = [super init])) {
        
        messageQueue = [[NSMutableArray alloc] initWithCapacity:10];
    }
    return self;
}

- (void)dealloc {
     rxChar = nil;
     txChar = nil;
     btAddrCHar = nil;
     baudRateChar = nil;
}

- (void)streamNextMessageInQueue {
    
    if ([messageQueue count] == 0) {
        isStreaming = NO;
        return;
    }
    
    isStreaming = YES;
    
    NSData *messageData = [messageQueue objectAtIndex:0];
    [self sendBlock:(unsigned char*)messageData.bytes withLength:(int)messageData.length];
    [messageQueue removeObjectAtIndex:0];
}

- (void)sendBlock:(unsigned char*)bytes withLength:(int)blockSize {
    if (txChar) {
        NSMutableData *data = [NSMutableData dataWithBytes:bytes length:blockSize];
        //NSLog(@"Sending: %@", [data description]);
        [peripheral_ writeValue:data forCharacteristic:txChar type:CBCharacteristicWriteWithResponse];
    }
}


- (void)streamData:(const unsigned char*)bytes andLength:(int)dataLength {
    
    if (txChar) {
        
        NSData *messageData = [NSData dataWithBytes:bytes length:dataLength];      
                
        [messageQueue addObject:messageData];
        
        if (!isStreaming) {
            [self streamNextMessageInQueue];
        }
    }
}

- (void)discoverServices {
    NSMutableArray *services = [NSMutableArray arrayWithObjects:
                                [CBUUID UUIDWithString:IK_DATA_SERVICE_UUID], nil];
    
    [peripheral_ discoverServices:services];
    
}


#pragma mark - CBPeripheralDelegate


- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    
    for (CBService *service in peripheral.services) {
        
       if ([service.UUID isEqual:[CBUUID UUIDWithString:IK_DATA_SERVICE_UUID]]) {
           
            [peripheral discoverCharacteristics:[NSArray arrayWithObjects:
                                                 [CBUUID UUIDWithString:IK_DATA_RX_CHAR_UUID],
                                                 [CBUUID UUIDWithString:IK_DATA_TX_CHAR_UUID],
                                                 [CBUUID UUIDWithString:IK_BT_ADDR_CHAR_UUID],
                                                 [CBUUID UUIDWithString:IK_BAUD_RATE_CHAR_UUID], nil]
                                     forService:service];
        }
        
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    
    for (CBCharacteristic *characteristic in service.characteristics) {
        
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:IK_DATA_RX_CHAR_UUID]]) {
            
            rxChar = characteristic;
            
            [peripheral setNotifyValue:YES forCharacteristic:rxChar];
            
        } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:IK_DATA_TX_CHAR_UUID]]) {
            
            txChar = characteristic;
            
            if (delegate && [delegate respondsToSelector:@selector(peripheralDidConnect:)]) {
                [delegate peripheralDidConnect:self];
            }
            
        } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:IK_BT_ADDR_CHAR_UUID]]) {
            
            btAddrCHar = characteristic;
            
        } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:IK_BAUD_RATE_CHAR_UUID]]) {
            
            baudRateChar = characteristic;
            
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    if (characteristic == txChar) {
        [self streamNextMessageInQueue];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    [delegate peripheral:self didReceiveData:characteristic.value];
}

@end
