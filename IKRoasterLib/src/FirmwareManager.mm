//
//  IKFirmwareManager.cpp
//
//  Created by Tijn Kooijmans on 13/7/14.
//
//

#import "FirmwareManager.h"
#import "RoasterManager.h"
#import "IKInterface.h"
#import "Roaster.h"
#import "IKPeripheral.h"
#include "IKFunctions.h"
#include <queue>
#import "IKTypes.h"

#define FRAME_BYTE 0x7e
#define ESCAPE_BYTE 0x7d
#define DATA_BLOCK_SIZE 18
#define IK_FIRMWARE_UPDATE_VERSION 24


@implementation FirmwareManager

- (id)init {
    if ((self = [super init])) {
        _delegates = [NSMutableArray arrayWithCapacity:10];
        currentCommand = 0xff;
        currentStep = 0xff;
        isUpdating = false;
        hardwareVersion = -1;
        totalPages = 0;
        _firmwareUpdateVersion = IK_FIRMWARE_UPDATE_VERSION;
    }
    return self;
}


- (void)processPayload: (void*)bytes {
    std::vector<uint8_t> *data = (std::vector<uint8_t> *)bytes;
    
    uint16_t crc_r, crc;
    
    switch (currentCommand) {
        case BOOTLOADER_CMD_GET_VERSION:
            bootloaderVersion = data->at(0) >> 4; //we currently don't use the bootloader version
            hardwareVersion = data->at(0) & 0x0f;
            NSLog(@"Hardware version: %d", hardwareVersion);
            NSLog(@"Bootloader version: %d", bootloaderVersion);
            
            if (isUpdating)
                [self startUpdate];
            else
                [self jumpToApp];
            
            break;
            
        case BOOTLOADER_CMD_JUMP_TO_APP:
            if (data->at(0) == BOOTLOADER_RESP_SUCCESS) {
                NSLog(@"Bootloader jump back to app succeeded");
                for (id<FirmwareManagerDelegate> delegate in _delegates) {
                    if ([delegate respondsToSelector:@selector(firmwareJumpedToApp)])
                        [delegate firmwareJumpedToApp];
                }
                            
            } else {
                NSLog(@"Bootloader jump back to app failed");
                
                // in case we are stuck in the bootloader, try updating the firmware again!
                [self updateFirmware];
            }
            break;
            
        case BOOTLOADER_CMD_LOAD_DATA:
            if (data->at(0) == BOOTLOADER_RESP_SUCCESS) {
                NSLog(@"Bootloader load data success");
                
                if (currentStep == BOOTLOADER_WRITE_STEP_LOAD) {
                    currentStep = BOOTLOADER_WRITE_STEP_PROGRAM;
                    [self programCurrentPage];
                }
            }
            break;
            
        case BOOTLOADER_CMD_PROGRAM_PAGE:
            if (data->at(0) == BOOTLOADER_RESP_SUCCESS) {
                NSLog(@"Bootloader page program success");
                
                if (currentStep == BOOTLOADER_WRITE_STEP_PROGRAM) {
                    currentStep = BOOTLOADER_WRITE_STEP_CHECK;
                    [self crcCurrentBlock];
                }
                
            }
            break;
            
        case BOOTLOADER_CMD_CRC_BLOCK:
            crc_r = crc16((char*)(firmwareData + currentPageIndex * BOOTLOADER_PAGE_SIZE), BOOTLOADER_PAGE_SIZE, 0xaaaa);
            crc = ((uint16_t)data->at(0) << 8) | data->at(1);
            
            if (currentStep == BOOTLOADER_WRITE_STEP_COMPARE) {
                
                if (crc == crc_r) {
                    [self nextPage];
                    
                } else {
                    currentStep = BOOTLOADER_WRITE_STEP_LOAD;
                    [self loadData:firmwareData + currentPageIndex * BOOTLOADER_PAGE_SIZE];
                }
                
            } else if (currentStep == BOOTLOADER_WRITE_STEP_CHECK) {
                
                if (crc == crc_r) {
                    [self nextPage];
                    
                } else if (pageRetry > 0) {
                    NSLog(@"Bootloader page crc error (%d != %d), failing...", crc_r, crc);
                    [self failUpdate];
                    
                } else {
                    NSLog(@"Bootloader page crc error (%d != %d), retrying...", crc_r, crc);
                    pageRetry++;
                    
                    currentStep = BOOTLOADER_WRITE_STEP_LOAD;
                    [self loadData:firmwareData + currentPageIndex * BOOTLOADER_PAGE_SIZE];
                }
            }
            
            break;

        default:
            break;
    }
}

- (void)setFirmwareData:(NSData*)data {
    
    firmwareData = (unsigned char*)malloc([data length]);
    memcpy(firmwareData, [data bytes], [data length]);
    totalPages = ceil(data.length / (float)BOOTLOADER_PAGE_SIZE);
}

- (void)updateFirmware{
    isUpdating = true;
    
    //start by getting the bootloader version to check which image to use
    [self getVersion];
}

- (void)getVersion {
    [self sendCommand:BOOTLOADER_CMD_GET_VERSION];
    
}

- (void)jumpToApp {
    [self sendCommand:BOOTLOADER_CMD_JUMP_TO_APP];
    currentStep = 0xff;
}

- (void)startUpdate {
    NSString *fileName = [self getFirmwareBinaryPath];
    NSLog(fileName);
    
    //retrieve file content
    NSData *data = [[NSData alloc] initWithContentsOfFile:fileName];
    
    
    if (!data) {
        [self failUpdate];
        return;
    }
    
    [self setFirmwareData:data];
    
    if (totalPages == 0) {
        [self failUpdate];
        return;
    }
    NSArray *tempArray = [_delegates copy];
    for (id<FirmwareManagerDelegate> delegate in tempArray) {
        if ([delegate respondsToSelector:@selector(firmwareUpdateStarted)])
            [delegate firmwareUpdateStarted];
    }
    currentPageIndex = -1;
    
    [self nextPage];
}

- (void)failUpdate {
    isUpdating = false;
    
    [self jumpToApp];
    for (id<FirmwareManagerDelegate> delegate in _delegates) {
        if ([delegate respondsToSelector:@selector(firmwareUpdateFailed)])
            [delegate firmwareUpdateFailed];
    }
    
    free(firmwareData);
}

- (void)completeUpdate {
    isUpdating = false;
    
    [self jumpToApp];
    for (id<FirmwareManagerDelegate> delegate in _delegates) {
        if ([delegate respondsToSelector:@selector(firmwareUpdateComplete)])
            [delegate firmwareUpdateComplete];
    }
    
    free(firmwareData);
}

- (void)nextPage {
    currentPageIndex++;
    currentPageAddress = currentPageIndex * BOOTLOADER_PAGE_SIZE / 2;
    pageRetry = 0;
    
    if (currentPageIndex < totalPages) {
        for (id<FirmwareManagerDelegate> delegate in _delegates) {
            if ([delegate respondsToSelector:@selector(firmwareProgressChanged:)])
                [delegate firmwareProgressChanged:(float)(currentPageIndex+1) / totalPages];
        }
        
        NSLog(@"Bootloader checking page %d of %d", currentPageIndex + 1, totalPages);
        
        currentStep = BOOTLOADER_WRITE_STEP_COMPARE;
        [self crcCurrentBlock];
        
    } else {
        [self completeUpdate];
    }
    
}

- (void)loadData: (unsigned char*)buffer {
    
    currentCommand = BOOTLOADER_CMD_LOAD_DATA;
    
    unsigned char data[BOOTLOADER_PAGE_SIZE + 2];
    data[0] = BOOTLOADER_CMD_LOAD_DATA;
    data[1] = 0;
    memcpy(data+2, buffer, BOOTLOADER_PAGE_SIZE);
    
    [self sendData:data withLength:BOOTLOADER_PAGE_SIZE + 2];
}

- (void)programCurrentPage {
    
    currentCommand = BOOTLOADER_CMD_PROGRAM_PAGE;
    
    unsigned char data[3];
    data[0] = BOOTLOADER_CMD_PROGRAM_PAGE;
    data[1] = (unsigned char)(currentPageAddress >> 8);
    data[2] = (unsigned char)currentPageAddress;
    
    [self sendData:data withLength:3];
}

- (void)crcCurrentBlock {
    
    currentCommand = BOOTLOADER_CMD_CRC_BLOCK;
    
    unsigned char data[5];
    data[0] = BOOTLOADER_CMD_CRC_BLOCK;
    data[1] = (unsigned char)(currentPageAddress >> 8);
    data[2] = (unsigned char)currentPageAddress;
    data[3] = (unsigned char)(BOOTLOADER_PAGE_SIZE >> 8);
    data[4] = (unsigned char)BOOTLOADER_PAGE_SIZE;
    
    [self sendData:data withLength:5];
}

- (void)sendCommand: (unsigned char)command {
    
    currentCommand = command;
    
    unsigned char data[1];
    data[0] = command;
    
    [self sendData:data withLength:1];
}

- (void)sendData: (unsigned char*)buffer withLength: (int)length {
    
    std::string cmdString;
    for (int i = 0; i < length; i++) {
        cmdString.push_back(buffer[i]);
    }
    
    //calculate crc
    uint16_t crc = crc16((char*)cmdString.c_str(), (int)cmdString.length(), 0xaaaa);
    
    //append crc to string
    cmdString.push_back((char)(crc >> 8)); //crc msb
    cmdString.push_back((char)crc); //crc lsb
    
    //frame and escape the string
    std::vector<uint8_t> bytes;
    bytes.push_back(FRAME_BYTE); //start frame
    std::string::iterator it;
    for (it = cmdString.begin(); it != cmdString.end(); ++it) {
        if (*it == ESCAPE_BYTE) { //escape the escape character 0x7d
            bytes.push_back(ESCAPE_BYTE);
            bytes.push_back(ESCAPE_BYTE ^ 0x20);
        } else if (*it == FRAME_BYTE) { //escape the frame character 0x7e
            bytes.push_back(ESCAPE_BYTE);
            bytes.push_back(FRAME_BYTE ^ 0x20);
        } else {
            bytes.push_back(*it);
        }
    }
    bytes.push_back(FRAME_BYTE); //end frame
    
    uint8_t *sendData = new uint8_t[bytes.size()];
    copy(bytes.begin(), bytes.end(), sendData);
    
    //send the data in DATA_BLOCK_SIZE chunks
    int byteIndex = 0;
    int chunkSize = 0;
    while (byteIndex < bytes.size()) {
        chunkSize = MIN(DATA_BLOCK_SIZE, (int)bytes.size() - byteIndex);
        
        //for(int j = 0; j < chunkSize; j++)
        //    CCLOG("sending byte: 0x%02X", sendData[byteIndex + j]);
        [[IKInterface instance] streamData:sendData + byteIndex andLength:chunkSize];
        //Interface::getInstance()->sendDataToRoaster(sendData + byteIndex, chunkSize);
        byteIndex += DATA_BLOCK_SIZE;
    }
    
    delete[] sendData;
    
//    uint8_t *data = new uint8_t[bytes.size()];
//    copy(bytes.begin(), bytes.end(), data);
//    [[IKInterface instance] streamData:data andLength:(int)bytes.size()];
//    delete[] data;
}

- (bool)hasFirmwareData {
    return totalPages > 0;
}

- (bool)isUpdating {
    return isUpdating;
}

- (NSString *)getFirmwareBinaryPath{
    switch (hardwareVersion) {
        case 0:
            return [[NSBundle mainBundle] pathForResource:@"main-avr-v1"
                                                   ofType:@"bin"
                                              inDirectory:[NSString stringWithFormat:@"%@/%d", @"firmware", self.firmwareUpdateVersion]];
            
        case 1:
            return [[NSBundle mainBundle] pathForResource:@"main-avr-v2"
                                                   ofType:@"bin"
                                              inDirectory:[NSString stringWithFormat:@"%@/%d", @"firmware", self.firmwareUpdateVersion]];
        case 4:
            return [[NSBundle mainBundle] pathForResource:@"main-avr-v4"
                                                   ofType:@"bin"
                                              inDirectory:[NSString stringWithFormat:@"%@/%d", @"firmware", self.firmwareUpdateVersion]];
        default:
            return @"";
    }
    return @"";
}
@end
