//
//  Created by Tijn Kooijmans on 05-10-13.
//  Copyright (c) 2013 Studio Sophisti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/Corebluetooth.h>
#import "IKInterfaceDelegate.h"

#define CONNECTION_TIMEOUT_SEC  5

typedef enum {
    kConnectionStateIdle = 0,
    kConnectionStateScanning,
    kConnectionStateConnecting,
    kConnectionStateConnected
} ConnectionState;

@class IKPeripheral;

@interface IKInterface : NSObject <CBCentralManagerDelegate>
{
    ConnectionState conState;
    
    //CBCentralManager *manager;
    
    NSMutableArray *scannedPeripherals;
    NSMutableArray *blockedPeripheralUuids;
    IKPeripheral *connectedPeripheral;
        
    NSTimer *scanTimer;
    
    id<IKInterfaceDelegate> __weak delegate;
    
    UIAlertController *deviceSheet;
    
    BOOL searchingMore;
    CBCentralManager *manager;
}

@property(nonatomic, weak) id<IKInterfaceDelegate> delegate;


+ (IKInterface*)instance;

- (BOOL)isConnected;
- (BOOL)isBluetoothEnabled;
- (void)disconnect;
- (void)stopScan;
- (void)connect;
- (void)reconnect;
- (void)blockPeripheral;
- (void)streamData:(const unsigned char*)bytes andLength:(int)dataLength;

@end
