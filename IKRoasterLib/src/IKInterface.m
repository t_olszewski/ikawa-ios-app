//
//  Created by Tijn Kooijmans on 05-10-13.
//  Copyright (c) 2013 Studio Sophisti. All rights reserved.
//

#import "IKInterface.h"
#import "IKPeripheral.h"
#import "UIAlertController+Window.h"

static IKInterface* instance__ = nil;



@implementation IKInterface {
    BOOL isConnecting;
}

@synthesize delegate;

+ (IKInterface*)instance {
    if (!instance__) {
        instance__ = [[IKInterface alloc] init];
    }
    return instance__;
}

- (id)init {
    if ((self = [super init])) {
        
        blockedPeripheralUuids = [[NSMutableArray alloc] initWithCapacity:10];
        scannedPeripherals = [[NSMutableArray alloc] initWithCapacity:10];
        
        manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        
      //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connect)
     //                                                name:UIApplicationDidBecomeActiveNotification object:nil];
        
    }
    return self;
}

- (void)connect {
    conState = kConnectionStateScanning;
    if ([self isConnected]) return;
    if (manager.state == CBCentralManagerStatePoweredOn) {
        
        [self scanPeripherals];
        if (scanTimer) {
            [scanTimer invalidate];
            scanTimer = nil;
        }
        scanTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scanTimeout) userInfo:nil repeats:NO];
    }
}

- (bool)connectPeripheral:(CBPeripheral*)peripheral {
    for (NSUUID *blockedId in blockedPeripheralUuids) {
        if ([peripheral.identifier isEqual:blockedId]) {
            return NO;
        }
    }
    
    if (scanTimer) {
       [scanTimer invalidate];
        scanTimer = nil;
    }
    
    conState = kConnectionStateConnecting;
    
    [manager stopScan];
    
    NSLog(@"Connecting periphiral %@", peripheral.name);
    
    IKPeripheral *device = [[IKPeripheral alloc] init];
    device.delegate = delegate;
    device.peripheral = peripheral;
    device.peripheral.delegate = device;
    
    if (connectedPeripheral.peripheral) {
        if (connectedPeripheral.peripheral.state != CBPeripheralStateDisconnected)
            [manager cancelPeripheralConnection:connectedPeripheral.peripheral];
    }
    
    connectedPeripheral = device;
    
    [manager connectPeripheral:peripheral options:nil];
    isConnecting = YES;
    [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(connectingTimeOut) userInfo:nil repeats:false];
    return YES;
}

- (void)connectingTimeOut {
    if (isConnecting) {
        isConnecting = NO;
        [self reconnect];
    }
}

- (void)scanTimeout {
    
    if (scanTimer) {
        [scanTimer invalidate];
        scanTimer = nil;
    }
    
    if ([scannedPeripherals count] == 0) {
        
        scanTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scanTimeout) userInfo:nil repeats:NO];
        
    } else if ([scannedPeripherals count] == 1 && !searchingMore) {
        
        if (![self connectPeripheral:[scannedPeripherals objectAtIndex:0]]) {
            scanTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scanTimeout) userInfo:nil repeats:NO];
        } else {
            searchingMore = NO;
        }
        
    } else {
        [manager stopScan];
        
        //show selection
        deviceSheet = [UIAlertController alertControllerWithTitle:@"Choose roaster to connect"
                                                          message:nil
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
        for (CBPeripheral* peripheral in scannedPeripherals) {
            [deviceSheet addAction:[UIAlertAction actionWithTitle:peripheral.name style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self connectPeripheral:peripheral];
                searchingMore = NO;
            }]];
        }
        [deviceSheet addAction:[UIAlertAction actionWithTitle:@"Search again..." style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
               [self connect];
        }]];
        //if iPhone
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [deviceSheet show];
        }
        //if iPad
        else {
            // Change Rect to position Popover
            UIViewController *vc = UIApplication.sharedApplication.keyWindow.rootViewController;
            [[vc presentingViewController] dismissViewControllerAnimated:YES completion:^{
                
            }];
            deviceSheet.modalInPopover = YES;
            UIPopoverPresentationController * pvc = deviceSheet.popoverPresentationController;
            pvc.delegate = nil;
            pvc.permittedArrowDirections = UIPopoverArrowDirectionUp;
            pvc.sourceView = vc.view;
            pvc.sourceRect = CGRectMake(vc.view.frame.size.width / 2, vc.view.frame.size.height / 2, 1, 1);
            [vc presentViewController:deviceSheet animated:YES completion:nil];
        }
    }
}

- (void)scanPeripherals {
    
    if ([self isConnected]) return;
    
    //check if other apps are connected
    NSArray *peripherals = [manager retrieveConnectedPeripheralsWithServices:
                            [NSArray arrayWithObject:[CBUUID UUIDWithString:IK_DATA_SERVICE_UUID]]];
    
    for (CBPeripheral *peripheral in peripherals) {
        if ([self connectPeripheral:peripheral])
            return;
    }
    
    conState = kConnectionStateScanning;
    
    NSLog(@"Scanning...");
    
    [manager stopScan];
    
    [scannedPeripherals removeAllObjects];
    
    NSArray	*uuidArray = [NSArray arrayWithObject:[CBUUID UUIDWithString:IK_DATA_SERVICE_UUID]];
	NSDictionary *options = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
    [manager scanForPeripheralsWithServices:uuidArray options:options];

    if (deviceSheet) {
        [deviceSheet dismissViewControllerAnimated:true completion:nil];
    }
}

- (void)stopScan {
    if (scanTimer) {
        [scanTimer invalidate];
        scanTimer = nil;
    }
    [manager stopScan];
    isConnecting = NO;
}

- (BOOL)isConnected {
    return (connectedPeripheral && connectedPeripheral.peripheral.state == CBPeripheralStateConnected);
}


- (BOOL)isBluetoothEnabled {
    return manager.state == CBCentralManagerStatePoweredOn;
}


- (void)disconnect {
    conState = kConnectionStateIdle;
    [scannedPeripherals removeAllObjects];
    searchingMore = NO;
    NSLog(@"disconnect");
    NSLog(@"Disconnecting from roaster");
    if (connectedPeripheral.peripheral) [manager cancelPeripheralConnection:connectedPeripheral.peripheral];
}

- (void)reconnect {
    conState = kConnectionStateScanning;
    [scannedPeripherals removeAllObjects];
    searchingMore = YES;
    NSLog(@"reconnect");
    NSLog(@"Disconnecting from roaster");
    if (connectedPeripheral.peripheral) [manager cancelPeripheralConnection:connectedPeripheral.peripheral];
}

- (void)blockPeripheral {
    
    NSLog(@"Blocking connected peripheral");
    
    if (connectedPeripheral.peripheral) [blockedPeripheralUuids addObject:connectedPeripheral.peripheral.identifier];
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"Central manager changed state: %ld", (long)central.state);
    
    if (central.state == CBCentralManagerStatePoweredOn) {
        //[self connect];
    }
    
    if (delegate && [delegate respondsToSelector:@selector(peripheralDidDisconnect:)]) {
        [delegate peripheralDidDisconnect:nil];
    }
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals {
    NSLog(@"%lu known periphirals retrieved", (unsigned long)[peripherals count]);
}


- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals {
    NSLog(@"%lu connected periphirals retrieved", (unsigned long)[peripherals count]);
    
    for (CBPeripheral *peripheral in peripherals) {
        //TODO: check if it has the IKAWA service!
        [self connectPeripheral:peripheral];
        break;
    }
    
    
    if (![peripherals count]) {
        [self scanPeripherals];
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    
    if (peripheral.name == nil) {
        return;
    }
    
    NSLog(@"Periphiral discovered: %@, signal strength: %d", peripheral.name, RSSI.intValue);
    
    for (CBPeripheral *device in scannedPeripherals) {
        if ([device.name isEqualToString:peripheral.name]) {
            return;
        }
    }
    if (![blockedPeripheralUuids containsObject:peripheral.identifier]) {
        [scannedPeripherals addObject:peripheral];
    }

}


- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"Periphiral connected: %@", peripheral.name);
    isConnecting = NO;
    [connectedPeripheral discoverServices];
        
    conState = kConnectionStateConnected;
    
//#ifdef DEBUG_RANDOM_DISCONNECT
//
//    [NSTimer scheduledTimerWithTimeInterval:arc4random() % 30 target:self selector:@selector(disconnect) userInfo:nil repeats:NO];
//#endif
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"Periphiral disconnected: %@", peripheral.name);

    
    if (delegate && [delegate respondsToSelector:@selector(peripheralDidDisconnect:)]) {
        [delegate peripheralDidDisconnect:connectedPeripheral];
    }
    
    connectedPeripheral = nil;
    
    if (conState != kConnectionStateIdle) {
        searchingMore = true;
        NSLog(@"Connect!!!!!!!");
        [self connect];
    }
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"Periphiral failed to connect: %@", peripheral.name);
    
    connectedPeripheral = nil;
    
    [self scanPeripherals];
}

#pragma mark - Application Specifics

- (void)streamData:(const unsigned char*)bytes andLength:(int)dataLength {
    
    [connectedPeripheral streamData:bytes andLength:dataLength];
}

@end
