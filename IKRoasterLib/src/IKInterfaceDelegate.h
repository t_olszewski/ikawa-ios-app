//
//  Created by Tijn Kooijmans on 05-10-13.
//  Copyright (c) 2013 Studio Sophisti. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IKPeripheral;
@class IKProfile;

@protocol IKInterfaceDelegate <NSObject>

- (void)peripheralDidConnect:(IKPeripheral*)peripheral;
- (void)peripheralDidDisconnect:(IKPeripheral*)peripheral;
- (void)peripheral:(IKPeripheral*)peripheral didReceiveData:(NSData*)data;


@end
