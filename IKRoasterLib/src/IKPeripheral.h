//
//  Created by Tijn Kooijmans on 05-10-13.
//  Copyright (c) 2013 Studio Sophisti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/Corebluetooth.h>
#import "IKInterfaceDelegate.h"

// Bluetooth UUIDs
#define IK_DATA_SERVICE_UUID            @"C92A6046-6C8D-4116-9D1D-D20A8F6A245F"
#define IK_BT_ADDR_CHAR_UUID            @"9C4A03B6-E762-4BDD-A732-A3CD76CB5679"
#define IK_DATA_RX_CHAR_UUID            @"948C5059-7F00-46D9-AC55-BF090AE066E3"
#define IK_DATA_TX_CHAR_UUID            @"851A4582-19C1-4E6C-AB37-E7A03766BA16"
#define IK_BAUD_RATE_CHAR_UUID          @"4DA79156-8F79-4C52-9928-6C8A58246A20"

@interface IKPeripheral : NSObject <CBPeripheralDelegate>
{
    CBPeripheral *peripheral_;
    CBCharacteristic *rxChar, *txChar, *btAddrCHar, *baudRateChar;
    
    id<IKInterfaceDelegate> __weak delegate;
    
    NSMutableArray *messageQueue;
    
    BOOL isStreaming;
}

@property(nonatomic, strong) CBPeripheral *peripheral;
@property(nonatomic, weak) id<IKInterfaceDelegate> delegate;

- (void)streamData:(const unsigned char*)bytes andLength:(int)dataLength;
- (void)discoverServices;

@end
