//
//  Profile.m
//  FFGlobalAlertController
//
//  Created by Tijn Kooijmans on 05/03/2018.
//

#import "Profile.h"
#import "spec.pb.h"
#import "ProfileFunctions.h"

@implementation Profile

- (instancetype)init {
    if (self = [super init]) {
        self.showInLibrary = YES;
        return self;
    }
    return nil;
}

- (NSData*)convertToProtobuf
{
    RoastProfile rp = [ProfileFunctions convertToProtobuf:self];
    std::string profileString;
    size_t size = rp.ByteSize();
    uint8_t bytes[size];
    rp.SerializeToArray(bytes, size);
    NSData *data = [NSData dataWithBytes:bytes length:size];
    return data;
}

+ (Profile*)convertFromProtobuf:(NSData*)data
{
    RoastProfile rp;
    rp.ParseFromArray(data.bytes, data.length);
    Profile *profile = [ProfileFunctions convertFromProtobuf:rp];
    return profile;
}
- (void)setShouldOpenInEditMode:(BOOL)shouldOpenInEditMode {
    _shouldOpenInEditMode = shouldOpenInEditMode;
}

- (BOOL)compare:(Profile *)profile {
   if ([self.uuid isEqualToString:profile.uuid] &&
       [self.parentUuid isEqualToString:profile.parentUuid] &&
       [self.name isEqualToString:profile.name] &&
       self.schemaVersion == profile.schemaVersion &&
       self.revision == profile.revision &&
       self.revision == profile.revision &&
       self.type == profile.type &&
       self.tempSensor == profile.tempSensor &&
       self.showInLibrary == profile.showInLibrary &&
       self.archived == profile.archived &&
       self.favorite == profile.favorite &&
       [self.cropsterId isEqualToString:profile.cropsterId] &&
       self.onServer == profile.onServer) {
        return YES;
    }
    return NO;
}

- (NSDate *)dateLastUsed {
    return [self.dateLastRoasted laterDate:self.dateCreated];
}
    
@end

@implementation RoastTempPoint
@end

@implementation RoastFanPoint
@end
