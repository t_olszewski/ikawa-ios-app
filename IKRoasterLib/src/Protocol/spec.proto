syntax = "proto2";

// Framing:
// (Based on asynchronous framing from HDLC protocol)
//  - Frame begins and ends with 0x7e
//    (end 0x7e can be the beginning of the next frame -- i.e. only
//     one 0x7e is required between frames)
//  - 0x7d byte is the 'escape byte': it must be removed from the data
//    stream, and the next byte to be received must be XOR'ed with
//    0x20.  e.g. if a frame needs to contain a byte of data that is
//    0x7e, the bytes on the wire must be 0x7d 0x5e

// Frame payload for frames sent from BLE client to Roaster
//  - An encoded protobuf Cmd message
//  - 16-bit CRC of the protobuf message (note: CRC of *unescaped* data)
//       using polynomial: x^16 + x^12 + x^5 + 1 (0x8408)
//          initial value: 0xffff
//          MSB first.

// Frame payload for frames sent from Roaster to BLE client
//  - An encoded protobuf Response message
//  - 16-bit CRC of protobuf message
//    (same CRC arrangement as frames in other direction)

enum CmdType {
	// Get firmware version
	BOOTLOADER_GET_VERSION = 0;
	// Jump to the bootloader
	BOOTLOADER_JUMP = 1;

	// Get machine type
	MACH_PROP_GET_TYPE = 2;
	// Get machine ID
	MACH_PROP_GET_ID = 3;
	// Get information on what this machine supports
	MACH_PROP_GET_SUPPORT_INFO = 23;
	// Get machine name
	MACH_PROP_GET_NAME = 28;
	// Set machine name
	MACH_PROP_SET_NAME = 29;

	// Get buffered sensor values -- takes a CmdMachStatusGetSensors arg
	MACH_STATUS_GET_SENSORS = 22;
	// Get the current error state
	MACH_STATUS_GET_ERROR = 10;
	// Get all the following bundled together
	// (time, temp, fan, state, heater, PID, setpoints)
	MACH_STATUS_GET_ALL = 11;
	// Get machine time in milliseconds
	MACH_STATUS_GET_TIME = 27;

	// Get the summary of the last roast
	ROAST_SUMMARY_GET = 30;

	// Get number of roasts completed with this profile
	HIST_GET_ROAST_COUNT = 12;
	// Get the total number of roasts this machine has done
	HIST_GET_TOTAL_ROAST_COUNT = 13;

	// Get the current profile
	PROFILE_GET = 15;
	// Set the current profile -- takes a CmdProfileSet arg
	PROFILE_SET = 16;

	// Get a setting
	SETTING_GET = 17;
	// Set a setting
	SETTING_SET = 18;
	// Something to get info on a particular setting
	SETTING_GET_INFO = 19;
	// Something to get info on all settings
	SETTING_GET_LIST = 20;

	// Reboot the machine
	MACH_REBOOT = 21;

	// Start a test on the machine in test-mode
	START_TEST = 24;

	// Requests the status of the current test
	TEST_STATUS_GET = 25;

	// End a test on the machine in test-mode
	END_TEST = 26;
}

// A temperature point
message TempPoint {
	// The time multiplied by 10 (e.g. 1.2 seconds is 12 in this field)
	required uint32 time = 1;

	// The temp multiplied by 10
	required uint32 temp = 2;
}

// A fan point
message FanPoint {
	// The time multiplied by 10 (e.g. 1.2 seconds is 12 in this field)
	required uint32 time = 1;

	// The fan power (0-255)
	required uint32 power = 2;
}

// A roast profile
message RoastProfile {
	// The schema number of the profile
	// This is currently 1.
	required uint32 schema = 1;

	// Max length 16 bytes (most likely a UUID)
	required bytes id = 2;

	// Max length 100 bytes
	required string name = 3;

	// Maximum of 20
	repeated TempPoint temp_points = 4;

	// Maximum of 20
	repeated FanPoint fan_points = 5;

	// The temp sensor to use whilst roasting
	enum TempSensor {
		// The sensor above the beans, standard for PRO
		ABOVE_BEANS = 0;

		// The sensor below the beans, standard for HOME
		BELOW_BEANS = 1;

		// The sensor below the beans, new type for PRO
		ABOVE_ROBUST = 2;
	}

	required TempSensor temp_sensor = 6;

	// Cooldown fan point
	// Once the roast has completed, the roaster will ramp the fan
	// up to the given speed over a period of 10 seconds.  It will
	// then exit cooldown once both of the following conditions
	// are met:
	//  - The temperature has reduced below the cooldown
	//    threshold (which is a factory-configured parameter)
	//  - The time given in this fan point has passed.
	//    This is the time since the beginning of the roast.
	required FanPoint cooldown_fan = 7;
}

enum Test {
	NO_TEST = 0;
	BUTTON_TEST = 1;
	LED_TEST = 2;
	CALIBRATE_TEMP_SENSORS = 3;
	TEST_TEMP_SENSORS = 4;
	TEST_FAN_ERROR = 5;
	TEST_FAN_SPIN = 6;
	TEST_FAN_STOP = 7;
	TEST_HEATER_RELAY = 8;
	TEST_HEATER_PWM = 9;
	TEST_DOSER = 10;
	TEST_BOARD_TEMP = 11;
	TEST_INVALID = 12;
	FLASH_BLE_FIRMWARE = 13;
	FLASH_AVR_FIRMWARE = 14;
}

enum TestStatus {
	NO_TEST_STARTED = 0;
	TEST_IN_PROGRESS = 1;
	TEST_SUCCESS = 2;
	TEST_FAILED = 3;
}

enum TestFailure {
	NO_FAILURE = 0;
	BUTTONS_NOT_RESPONDING = 1;
	BUTTON_1_NOT_RESPONDING = 2;
	BUTTON_2_NOT_RESPONDING = 3;
	BUTTONS_SHORT_TO_GROUND = 4;
	BUTTON_1_SHORT_TO_GROUND = 5; 
	BUTTON_2_SHORT_TO_GROUND = 6;
	HEATER_RELAY_NOT_CLOSING = 7;
	HEATER_RELAY_NOT_OPENING = 8;
	DOSER_NOT_CLOSING = 9;
	DOSER_NOT_OPENING = 10;
	HEATER_PWM_FAILED = 11;
	ROOM_TEMP_NOT_SET = 12;
	NTC_TOLERANCE_NOT_SET = 13;
	BOARD_TEMP_OUT_OF_RANGE = 14;
	MIN_FAN_RPM_NOT_SET = 15;
	FAN_RPM_TOO_LOW = 16;
	FAN_RPM_ZERO = 17;
	FAN_BREAK_NOT_WORKING = 18;
	FAN_FAULT = 19;
	FAN_FAULT_NOT_WORKING = 20;
	TEMP_ABOVE_OUT_OF_RANGE = 21;
	TEMP_BELOW_OUT_OF_RANGE = 22;
	TEMP_ABOVE_M_OUT_OF_RANGE = 23;
	TEMP_ABOVE_C_OUT_OF_RANGE = 24;
	TEMP_BELOW_M_OUT_OF_RANGE = 25;
	TEMP_BELOW_C_OUT_OF_RANGE = 26;
	TEST_INTERRUPTED = 27;
}

// Args for MACH_STATUS_GET_SENSORS
message CmdMachStatusGetSensors {
	// Time of the last received reading
	// The roaster will discard all readings prior to this time,
	// and provide as many as will fit in a frame in response
	// after this time.
	// This is the time multiplied by 10 (i.e. 1.2 seconds is 12
	// in this field)
	required uint32 time = 1;
}

// Args for PROFILE_SET
message CmdProfileSet {
	required RoastProfile profile = 1;
}

// Args for SETTING_GET
message CmdSettingGet {
	// The setting number
	required uint32 number = 1;
}

// Args for SETTING_SET
message CmdSettingSet {
	// The setting number
	required uint32 number = 1;

	optional uint32 val_u32 = 2;
	// Value multiplied by 1000000
	optional sint32 val_u32_float = 3;
}

// Args for SETTING_GET_INFO
message CmdSettingGetInfo {
	// The setting number
	required uint32 number = 1;
}

// Args for SETTING_GET_LIST
message CmdSettingGetList {
	// Offset to start from
	required uint32 offset = 1;
}

// Args for START_TEST
message CmdStartTest {
	// Which test to start
	required Test test = 1;

	// The time to use for this test
	required uint32 test_time = 2;

	// The room temperature, used by TEST_BOARD_TEMP
	optional uint32 room_temp = 3;
}

// Args for MACH_PROP_SET_NAME
message CmdPropSetName {
	// The name of the machine
	// Max length 100 bytes
	required string name = 1;
}

// The command itself
message Cmd {
	required CmdType cmd_type = 1;

	// Sequence code: comes back in response
	required uint32 seq = 2;

	// Command args
	optional CmdProfileSet profile_set = 4;
	optional CmdSettingGet setting_get = 5;
	optional CmdSettingSet setting_set = 6;
	optional CmdSettingGetInfo setting_get_info = 7;
	optional CmdSettingGetList setting_get_list = 8;
	optional CmdMachStatusGetSensors mach_status_get_sensors = 9;
	optional CmdStartTest test_start = 10;
	optional CmdPropSetName name_set = 11;
}

// Response messages

message RespBootloaderGetVersion {
	// The version
	required uint32 version = 1;

	// The git commit hash of the firmware (with "-mod" on the end if modified)
	optional string revision = 2;
}

message RespMachPropGetType {
	enum MachType {
		// "Original" hardware version shipped to Nespresso
		v1 = 0;
		// Second iteration of hardware
		v2 = 1;
		// Third iteration of hardware (first to be made in China)
		v3 = 2;
		v4 = 3;
		v5 = 4;
	}
	enum MachVariant {
		// Pro roasters
		PRO = 0;

		// Roasters shipped to Nespresso
		NESPRESSO = 1;

		// A bare-board roaster (no temp sensors, fan, or heater)
		BARE = 2;

		// Home roasters
		HOME = 3;
	} 

	// The machine type
	required MachType type = 1;

	// The machine variant
	required MachVariant variant = 2;
}

message RespMachPropGetID {
	// The ID of the machine
	required uint32 id = 1;
}

message SensorReadings {
	// Temperature sensor above beans
	required uint32 temp_above = 1;
	// Temperature sensor below beans
	required uint32 temp_below = 2;
	// Measured fan speed
	required uint32 fan = 3;
}

message RespMachStatusGetSensors {
	// The time of the first reading
	// (Time multiplied by 10 again)
	// (All readings are spaced 1 second apart)
	required uint32 time = 1;

	// Sensor readings
	repeated SensorReadings reading = 2;
}

enum MachState {
	IDLE = 0;
	PRE_HEAT = 1;
	READY_FOR_ROAST = 2;
	ROASTING = 3;
	BUSY = 4;
	COOLDOWN = 5;
	DOSER_OPEN = 6;
	ERROR = 7;
	READY_TO_BLOWOVER = 8;
	TEST_MODE = 9;
}

message RespMachStatusGetError {
	enum ErrorType {
		NONE = 1;

		// The board is too hot
		BOARD_TOO_HOT = 2;

		// The above sensor got too hot
		ABOVE_BEANS_TOO_HOT = 3;

		// The above sensor got too cold
		ABOVE_BEANS_TOO_COLD = 4;

		// The fan was not spinning fast enough
		FAN_NOT_SPINNING = 5;

		// A motor failure occurred
		MOTOR_FAILURE = 6;

		// The below sensor got too hot
		BELOW_BEANS_TOO_HOT = 7;

		// The below sensor got too cold
		BELOW_BEANS_TOO_COLD = 8;
	}
	required ErrorType error = 1;
}

message RespMachStatusGetAll {
	required uint32 time = 1;
	optional uint32 temp_above = 2;
	optional uint32 temp_below = 12;
	required uint32 fan = 3;
	// The measured fan speed
	optional uint32 fan_measured = 10;
	required MachState state = 4;
	required uint32 heater = 5;
	required uint32 p = 6;
	required uint32 i = 7;
	required uint32 d = 8;
	optional sint32 j = 19;
	required uint32 setpoint = 9;
	optional uint32 board_temp = 11;
	optional uint32 fan_rpm_measured = 13;
	optional uint32 fan_rpm_setpoint = 14;
	optional sint32 fan_p = 15;
	optional sint32 fan_i = 16;
	optional sint32 fan_d = 17;
	optional uint32 fan_power = 18;
	optional uint32 relay_state = 20;
	enum PidSensor {
		ABOVE = 1;
		BELOW = 2;
	}
	optional PidSensor pid_sensor = 21;
	optional uint32 temp_above_filtered = 22;
	optional uint32 temp_below_filtered = 23;
	optional sint32 ror_above = 24;
	optional sint32 ror_below = 25;
}

message RespMachStatusGetTime {
	// The current machine time in milliseconds from boot
	required uint32 time = 1;
}

message RespRoastSummaryGet {
	required uint32 roast_done = 1;
	optional uint32 end_temp_above = 2;
	optional uint32 end_temp_below = 3;
}

message RespHistGetProfileRoastCount {
	// The number of roasts completed with this profile
	required uint32 count = 1;
}

message RespHistGetTotalRoastCount {
	// The number of roasts this roaster has done
	required uint32 count = 1;
}

message RespProfileGet {
	// The current roast profile
	required RoastProfile profile = 1;
}

message RespSettingGet {
	optional uint32 val_u32 = 1;

	// Value multiplied by 1000000
	optional sint32 val_u32_float = 2;
}

message RespSettingGetInfo {
	required string name = 1;

	enum SettingType {
		UINT8 = 1;
		UINT16 = 2;
		UINT32 = 3;
		FLOAT = 4;
	}

	required SettingType type = 2;
}

message RespSettingGetList {
	// Repeated setting numbers
	// Will respond with up to 32 values.
	// If there are less than 32, then that's the end of the list of
	// settings.
	repeated uint32 number = 1;
}

message RespMachPropGetSupportInfo {
	// The profile schema number that this roaster supports
	required uint32 profile_schema = 1;
}

message RespMachPropGetName {
	// The name of the machine
	// Max length 100 bytes
	required string name = 3;
}

message RespTestStatusGet {
	// The current status of the test
	required TestStatus status = 1;

	// The current test in progress
	optional Test test = 3;

	// In case the test failed, returns the failure type
	optional TestFailure failure = 2;
}

// The response
message Response {
	required uint32 seq = 1;

	enum GenericResp {
		OK = 1;
		ERROR = 2;
	}

	required GenericResp resp = 2;

	// Responses

	// BOOTLOADER_GET_VERSION:
	optional RespBootloaderGetVersion resp_bootloader_get_version = 3;

	// MACH_PROP_GET_TYPE:
	optional RespMachPropGetType resp_mach_prop_type = 4;
	// MACH_PROP_GET_ID:
	optional RespMachPropGetID resp_mach_id = 5;
	// MACH_PROP_GET_SUPPORT_INFO
	optional RespMachPropGetSupportInfo resp_mach_prop_get_support_info = 21;
	// MACH_PROP_GET_NAME
	optional RespMachPropGetName resp_mach_prop_get_name = 24;

	// MACH_STATUS_GET_SENSORS:
	optional RespMachStatusGetSensors resp_mach_status_get_sensors = 20;
	// MACH_STATUS_GET_ERROR:
	optional RespMachStatusGetError resp_mach_status_get_error = 12;
	// MACH_STATUS_GET_ALL:
	optional RespMachStatusGetAll resp_mach_status_get_all = 13;
	// MACH_STATUS_GET_TIME:
	optional RespMachStatusGetTime resp_mach_status_get_time = 23;

	// ROAST_SUMMARY_GET:
	optional RespRoastSummaryGet resp_roast_summary_get = 25;

	// HIST_GET_PROFILE_ROAST_COUNT:
	optional RespHistGetProfileRoastCount resp_hist_get_profile_roast_count = 14;
	// HIST_GET_TOTAL_ROAST_COUNT:
	optional RespHistGetTotalRoastCount resp_hist_get_total_roast_count = 15;

	// PROFILE_GET
	optional RespProfileGet resp_profile_get = 16;

	// SETTING_GET
	optional RespSettingGet resp_setting_get = 17;

	// SETTING_GET_INFO
	optional RespSettingGetInfo resp_setting_get_info = 18;

	// SETTING_GET_LIST
	optional RespSettingGetList resp_setting_get_list = 19;

	// TEST_STATUS_GET
	optional RespTestStatusGet resp_test_status_get = 22;
}
